// JavaScript Document
$(document).ready(function(e) {
	

	$('#lock_msg').popover();
    $('.panel').bind('shown.bs.collapse', function (e) {
	  // do something…
	  $(this).addClass('open');
	  console.log($(this).attr('id'));
	});
	$('.panel').bind('hidden.bs.collapse', function (e) {
	  // do something…
	  $(this).removeClass('open');
	  
	});
	
	$('#addCourseSubmit').click(function(e){
		$(this).text('Updating...Please wait');
		$('#addcourseform').submit();	
		return false;
	});
	
	
	$('.m_title').click(function (e) {
		$('#module-panel-'+$(this).attr('module')).toggleClass('open');
		//el.addClass('open');
		console.log('Event fired on #' + $(this).attr('module'));
	});
	
	$('.resource_delete').click(function(){

		if(confirm('Are you sure you want to delete this? This is cannot be undone') != true){
			return false;
		}
		
		var resource_id = $(this).attr('resource_id');
		
		var data_target = $(this).attr('data-target');
            $.ajax({
                url: '/deleteresource/'+resource_id,  //server script to process data
                type: 'POST',
                //Ajax events
                success: completeHandler = function(data) {					
					$('#'+data_target).remove();
                },
                error: errorHandler = function() {
                    alert("Error");
                },
                // Form data
                data: '',
                //Options to tell JQuery not to process data or worry about content-type
                cache: false,
                contentType: false,
                processData: false
            }, 'json');
			return false;		
	});

	$('.resource_edit').click(function(){
		
		var resource_id = $(this).attr('resource_id');
		var module_id = $(this).attr('module_id');
		var data_target = $(this).attr('data-target');
		
		var list_module_no = $(this).attr('list_module_no');
		var list_resource_no = $(this).attr('list_resource_no');
		
		$('#module'+list_module_no+'_resource_action').val("update");
		$('#module'+list_module_no+'_resource_id').val(resource_id);
		$('#module'+list_module_no+'add_resource').html(' Update <i class="fa fa-angle-right"></i>').attr('list_module_no',list_module_no).attr('list_resource_no',list_resource_no);		
		$('#module'+list_module_no+'_resource_title').val($('#module_'+list_module_no+'_resource_'+list_resource_no+'_title').val());
		return false;		
	});	
	
	$('#addModule').click(function(e) {
		var module_count = parseInt($('#count_module').val())+1;
		
		var delete_block = ($('#mode').val()=="edsfdsit")?'<div class="delete-module"><a href="#" onclick="delete_module(module-panel-'+module_count+')" class="deletemoduletrgigger" data-target="module-panel-'+module_count+'">Delete this module</a></div>':'';
		$('#count_module').val(module_count);
		var html = '<div id="module-panel-'+module_count+'" class="panel panel-default open">                     ' +
            '<div class="panel-heading" role="tab" id="heading'+module_count+'">                       ' +
            '<h4 class="panel-title"> 					' +
            '<a module="'+module_count+'" class="m_title" data-toggle="collapse" data-parent="#accordion" href="#collapse'+module_count+'" aria-expanded="true" aria-controls="collapse'+module_count+'">  Module '+(module_count-2)+'  																					                        </a> 																			                      </h4>                     </div>';
		
            html = html+'<div id="collapse'+module_count+'" class="panel-collapse collapse" role="tabpane'+module_count+'" aria-labelledby="heading'+module_count+'">                      														' +
            '<div class="panel-body"> <input type="hidden" id="module_'+module_count+'_resource_ids" name="module_'+module_count+'_resource_ids" value="" placeholder="Resource Title '+(module_count-2)+'" />                       ' +
            '<div class="module-fields">                          ' +
            '<label>Title</label>                            ' +
            '<input type="text" id="module_'+module_count+'_title" name="module_'+module_count+'_title" value="" placeholder="Module Title '+(module_count-2)+'" />                             ' +
            '<label>Description</label>                               ' +
            '<textarea id="module_'+module_count+'_description" cols="100" rows="5" name="module_'+module_count+'_description"></textarea>  ' +
            '<div class="drop"> <input type="hidden" name="module'+module_count+'_resource_count" value="0" />                               ' +
            '<label>Resources</label><br/> ' +
            '<input type="text" id="module'+module_count+'_resource_title" name="module'+module_count+'_resource_title" ' +
            'value="" placeholder="Resource Title '+(module_count-2)+'" /> ' +
            '<input type="text" value="" name="module'+module_count+'_video_key" ' +
            'id="module'+module_count+'_video_key" placeholder="Put youtube or vimeo url here(optional)">'+
            '<input type="hidden" id="module'+module_count+'_resource_action"  name="module'+module_count+'_resource_action" value="add" /> ' +
            '<input type="hidden" id="module'+module_count+'_resource_id"  name="module'+module_count+'_resource_id" value="" />' +
            '<input type="file" name="module'+module_count+'_upl" id="module'+module_count+'_upl" multiple />' +
            '<a class="pinkbutton blackbg addresource" href="#" module_id="module'+module_count+'" onclick="return addResources('+module_count+')"> Add <i class="fa fa-angle-right"></i></a> ' +
            ' </div> ' +
            ' <div class="module-resources"> ' +
            '<ul id="module-resources-'+module_count+'"> </ul> '+delete_block+'</div> ' +
            '</div></div><input type="hidden" id="module_'+module_count+'_action" name="module_'+module_count+'_action" value="add" /> ' +
            '</div> </div>';

	  $(".panel-group" ).append(html);
	   	return false;
    });

	//Certificate Module
	$('#addCertificateModule').click(function(e) {
		var module_count = parseInt($('#count_module').val())+1;

		var delete_block = ($('#mode').val()=="edsfdsit")?'<div class="delete-module"><a href="#" onclick="delete_module(module-panel-'+module_count+')" class="deletemoduletrgigger" data-target="module-panel-'+module_count+'">Delete this module</a></div>':'';
		$('#count_module').val(module_count);
		var html = '<div id="module-panel-'+module_count+'" class="panel panel-default">                     ' +
				'<div class="panel-heading" role="tab" id="heading'+module_count+'">                       ' +
				'<h4 class="panel-title"> 					' +
				'<a module="'+module_count+'" class="m_title" data-toggle="collapse" data-parent="#accordion" href="#collapse'+module_count+'" aria-expanded="true" aria-controls="collapse'+module_count+'">  Module '+(module_count-1)+'  																					                        </a> 																			                      </h4>                     </div>';

		html = html+'<div id="collapse'+module_count+'" class="panel-collapse collapse" role="tabpane'+module_count+'" aria-labelledby="heading'+module_count+'">                      														' +
				'<div class="panel-body"> <input type="hidden" id="module_'+module_count+'_resource_ids" name="module_'+module_count+'_resource_ids" value="" placeholder="Module Title '+module_count+'" />                       ' +
				'<div class="module-fields">                          ' +
				'<label>Title</label>                            ' +
				'<input type="text" id="module_'+module_count+'_title" name="module_'+module_count+'_title" value="" placeholder="Module Title '+(module_count-1)+'" />                             ' +
				'<label>Description</label>                               ' +
				'<textarea id="module_'+module_count+'_description" cols="100" rows="5" name="module_'+module_count+'_description"></textarea>  ' +
				'<div class="drop"> <input type="hidden" name="module'+module_count+'_resource_count" value="0" />                               ' +
				'<label>Resources</label><br/> ' +
				'<input type="text" id="module'+module_count+'_resource_title" name="module'+module_count+'_resource_title" ' +
				'value="" placeholder="Resource Title '+(module_count-1)+'" /> ' +
				'<input type="text" value="" name="module'+module_count+'_video_key" ' +
				'id="module'+module_count+'_video_key" placeholder="Put youtube or vimeo url here(optional)">'+
				'<input type="hidden" id="module'+module_count+'_resource_action"  name="module'+module_count+'_resource_action" value="add" /> ' +
				'<input type="hidden" id="module'+module_count+'_resource_id"  name="module'+module_count+'_resource_id" value="" />' +
				'<input type="file" name="module'+module_count+'_upl" id="module'+module_count+'_upl" multiple />' +
				'<a class="pinkbutton blackbg addresource" href="#" module_id="module'+module_count+'" onclick="return addResources('+module_count+')"> Add <i class="fa fa-angle-right"></i></a> ' +
				' </div> ' +
				' <div class="module-resources"> ' +
				'<ul id="module-resources-'+module_count+'"> </ul> '+delete_block+'</div> ' +
				'</div></div><input type="hidden" id="module_'+module_count+'_action" name="module_'+module_count+'_action" value="add" /> ' +
				'</div> </div>';

		$(".panel-group" ).append(html);
		return false;
	});
	
	//$( ".speed" ).selectmenu();
	//$('.dropdown').dropdown();
	
	$('.fancybox').fancybox({
			afterLoad: function(){

			}
		});
	$('.fancybox').click(function(){
		$("#feedbackModuleID").val($(this).attr("moduleID"));
		$("#feedbackCourseID").val($(this).attr("courseID"));	
		$("#feedbackUserID").val($(this).attr("userID"));			
	});



	//Ajax Feedback form
	$('#feedbackFormTrigger').click(function(){
		
		if($("#feedbackmessage").val()==""){
			$('#feedbackProgress').html('Please write something in messagebox');
			return false;
		}
		
		$('#feedbackProgress').html('Processing your request....please wait');
        var feedback_formdata = new FormData();
        feedback_formdata.append("user_id", $('#feedbackUserID').val());
        feedback_formdata.append("module_id", $('#feedbackModuleID').val());
        feedback_formdata.append("course_id", $('#feedbackCourseID').val());
        feedback_formdata.append("attachment_id", $('#feedbackAttachmentsID').val());
        feedback_formdata.append("message", $('#feedbackmessage').val());
		
        $.ajax({
                //url: '/postAdminFeedback/'+$("#feedbackUserID").val()+'/'+$("#feedbackModuleID").val()+'/'+$("#feedbackCourseID").val()+'/'+$("#feedbackmessage").val(),  //server script to process data
            url: '/postAdminFeedback',
            type: 'POST',
                //Ajax events
                success: completeHandler = function(data) {
					//console.log(data.filetype);
					
					$('#feedbackProgress').html('Your feedback successfully added');
					$('#feedbackmessage').val('');
					setTimeout($.fancybox.close,1000);
                },
                error: errorHandler = function(data) {
                    $('#feedbackProgress').html(data.msg);
                },
                // Form data
                data: feedback_formdata,
                //Options to tell JQuery not to process data or worry about content-type
                cache: false,
                contentType: false,
                processData: false
            }, 'json');
		return false;			
	});
	
	
	//show the feedback details
	$(".feedbackentry").click(function() {
		
/*		$("#feedbackModuleID").val($(this).attr("moduleID"));
		$("#feedbackCourseID").val($(this).attr("courseID"));	
		$("#feedbackUserID").val($(this).attr("userID"));*/
		//$.fancybox.show();	
		$.ajax({
			type		: "GET",
			cache	: false,
			url		: "/showFeedbackDetails/"+$(this).attr('feedbackid'),
			data    : '',
			success: function(data) {
				$("#postedby").html(data.sender);
				$("#postedon").html(data.posted_on);
				$("#fmessage").html(data.message);
				//$.fancybox(data);
			}
		});
		/*
		$.fancybox.show();*/
	
		//return false;
	});	
});

function addResources(module_id){
	if($('#module'+module_id+'_resource_title').val() ==""){
		$('#module'+module_id+'_resource_title').css('border','1px solid #ff0000').focus().attr('placeholder','Enter a title');
		return false;
	}
	
	var file = _('module'+module_id+'_upl').files[0]; // alert(file.name+" | "+file.size+" | "+file.type);
	var formdata = new FormData(); 
	formdata.append("file1", file); 
	formdata.append("resource_title", $('#module'+module_id+'_resource_title').val());
	formdata.append("resource_action", $('#module'+module_id+'_resource_action').val());
	formdata.append("resource_id", $('#module'+module_id+'_resource_id').val());
    formdata.append("video_code", $('#module'+module_id+'_video_key').val());
	$('#module'+module_id+'add_resource').html('Uploading...');
	$('#resource_status_'+module_id).html('Uploading...');


            $.ajax({
                url: '/addresource',  //server script to process data
                type: 'POST',
                xhr: function() {  // custom xhr
                    myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // if upload property exists
                        //myXhr.upload.addEventListener('progress', function(event){ progressHandler(module_id,event); }, false); // progressbar
						
                    }
                    return myXhr;
                },
                //Ajax events
                success: completeHandler = function(data) {
					//console.log(data.filetype);
					if(data.resourceaction=="add"){
						addNewModuleResource(module_id,data);
						var existing_resource_ids = $('#module_'+module_id+'_resource_ids').val();
						if(existing_resource_ids=='')
						$('#module_'+module_id+'_resource_ids').val(data.resourceid);
						else
						$('#module_'+module_id+'_resource_ids').val(existing_resource_ids+','+data.resourceid);
						
						$('#module'+module_id+'add_resource').html('Add <i class="fa fa-angle-right"></i>');
						
						$('#module'+module_id+'_resource_title').css('border','0');
						$('#module'+module_id+'_resource_title').val();
						$('#module'+module_id+'upl').val();
						$('#resource_status_'+module_id).hide(300);
						
					}else if(data.resourceaction=="error"){
                        $('#resource_status_'+module_id).html('<span class="">'+data.msg+'</span>');
                        return false;
                    }
                    else{
						console.log('module_'+module_id+'resources_'+$('#module'+module_id+'add_resource').attr('list_resource_no')+'_title');
						$('#module_'+module_id+'resources_'+$('#module'+module_id+'add_resource').attr('list_resource_no')+'_title').html(data.title);
						
						$('#module'+module_id+'_resource_action').val("add");
						$('#module'+module_id+'_resource_id').val(data.resourceid);
						$('#module'+module_id+'add_resource').html(' Add <i class="fa fa-angle-right"></i>');		
						$('#module'+module_id+'_resource_title').val('Resource Title');	
						
						$('#module'+module_id+'add_resource').html('Add <i class="fa fa-angle-right"></i>');
						$('#resource_status_'+module_id).hide(300);
						
					}
                },
                error: errorHandler = function() {
                    alert("FIle size is larger than allowed limit");
                    $('#module'+module_id+'add_resource').html('Upload');
                    $('#resource_status_'+module_id).hide();
                    return false;
                },
                // Form data
                data: formdata,
                //Options to tell JQuery not to process data or worry about content-type
                cache: false,
                contentType: false,
                processData: false
            }, 'json');
	return false;		
}

function addNewModuleResource(module_id,response){
	var el = '';
	var resource_count = $('#module-resources'+module_id).children('li').length;//parseInt($('#module'+module_id+'_resource_count').val())+1;
	el = el+'<li id="module_'+module_id+'resources_'+resource_count+'"><input type="hidden" id="module_'+module_id+'_resource_'+module_id+'_title" name="module_'+module_id+'_resource_'+module_id+'_title" value="'+$('#module'+module_id+'_resource_title').val()+'"/>';
	
	if(response.filetype=='pdf')
	    el = el+'<i class="fa fa-file-pdf-o"></i>';
	else if(response.filetype=='doc' || response.filetype=='docx')
	    el = el+'<i class="fa fa-file-word-o"></i>';
    else if(response.filetype=='ppt' || response.filetype=='pptx')
        el = el+'<i class="fa fa-file-powerpoint-o"></i>';
    else if(response.filetype=='story')
        el = el+'<i class="fa fa-file-audio-o"></i>';
	else if(response.filetype == "youtube" || response.filetype=='vimeo' || response.filetype=='avi' || response.filetype=='wmv')
	    el = el+'<i class="fa fa-file-video-o"></i>';
	
	el = el+'<span class="title">'+$('#module'+module_id+'_resource_title').val()+'</span>';
	el = el+'<span class="actionbar"><a href="#" module_id="'+module_id+'" class="resource_edit" resource_id="'+module_id+'" data-target="module_'+module_id+'resources_'+resource_count+'">Edit</a> | <a href="#" module_id="'+module_id+'" class="resource_delete" resource_id="'+module_id+'"  data-target="module_'+module_id+'resources_'+resource_count+'">Delete</a></span>';
	$('#module-resources-'+module_id).append(el);
	return false;	
}

function _(el){ return document.getElementById(el); }

/*$('#user_submission_trigger').click(function(e) {

	var module_id = $(this).attr('module_id');
	var course_id = $(this).attr('course_id');
	var user_id = $(this).attr('user_id');
	// For some browsers, `attr` is undefined; for others,
	// `attr` is false.  Check for both.
	if (!upload_permission) {
		$('.drop').prepend('<h4>'+msg+'</h4>');
		return false;
	}


	var file = _('user_submission').files[0];  //alert(file.name+" | "+file.size+" | "+file.type);
	if (file.name == '') {
		$('.drop').prepend('<h4>Choose a file please.Accepted formats are MS Word, PDF, Image, PPT</h4>');
		return false;
	}
	var formdata = new FormData();
	formdata.append("file1", file);
	formdata.append("moduleID", module_id);
	formdata.append("courseID", course_id);
	$('.drop').prepend('<h4>Your attachment is uploading...please wait</h4>');

	$.ajax({
		url: '/addusersubmission',  //server script to process data
		type: 'POST',
		xhr: function() {  // custom xhr
			myXhr = $.ajaxSettings.xhr();
			if(myXhr.upload){ // if upload property exists
				//myXhr.upload.addEventListener('progress', function(event){ progressHandler(module_id,event); }, false); // progressbar

			}
			return myXhr;
		},
		//Ajax events
		success: completeHandler = function(data) {
			//console.log(data.filetype);
			$('.drop > h4').html('Module submission successfully posted.');
			$('#user_submission').attr('disabled','disabled');
			$('#user-submission-items').prepend('<li> <span class="date">'+data.created_at+'</span><span class="filename">'+data.filename+'</span> </li>');
			$('#user_submission').val('');
			$('.drop > h4').hide(4000);
		},
		error: errorHandler = function() {
			alert("Error");
		},
		// Form data
		data: formdata,
		//Options to tell JQuery not to process data or worry about content-type
		cache: false,
		contentType: false,
		processData: false
	}, 'json');
	return false;
});*/
function addModuleSubmission(module_id,course_id,upload_permission,msg){
	
	// For some browsers, `attr` is undefined; for others,
	// `attr` is false.  Check for both.
	if (!upload_permission) {
		$('.drop').prepend('<h4>'+msg+'</h4>');
		return false;
	}
	

	var file = _('user_submission').files[0];  //alert(file.name+" | "+file.size+" | "+file.type);
	if (file.name == '') {
		$('.drop').prepend('<h4>Choose a file please.Accepted formats are MS Word, PDF, Image, PPT</h4>');
		return false;
	}	
	var formdata = new FormData(); 
	formdata.append("file1", file); 
	formdata.append("moduleID", module_id); 
	formdata.append("courseID", course_id);
	$('.drop').prepend('<h4>Your attachment is uploading...please wait</h4>');

            $.ajax({
                url: '/addusersubmission',  //server script to process data
                type: 'POST',
                xhr: function() {  // custom xhr
                    myXhr = $.ajaxSettings.xhr();
                    if(myXhr.upload){ // if upload property exists
                        //myXhr.upload.addEventListener('progress', function(event){ progressHandler(module_id,event); }, false); // progressbar
						
                    }
                    return myXhr;
                },
                //Ajax events
                success: completeHandler = function(data) {
					//console.log(data.filetype);
					$('.drop > h4').html('Thank you, your submission has been sent to one of our examiners. You will receive an email notification when we have evaluated your submission.');
					$('#user_submission').attr('disabled','disabled');
					$('#user-submission-items').prepend('<li> <span class="date">'+data.created_at+'</span><span class="filename">'+data.filename+'</span> </li>');
					$('#user_submission').val('');
					$('.drop > h4').hide(4000);
                },
                error: errorHandler = function() {
                    alert("Error");
                },
                // Form data
                data: formdata,
                //Options to tell JQuery not to process data or worry about content-type
                cache: false,
                contentType: false,
                processData: false
            }, 'json');
	return false;		
}

function change_user_module_status(userid,moduleid,courseid){
	console.log($( '#status-'+userid+'-'+moduleid+'-'+courseid+' option:selected' ).val());
	$('#status-message-'+userid+'-'+moduleid+'-'+courseid).html('Updating...');
	
	var formdata = new FormData(); 
	formdata.append("moduleID", moduleid); 
	formdata.append("courseID", courseid);
	formdata.append("userID", userid);	
	formdata.append("status", $( '#status-'+userid+'-'+moduleid+'-'+courseid+' option:selected' ).val());
	
	
   $.ajax({
		url: '/postUserModuleStatus',  //server script to process data
		type: 'POST',

		//Ajax events
		success: completeHandler = function(data) {
			//console.log(data.filetype);			
			$('#status-message-'+userid+'-'+moduleid+'-'+courseid).html(data.msg).hide(4000);
		},
		error: errorHandler = function(xhr, status, error) {
			 var err = eval("(" + xhr.responseText + ")");
			console.log(err.Message);
		},
		// Form data
		data: formdata,
		//Options to tell JQuery not to process data or worry about content-type
		cache: false,
		contentType: false,
		processData: false
	}, 'json');
}

//Delete Module
function delete_module(mid){
	if(confirm('Are you sure you want to delete this course? This can not be undone')){
		$('#module-panel-'+mid).remove();
		return false;
	}else{
		return false;
	}

}

//delete course
function delete_course(){
	if(confirm('Are you sure you want to delete this course? This can not be undone')){
		return true;
	}else{ 
		return false;
	}
}

//delete user
function delete_user(){
	if(confirm('All of the user information, submissions, feedback, enrollment, files will be deleted...\nAre you sure you want to delete this user? This cannot be undone')){
		return true;
	}else{ 
		return false;
	}
}

//upload certificates
function addNewCertificate(userid, courseID){

	var file = _('certificate_submission').files[0];  //alert(file.name+" | "+file.size+" | "+file.type);
	if (file.name == '') {
		$('.drop').prepend('<h4>Choose a file please.Accepted formats are MS Word, PDF, Image, PPT</h4>');
		return false;
	}
	var formdata = new FormData();
	formdata.append("file1", file);
	formdata.append("userID", userid);
	formdata.append("courseID", courseID);
	$('.drop').prepend('<h4>Certificate is uploading...please wait</h4>');
			$.ajaxSetup({
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			});
            $.ajax({
                url: '/addusercertificate',  //server script to process data
                type: 'POST',
                //Ajax events
                success: completeHandler = function(data) {
					//console.log(data.filetype);
					if(data.success == 'OK'){
						$('.drop > h4').html('Certificate successfully uploaded.');
						$('#certificate-items').prepend('<li id="cer-'+data.certificate_id+'"> <span class="date">'+data.created_at+'</span><span class="filename">'+data.filename+'</span> <span class="actionbar"><a href="#"  onclick="return deleteCertificate('+data.certificate_id+')">Delete</a></span></li>');
						$('#certificate_submission').val('');
						$('.drop > h4').hide(4000);
					}else{
						$('.drop > h4').html(data.errormessage);
					}
                },
                error: errorHandler = function(data) {
                    //$('.drop > h4').html(data.errormessage);
                },
                // Form data
                data: formdata,
                //Options to tell JQuery not to process data or worry about content-type
                cache: false,
                contentType: false,
                processData: false
            }, 'json');
	return false;
}


//delete certificate

function deleteCertificate(id){
		if(confirm('Certificate will be permanently deleted\nAre you sure you want to delete this? This cannot be undone')){
			$.ajax({
                url: '/deleteCertificate/'+id,  //server script to process data
                type: 'GET',
                //Ajax events
                success: completeHandler = function(data) {	
					$('li#cer-'+id).remove();				
					$('.drop > h4').html(data.msg).hide(8000);
                },
                error: errorHandler = function() {
                    alert("Error");
                },
                // Form data
                data: '',
                //Options to tell JQuery not to process data or worry about content-type
                cache: false,
                contentType: false,
                processData: false
            }, 'json');
		}else{ 
			return false;
		}	
            
		return false;	
}
$(document).ready(function() 
    { 
        $("#myTable").tablesorter(); 
		$( ".enroldate" ).datepicker({dateFormat: "yy-mm-dd"});
		$( "input[type=checkbox]" ).on( "click", function(){
				$('#enroldate-hidden'+$(this).val()).addClass('date-shown').removeClass();
		});
		
    } 
);

//upload feedback attachment
function addNewAttachment(userid){

    var file = _('attachment_submission').files[0];  //alert(file.name+" | "+file.size+" | "+file.type);
    if (file.name == '') {
        $('.drop').prepend('<h4>Choose a file please.Accepted formats are MS Word, PDF, Image, PPT</h4>');
        return false;
    }
    var formdata = new FormData();
    formdata.append("file1", file);
    formdata.append("userID", userid);
    $('.drop').prepend('<h4>Attachment is uploading...please wait</h4>');

    $.ajax({
        url: '/addfeedbackattachment',  //server script to process data
        type: 'POST',
        //Ajax events
        success: completeHandler = function(data) {
            //console.log(data.filetype);
            if(data.success == 'OK'){
                $('.drop > h4').html('Attachment successfully uploaded.');
                $('#attachment-items').prepend('<li id="feedbackAttachment-'+data.attachment_id+'"> <span class="filename">'+data.filename+'</span> <span class="actionbar"><a href="#"  onclick="return deleteAttachment('+data.attachment_id+')">Delete</a></span></li>');
                $('#attachment_submission').val('');
                var existing;
                existing = $('#feedbackAttachmentsID').val();
                if(existing == "")
                    $('#feedbackAttachmentsID').val(data.attachment_id);
                else
                    $('#feedbackAttachmentsID').val(existing+','+data.attachment_id);

                $('.drop > h4').hide(4000);
            }else{
                $('.drop > h4').html(data.errormessage);
            }
        },
        error: errorHandler = function(data) {
            //$('.drop > h4').html(data.errormessage);
        },
        // Form data
        data: formdata,
        //Options to tell JQuery not to process data or worry about content-type
        cache: false,
        contentType: false,
        processData: false
    }, 'json');
    return false;
}

//delete feedback attachment

function deleteAttachment(id){
    if(confirm('Attachment will be permanently deleted\nAre you sure you want to delete this? This cannot be undone')){
        $.ajax({
            url: '/deleteattachment/'+id,  //server script to process data
            type: 'GET',
            //Ajax events
            success: completeHandler = function(data) {
                $('li#feedbackAttachment-'+id).remove();
                $('.drop > h4').html(data.msg).hide(8000);
            },
            error: errorHandler = function() {
                alert("Error");
            },
            // Form data
            data: '',
            //Options to tell JQuery not to process data or worry about content-type
            cache: false,
            contentType: false,
            processData: false
        }, 'json');
    }else{
        return false;
    }

    return false;
}

function playVideo(videokey, type) {

}