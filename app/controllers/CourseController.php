<?php
use \Anam\PhantomMagick\Converter;
use \Knp\Snappy\Pdf;
class CourseController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function __construct()
	{
		//$this->beforeFilter();
		
		
		//update last visited url for logged in user
/*		$logged_user = Auth::user();
		if(Auth::check() && Route::currentRouteAction()!='logout') 
		$update_url = $logged_user->updateLastVisitedURL(Request::url());*/	
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'courses';
		$data['user'] = Auth::user();
		$data['template_part'] = 'courselist';
		$data['logged_in'] = (Auth::id() === null)?'0':'1';
		$data['search'] = '';
		$data['sortby'] = '';
		$data['order'] = '';

		$limit = 20;
		$page = (Input::get('page')!='')?Input::get('page'):1;
		if(Auth::user()->user_type == 0) {

			if(Input::has('search') or Input::has('sortby') or Input::has('order')){
				$data['search'] = Input::get('search');
				$data['sortby'] = Input::get('sortby');
				$data['order'] = Input::get('order');
				if($data['search'] != ''){
					$data['courses'] = Course::where('title','LIKE','%'.$data['search'].'%')
							->orWhere('ref_no','LIKE','%'.$data['search'].'%')
							->orWhere('qualification','LIKE','%'.$data['search'].'%')
							->orderBy($data['sortby'],$data['order'])
							->paginate($limit);

					$data['courses_total'] =  Course::where('title','LIKE','%'.$data['search'].'%')
							->orWhere('ref_no','LIKE','%'.$data['search'].'%')
							->orWhere('qualification','LIKE','%'.$data['search'].'%')
							->orderBy($data['sortby'],$data['order'])
							->count();
				}
				else{
					$data['courses'] = Course::where('type',1)->orderBy('title','asc')->paginate($limit);
					$data['courses_total'] =  Course::where('type',1)->count();
				}


				$data['courses']->setBaseUrl('/courses');
			}else{
				$data['courses'] = Course::where('type',1)->orderBy('title','asc')->paginate($limit);
				$data['courses_total'] =  Course::where('type',1)->count();
				$data['courses']->setBaseUrl('/courses');
			}




			$data['courses_current_total'] = (($page*$limit)>$data['courses_total'])?$data['courses_total']:($page)*$limit;
			$data['course_current_start'] = (($page-1)*$limit)+1;


		}else{

			/*$user_courses = DB::select( DB::raw("SELECT courses.* FROM courses where courses.type=1 inner join user_courses on  user_courses.courseID=courses.id AND user_courses.userID=".Auth::user()->id." order by courses.created_at DESC"));*/


			//find the first module of each course and add those to course objects
			$data['user_courses'] = array();
			/*if(count($user_courses)>0){
				$i=0;
				foreach( $user_courses as $course){
					 $module = DB::table('modules')->where('courseID',$course->id)->first();
					 $course->moduleID = $module->id;
					 array_push($data['user_courses'],$course);
					 $i++;
				}
			}*/

			$data['courses'] = Course::where('type',1)
										->join('user_courses','courses.id','=','user_courses.courseID')
										->where('user_courses.userID','=',Auth::user()->id)
										->paginate($limit);

			if(count($data['courses'])>0){
				$i=0;
				foreach( $data['courses'] as $course){
					$module = DB::table('modules')->where('courseID',$course->id)->first();
					$course->moduleID = $module->id;
					array_push($data['user_courses'],$course);
					$i++;
				}
			}
			$data['courses_total'] = Course::where('type',1)
					->join('user_courses','courses.id','=','user_courses.courseID')
					->where('user_courses.userID','=',Auth::user()->id)
					->count();
			$data['courses_current_total'] = (($page*$limit)>$data['courses_total'])?$data['courses_total']:($page)*$limit;
			$data['course_current_start'] = ($data['courses_total']!=0)?(($page-1)*$limit)+1:0;
		}
		return View::make('courses',$data);

	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function qualifications()
	{
		//
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'qualifications';
		$data['user'] = Auth::user();
		$data['template_part'] = 'qualifications';
		$data['logged_in'] = (Auth::id() === null)?'0':'1';
		$data['search'] = '';
		$data['sortby'] = '';
		$data['order'] = '';

		$limit = 20;
		$page = (Input::get('page')!='')?Input::get('page'):1;
		if(Auth::user()->user_type == 0) {

			if(Input::has('search') or Input::has('sortby') or Input::has('order')){
				$data['search'] = Input::get('search');
				$data['sortby'] = Input::get('sortby');
				$data['order'] = Input::get('order');
				if($data['search'] != ''){
					$data['courses'] = Course::where('title','LIKE','%'.$data['search'].'%')
							->orWhere('ref_no','LIKE','%'.$data['search'].'%')
							->orWhere('qualification','LIKE','%'.$data['search'].'%')
							->orderBy($data['sortby'],$data['order'])
							->paginate($limit);

					$data['courses_total'] =  Course::where('title','LIKE','%'.$data['search'].'%')
							->orWhere('ref_no','LIKE','%'.$data['search'].'%')
							->orWhere('qualification','LIKE','%'.$data['search'].'%')
							->orderBy($data['sortby'],$data['order'])
							->count();
				}
				else{
					$data['courses'] = Course::where('type',2)->orderBy('title','asc')->paginate($limit);
					$data['courses_total'] =  Course::where('type',2)->count();
				}


				$data['courses']->setBaseUrl('/qualifications');
			}else{
				$data['courses'] = Course::where('type',2)->orderBy('title','asc')->paginate($limit);
				$data['courses_total'] =  Course::where('type',2)->count();
				$data['courses']->setBaseUrl('/qualifications');
			}




			$data['courses_current_total'] = (($page*$limit)>$data['courses_total'])?$data['courses_total']:($page)*$limit;
			$data['course_current_start'] = (($page-1)*$limit)+1;


		}else{

			/*$user_courses = DB::select( DB::raw("SELECT courses.* FROM courses where courses.type=2 inner join user_courses on  user_courses.courseID=courses.id AND user_courses.userID=".Auth::user()->id." order by courses.created_at DESC"));*/


			//find the first module of each course and add those to course objects
			$data['user_courses'] = array();
			/*if(count($user_courses)>0){
				$i=0;
				foreach( $user_courses as $course){
					$module = DB::table('modules')->where('courseID',$course->id)->first();
					$course->moduleID = $module->id;
					array_push($data['user_courses'],$course);
					$i++;
				}
			}*/

			$data['courses'] = Course::where('type',2)
					->join('user_courses','courses.id','=','user_courses.courseID')
					->where('user_courses.userID','=',Auth::user()->id)
					->paginate($limit);

			if(count($data['courses'])>0){
				$i=0;
				foreach( $data['courses'] as $course){
					$module = DB::table('modules')->where('courseID',$course->id)->first();
					$course->moduleID = $module->id;
					array_push($data['user_courses'],$course);
					$i++;
				}
			}

			$data['courses_total'] = Course::where('type',2)
					->join('user_courses','courses.id','=','user_courses.courseID')
					->where('user_courses.userID','=',Auth::user()->id)
					->count();
			$data['courses_current_total'] = (($page*$limit)>$data['courses_total'])?$data['courses_total']:($page)*$limit;
			$data['course_current_start'] = ($data['courses_total']!=0)?(($page-1)*$limit)+1:0;
		}
		return View::make('courses',$data);

	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function accreditations()
	{
		//
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'accreditations';
		$data['user'] = Auth::user();
		$data['template_part'] = 'accreditations';
		$data['logged_in'] = (Auth::id() === null)?'0':'1';
		$data['search'] = '';
		$data['sortby'] = '';
		$data['order'] = '';

		$limit = 20;
		$page = (Input::get('page')!='')?Input::get('page'):1;
		if(Auth::user()->user_type == 0) {

			if(Input::has('search') or Input::has('sortby') or Input::has('order')){
				$data['search'] = Input::get('search');
				$data['sortby'] = Input::get('sortby');
				$data['order'] = Input::get('order');
				if($data['search'] != ''){
					$data['courses'] = Course::where('title','LIKE','%'.$data['search'].'%')
							->orWhere('ref_no','LIKE','%'.$data['search'].'%')
							->orWhere('qualification','LIKE','%'.$data['search'].'%')
							->orderBy($data['sortby'],$data['order'])
							->paginate($limit);

					$data['courses_total'] =  Course::where('title','LIKE','%'.$data['search'].'%')
							->orWhere('ref_no','LIKE','%'.$data['search'].'%')
							->orWhere('qualification','LIKE','%'.$data['search'].'%')
							->orderBy($data['sortby'],$data['order'])
							->count();
				}
				else{
					$data['courses'] = Course::where('type',3)->orderBy('title','asc')->paginate($limit);
					$data['courses_total'] =  Course::where('type',3)->count();
				}


				$data['courses']->setBaseUrl('/accreditations');
			}else{
				$data['courses'] = Course::where('type',3)->orderBy('title','asc')->paginate($limit);
				$data['courses_total'] =  Course::where('type',3)->count();
				$data['courses']->setBaseUrl('/accreditations');
			}




			$data['courses_current_total'] = (($page*$limit)>$data['courses_total'])?$data['courses_total']:($page)*$limit;
			$data['course_current_start'] = (($page-1)*$limit)+1;


		}else{

			/*$user_courses = DB::select( DB::raw("SELECT courses.* FROM courses where courses.type=3 inner join user_courses on  user_courses.courseID=courses.id AND user_courses.userID=".Auth::user()->id." order by courses.created_at DESC"));*/


			//find the first module of each course and add those to course objects
			$data['user_courses'] = array();
			/*if(count($user_courses)>0){
				$i=0;
				foreach( $user_courses as $course){
					$module = DB::table('modules')->where('courseID',$course->id)->first();
					$course->moduleID = $module->id;
					array_push($data['user_courses'],$course);
					$i++;
				}
			}*/

			$data['courses'] = Course::where('type',3)
					->join('user_courses','courses.id','=','user_courses.courseID')
					->where('user_courses.userID','=',Auth::user()->id)
					->paginate($limit);

			if(count($data['courses'])>0){
				$i=0;
				foreach( $data['courses'] as $course){
					$module = DB::table('modules')->where('courseID',$course->id)->first();
					$course->moduleID = $module->id;
					array_push($data['user_courses'],$course);
					$i++;
				}
			}

			$data['courses_total'] = Course::where('type',3)
					->join('user_courses','courses.id','=','user_courses.courseID')
					->where('user_courses.userID','=',Auth::user()->id)
					->count();
			$data['courses_current_total'] = (($page*$limit)>$data['courses_total'])?$data['courses_total']:($page)*$limit;
			$data['course_current_start'] = ($data['courses_total']!=0)?(($page-1)*$limit)+1:0;
		}
		return View::make('courses',$data);

	}


	/**
	 * Add new Course form view.
	 *
	 * @return Response
	 */
	public function addCourse()
	{
		//
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'courses';
		$data['logged_in'] = (Auth::id() === null)?'0':'1';	
		$data['template_part'] = 'addcourse';
		
		return View::make('courses',$data);
	}

	/**
	 * Add new Course processing.
	 *
	 * @return Response
	 */
	public function postAddCourse()
	{
		//
		if(Input::has('title')){
			//save course data
			$course = new Course();
			$course->title = Input::get('title');
			$course->description = nl2br(Input::get('description'));
			$course->qualification = Input::get('qualification');
			$course->ref_no = Input::get('refno');
			$course->expiry_date = (Input::get('expiry_date'))?Input::get('expiry_date'):date('Y-m-d h:i:s',strtotime('+2 years'));
			$slug = strtolower(str_replace(' ','-',Input::get('title')));
			$check_slug = DB::table('courses')
							->where('course_slug',$slug)
							->where('id',Input::get('ID'))
							->count();
			
			if($check_slug>0){
				$slug_alias = $check_slug+1;
				$slug = $slug.'-'.$slug_alias;		
			}
			else{
				$course->course_slug = $slug;	
			}
			$course->save();		
			$course_id = DB::getPdo()->lastInsertId();	
			
			//save module data
			$module_count = Input::get('count_module');	
			
			for($i=0; $i<$module_count; $i++){
				$module = new Module();
				$module->title = Input::get('module_'.($i+1).'_title');
				if($module->title=="")
				continue;
				
				$module->description = nl2br(Input::get('module_'.($i+1).'_description'));
				$module->courseID = $course_id;
				if($i==0)
				    $module->is_overview = 1;
				else
				    $module->is_overview = 0;


				//check if this certification module Input::get('module_certificate_'.($i+1))
                $cer = Input::get('module_certificate_'.($i+1));
                if(isset($cer))
                    $module->is_certificate =1;
                else
                    $module->is_certificate =0;

				$module->save();
				$module_id = DB::getPdo()->lastInsertId();	
				//DB::table('course_modules')->insert(array('moduleID'=>$module_id,'courseID'=>$course_id));
				
				//Resources id for this module
				$resource_ids = explode(',',Input::get('module_'.($i+1).'_resource_ids'));
				if(count($resource_ids)>0){
					foreach($resource_ids as $ri){
						DB::table('resources')->where('ID',$ri)->update(array('moduleID'=>$module_id,'courseID'=>$course_id));
					}
				}
			}
					
		}
	
		return Redirect::to('courses');
	}

	/**
	 * Display the specified resource.
	 *
	 * @return Response
	 */
	public function show($slug,$moduleid)
	{
 
		$moduleid = base64_decode($moduleid);
		//$sl = base64_decode($id);
		$data['selected'] = 'users';
		$data['template_part'] = 'showcourse';
		$data['module_sidebar'] = 'yes';
		$data['siteurl'] = Config::get('app.url');
		$data['user'] = Auth::user();
		$data['module'] = Module::find($moduleid);
		$data['course'] = DB::table('courses')->where('course_slug',$slug)->first();

		$data['upload_disabled'] = 11;//$data['module']->getUserModuleStatus($data['user']->id,$data['course']->id);
		$courseid =	$data['course']->id;

        //Determine user validity still permit to visit the course
        $can_view_course = DB::table('user_courses')
            ->where('userID',$data['user']->id)
            ->where('courseID',$data['course']->id)
            ->where('exp_date','>=',date('Y-m-d'))
            ->first();

        if(count($can_view_course)<1){
            return Redirect::to('courses')->with('flash_notice','Your desired course has expired');
        }

        //determine module if its certification module
        $cer_mod = DB::table('modules')
            ->where('courseID','=',$data['course']->id)
            ->where('ID','=',$moduleid)
            ->where('is_certificate','=',1)
            ->first();

        if(count($cer_mod)>0){
            //check certificate payment

            /*$payment_status = DB::table('course_payments')
                ->where('course_id','=',$data['course']->id)
                ->where('user_id','=',Auth::user()->id)
                ->first();
            if(count($payment_status)==0){
                return Redirect::to('courses')->with('flash_notice','Please complete the payment for the certification.
                Also check if you have any module left to be eligible for the certificatio');
            }*/
        }



		//Find user modules
		$data['user_modules'] = array();
		$data['user_modules'] = $data['user']->getUserCourseModules($data['course']->id);


		$data['resources'] = DB::table('resources')->where('moduleID',$moduleid)->get();
		$data['user_submissions'] = DB::table('user_submissions')->where('moduleID',$moduleid)->Where('userID',Auth::user()->id)->orderby('updated_at','desc')->get();
		
		$data['user_feedbacks'] = $data['user']->getFeedbackOnUser(Auth::user()->id,$moduleid,$data['course']->id);
		//print_r($data['user_feedbacks']);
		//exit;

		//count the new feeedback
		$counter = 1;
		$previous_module_status = '';
		foreach($data['user_modules'] as $um){
			if($um->is_certificate or $um->is_overview)
				continue;
			if($counter == 1 and $um->id == $moduleid and $um->module_status == 10){
				$data['upload_disabled'] = 12;
			}

			//accreditation module
			if($counter == 1 and $um->id == $moduleid and $um->module_status == 10 && $um->type == 3){
				$data['upload_disabled'] = 12;
			}
			if($counter == 1 and $um->id == $moduleid and $um->module_status == 3 && $um->type == 3){
				$data['upload_disabled'] = 3;
			}

			if($counter == 1 and $um->id == $moduleid and $um->module_status ==11 && $um->type == 3){
				$data['upload_disabled'] = 12;
			}

			if($counter > 1 and $previous_module_status == 3 and $um->id == $moduleid and $um->module_status == 10 && $um->type != 3){
				$data['upload_disabled'] = 12;
			}

			if($previous_module_status == 3 and $um->module_status == 10 and $um->id == $moduleid)
				$data['upload_disabled'] = 12;

			if($um->module_status == 3 and $um->id == $moduleid)
				$data['upload_disabled'] = 3;


			$counter++;
			$count_feedback =  count($data['user']->getUnreadFeedbacks(Auth::user()->id,$um->id));
			$um->new_feedback_count = $count_feedback;
			$previous_module_status = $um->module_status;
		}
											

		$user_courses = DB::select( DB::raw("SELECT courses.* FROM courses inner join user_courses on  user_courses.courseID=courses.id AND user_courses.userID=".$data['user']->id." order by courses.created_at DESC"));
		$data['user_courses'] = array();
		//find t first module of each course and add those to course objects
		if(count($user_courses)>0){
			$i=0;
			foreach( $user_courses as $course){							 
				 $module = DB::table('modules')->where('courseID',$course->id)->first();				 
				 $course->moduleID = $module->id;
				 array_push($data['user_courses'],$course);
				 $i++;
			}
		}
		
		$data['user_current_course'] = DB::table('courses')
				->join('user_courses','user_courses.courseID','=','courses.id')
				->where('courses.id',$courseid)
				->where('user_courses.userID',$data['user']->id)
				->first();

		$data['user_left_modules'] = $data['module']->getUserLeftModuleCount($data['user']->id,$courseid);
        
        $usercourse = DB::table('user_courses')
		->where('userID',Auth::user()->id)
		->where('courseID',$courseid)
		->first(); 

		$data['lastassessment'] = $usercourse->last_assessment_date; 
		  
		return View::make('courses',$data);			

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($courseid_type)
	{
		//
		$data = array();
		$data['siteurl'] = Config::get('app.url');
        
		
		$data['template_part'] = 'editcourse';
		$data['logged_in'] = (Auth::id() === null)?'0':'1';	
        $courseid_type_extract = explode('-',$courseid_type);
        $id = $courseid_type_extract[0];
        
        if($courseid_type_extract[1] == 1)
          $data['selected'] = 'courses';
        else if($courseid_type_extract[1]== 3)
            $data['selected'] = 'accreditations';
        else if($courseid_type_extract[1]== 2)
            $data['selected'] = 'qualifications';
        
		//return Redirect::to('courses');
		
		
		$data['course'] = DB::table('courses')->where("courses.id",$id)->first();
	
		$data['modules'] = DB::table('modules')->where('modules.courseID','=',$id)->where('modules.is_certificate','=',0)->get();


		$data['certification_module'] = DB::table('modules')->where('modules.courseID','=',$id)->where('modules.is_certificate','=',1)->first();
        if(count($data['certification_module'])>0){
            $data['certification_module_id'] = $data['certification_module']->id;
            $data['certification_module_title'] = $data['certification_module']->title;
            $data['certification_module_description'] = $data['certification_module']->description;
        }
        else{
            $module = new Module();
            $module->title = 'Certification';
            $module->description = 'Enter certification details instructions here';
            $module->courseID = $id;
            $module->is_overview = 0;
            $module->is_certificate = 1;

            $module->save();
            $module_id = DB::getPdo()->lastInsertId();
            $data['certification_module_title'] = 'Certification';
            $data['certification_module_id'] = $module_id;
            $data['certification_module_description'] = 'Enter certification details instructions here';

			return Redirect::to('course/edit/'.$id);
        }


		$data['count_module']= count($data['modules'])+1;//DB::table('modules')->where('modules.courseID','=',$id)->count();



		$data['resources'] = DB::table('modules')		
		->where('modules.courseID','=',$id)
		->join('resources','modules.ID','=','resources.moduleID')
		->select('resources.id','resources.title','resources.moduleID','resources.attachment_name','resources.attachment_type')
		->get();


	
		return View::make('courses',$data);
		
	}

	/**
	 * Course Edit action.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function postEdit($id)
	{
		//update action
		if(Input::has('title')){
		//Update course data
		$course = Course::find(Input::get('ID'));
		$course->title = Input::get('title');
		$course->description = Input::get('description');
		$course->qualification = Input::get('qualification');
		$course->ref_no = Input::get('refno');
		//$course->expiry_date = (Input::get('expiry_date'))?Input::get('expiry_date'):date('Y-m-d h:i:s',strtotime('+2 years'));
		$slug = strtolower(str_replace(' ','-',Input::get('title')));
		
		if($course->course_slug == NULL){
			$course->course_slug = $slug;

		}else{
			$check_slug = DB::table('courses')
							->where('course_slug',$slug)
							->where('id','!=',Input::get('ID'))
							->count();
			
			if($check_slug>0){
				$slug_alias = $check_slug+1;
				$slug = $slug.'-'.$slug_alias;		
			}
			
			$course->course_slug = $slug;
		}
		
		$course->save();		
		$course_id = Input::get('ID');	

		//save module data
		$module_count = Input::get('count_module');	
		$intended_module_to_delete = Input::get('module_delete');
		for($i=0; $i<$module_count; $i++){
			$m_id = Input::get('module_'.($i+1).'_id');
			if(count($intended_module_to_delete)>0 AND in_array($m_id,$intended_module_to_delete)){
				$del = DB::table('modules')->where('id',$m_id)->delete();
				$del = DB::table('user_module_feedback')->where('moduleID',$m_id)->where('courseID',$course->id)->delete();
				$del = DB::table('user_module_status')->where('moduleID',$m_id)->where('courseID',$course->id)->delete();
				continue;
			}


			
			$action = Input::get('module_'.($i+1).'_action');
			$module = ($action=="update")?Module::find($m_id):new Module();
			$module->title = Input::get('module_'.($i+1).'_title');

			if($module->title=="")
				continue;

			$module->description = Input::get('module_'.($i+1).'_description');
			$module->courseID = $course_id;		
			/*if($i==0)
			$module->is_overview = 1;
			else
			$module->is_overview = 0;


			$module->is_certificate = 0;*/
					
			$module->save();
			$module_id = ($action=="update")?$m_id:DB::getPdo()->lastInsertId();

			//DB::table('course_modules')->insert(array('moduleID'=>$module_id,'courseID'=>$course_id));
			
			//Resources id for this module
			$resource_ids = explode(',',Input::get('module_'.($i+1).'_resource_ids'));


				
			if(count($resource_ids)>0){
				foreach($resource_ids as $ri){					
					DB::table('resources')->where('id',$ri)->update(array('moduleID'=>$module_id,'courseID'=>$course_id));
				}
			}
		 }
			return Redirect::to('course/edit/'.$id.'-'.$course->type)->withFlashNotice('Course updated successfully');		
		}
		
	}
	
	/**
	 * Upload course module resources.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function addResource()
	{
		$target_resource_id = Input::get('resource_id');
		$resource_title = Input::get('resource_title');
		$resource_action = Input::get('resource_action');
        $video_code = Input::get('video_code');
		
		$destinationPath = "uploads/user-submission/r-".$target_resource_id;
		if(!is_dir($destinationPath))
			mkdir($destinationPath,0755);	
				
		//Add resource info into database
		$resource = ($resource_action=='update' AND $target_resource_id!='')?Resource::find($target_resource_id):new Resource();
		$resource->title = $resource_title;
		$resource->attachment_path = $destinationPath;
		//Process file upload		
		if(Input::hasFile('file1') && $video_code==""){
			$fileName = $_FILES["file1"]["name"];
			$file_extension = Input::file('file1')->getClientOriginalExtension();

            if(!in_array(strtolower($file_extension), array('pdf','ppt','pptx','doc','docx','story','mp4','flv','avi','wmv','gif','jpg','jpeg','png','.zip'))){
                $response = array(
                    'msg'=>"Invalid file type. Allowed extension 'pdf','ppt','pptx','doc','docx','story','mp4','flv','avi','wmv','gif','jpg','jpeg','png'",
                    'title'=>$resource_title,
                    'resourceaction'=> 'error'
                );
                return Response::json($response);

            }

           /*
            * if($size>3000000){
                $response = array(
                    'msg'=>"File size is larger than 3mb",
                    'title'=>$resource_title,
                    'resourceaction'=> 'error'
                );
                return Response::json($response);
            }*/

			Input::file('file1')->move($destinationPath, $fileName);
		}

		
		if(Input::hasFile('file1') && $video_code==""){
			$resource->attachment_name = $fileName;
			$resource->attachment_type = $file_extension;
		}else{
            $parsed_url = parse_url($video_code);
            if($parsed_url['host'] == "www.youtube.com" or $parsed_url['host'] == "youtube.com"){
                $qvars = explode('=',$parsed_url['query']);
                $resource->attachment_name = $qvars[1];
                $resource->attachment_type = 'youtube';
            }
            else if($parsed_url['host'] == "www.vimeo.com" or $parsed_url['host'] == "vimeo.com") {
                $qvars = explode('/',$video_code);
                $resource->attachment_name = $qvars[count($qvars)-1];
                $resource->attachment_type = 'vimeo';
            }else{
                $response = array(
                    'msg'=>"Please select only youtube or vimeo",
                    'title'=>$resource_title,
                    'resourceaction'=> 'error'
                );
                return Response::json($response);
            }




        }

		$resource->save();
		$resource_id = ($resource_action=='update')?$target_resource_id:DB::getPdo()->lastInsertId();
		$response = array(
                        'msg'=>'Successfully uploaded',
                        'title'=>$resource_title,
                        'resourceaction'=> $resource_action
                        );
		if(Input::hasFile('file1')){
			$response['filetype'] = $resource->attachment_type;
									
		}

        if(Input::has('video_code')){
            $response['filetype'] = $resource->attachment_type;

        }
		$response['resourceid'] = $resource_id;
		return Response::json($response);		
	}
	
	/**
	 * Upload course module resources.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteResource($id)
	{
		//Delete resource info into database
		DB::table('resources')->where('id',$id)->delete();
		return Response::json(array(
									'msg'=>'Successfully deleted',
									'resource_id'=>$id
									)
		);		
	}	
	
	/**
	 * Delete Courses
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteCourse($id)
	{
		//Delete resource info into database
		DB::table('courses')->where('id',$id)->delete();
		DB::table('user_courses')->where('courseID',$id)->delete();
		$redirect =  Request::server('HTTP_REFERER');;
		return Redirect::to($redirect)->withFlashNotice('Course deleted successfully');
	}

	/**
	 * Self assessment
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function selfAssessment($id)
	{
		//Delete resource info into database
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = '';
		$data['logged_in'] = (Auth::id() === null)?'0':'1';
		$data['template_part'] = 'assessment';
		$data['course'] = Course::find($id);

		$usercourses = DB::table('user_courses')
		->where('userID',Auth::user()->id)
		->where('courseID',$id)
		->first();

		$course_module_info = DB::table('modules')
				->where('modules.courseID',$id)
				->where('modules.is_overview','1')
				->first();




		$lastassessment = $usercourses->last_assessment_date;
		$redirect = $data['siteurl'].'/course/'.$data['course']->course_slug.'/'.base64_encode($course_module_info->id);

		/*if(strtotime(date('Y-m-d h:i:s')) < strtotime($lastassessment.' +1 day'))
			return Redirect::to($redirect)->withFlashNotice('You can re-take assessment on '.$usercourses->last_assessment_date);*/

		$data['ecommerce_course_id'] = $data['course']->ecommerce_course_id;
		return View::make('courses',$data);
		//return Redirect::to('courses')->withFlashNotice('Course deleted successfully');
	}


	/**
	 * Get the question for quiz
	 */

	public function getAssessmentQuestions($ecommerce_courseid){

		$json = json_decode(file_get_contents('http://2017.harpar.com/wp-admin/admin-ajax.php?action=getQuestionareLMS&post_id='.$ecommerce_courseid), true);

		return Response::json($json);
	}

	/**
	 * Get the question for quiz
	 */

	public function logAssessmentStatus($courseid,$passed){

		//$json = json_decode(file_get_contents('http://2017.harpar.com/wp-admin/admin-ajax.php?action=getQuestionareLMS'), true);

		$q = DB::table('user_courses')
				->where('userID', Auth::user()->id)
				->where('courseID', $courseid)
				->update(array('last_assessment_date' => date('Y-m-d h:i:s'),'certificate_ready'=>1));
        
        $course = Course::find($courseid);
        $user_course = DB::table('user_courses')
				->where('userID', Auth::user()->id)
				->where('courseID', $courseid)
				->first();

		if($passed=='passed'){
			
            
            $snappy72 = new Pdf('/usr/bin/wkhtmltopdf');
            $snappy72->setOption('dpi',119);
            $snappy72->setOption('image-dpi',72);
            $snappy72->setOption('margin-bottom','0px');
            $snappy72->setOption('margin-left','0px');
            $snappy72->setOption('margin-right','0px');
            $snappy72->setOption('margin-top','0px'); 
            $snappy72->setOption('page-width','3508px');
            $snappy72->setOption('page-height','2401px'); 
            $snappy72->setOption('user-style-sheet',public_path().'/css/pdf-style.css');
            $name = Auth::user()->firstname.' '.Auth::user()->lastname;
            $subject = $course->title;
            $startdate = date('Y-m-d', strtotime($user_course->enroll_date));
            $enddate = date('Y-m-d');
            $html = '<div class="certificate">';
            $html .='<div class="name">'.$name.'</div>';
            $html .='<div class="subject">'.$subject.'</div>';
            $html .='<div class="startdate">'.$startdate.'</div>';
            $html .='<div class="enddate">'.$enddate.'</div>';
            $html .='</div>';
            $path = public_path().'/user-certificates/';
            $filename = '/certificate-'.time().'.pdf';
            
            if(!is_dir($path))
                mkdir($path, 0755);
            
            $download = $snappy72->generateFromHtml($html, $path.$filename);
            
            DB::table('user_certificates')->insert(array(
					'filename' => $filename,
					'filepath' => $path,
					'userID' => AUth::user()->id,
					'courseID'=>$courseid,
					'approved_by' => '',
					'created_at' => date('Y-m-d h:i:s'),
					'updated_at' => date('Y-m-d h:i:s')
			));
            
            //Send mail with username
			$setting = Setting::where('name','=','certificateDownloadEmail')->first();
            $maildata['firstname'] = Auth::user()->firstname;
            $maildata['lastname'] = Auth::user()->lastname;
            $maildata['coursetitle'] = $course->title;
            
            if(!isset($setting->content))
                $maildata['email_content'] = '';
            else
			 $maildata['email_content'] = $setting->renderContent($setting->content, $maildata);
            
            $maildata['name'] = $name;
            $maildata['to'] = Auth::user()->email;
            $maildata['path'] = $path;
            $maildata['filename'] = $filename;
            
            Mail::send('emails.certificateDownload', $maildata, function($message) use($maildata) {
			     $message->to( $maildata['to'], $maildata['name'])->subject('Congratulations! You have successfully completed the assessment on '.$maildata['coursetitle']);
                 $message->attach($maildata['path'].$maildata['filename']);
                       
            });
                
            }
		    return Response::json(array('status'=>'OK'));
	}

	/**
	 * PDF
	 */
	public function downloadmypdf(){
        
        $snappy72 = new Pdf('/usr/bin/wkhtmltopdf');
        $snappy72->setOption('dpi',119);
        $snappy72->setOption('image-dpi',72);
        $snappy72->setOption('margin-bottom','0px');
        $snappy72->setOption('margin-left','0px');
        $snappy72->setOption('margin-right','0px');
        $snappy72->setOption('margin-top','0px'); 
        $snappy72->setOption('page-width','3508px');
        $snappy72->setOption('page-height','2401px'); 
        $snappy72->setOption('user-style-sheet',public_path().'/css/pdf-style.css');
        $name = "Md Manzurul Haque";
        $subject = "CMI on Leading And Coaching";
        $startdate = "2015-01-12";
        $enddate = "2017-01-12";
        $html = '<div class="certificate">';
        $html .='<div class="name">'.$name.'</div>';
        $html .='<div class="subject">'.$subject.'</div>';
        $html .='<div class="startdate">'.$startdate.'</div>';
        $html .='<div class="enddate">'.$enddate.'</div>';
        $html .='</div>';
        echo $snappy72->generateFromHtml($html, public_path().'/file-'.time().'.pdf');

	}
}
