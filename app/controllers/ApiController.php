<?php

/**
 * Created by PhpStorm.
 * User: Manzurul
 * Date: 10/17/2016
 * Time: 4:58 AM
 */

class ApiController extends BaseController
{
    /*protected $apiMethods = [
        'checkuser' => [
            'keyAuthentication' => true,
            'level' => 1,
            'limits' => [
                // The variable below sets API key limits
                'key' => [
                    'increment' => '1 hour',
                    'limit' => 10
                ],
                // The variable below sets API method limits
                'method' => [
                    'increment' => '1 day',
                    'limit' => 1000
                ]
            ]
        ],

        'getAllCourses' => [
            'keyAuthentication' => true,
            'level' => 1,
            'limits' => [
                // The variable below sets API key limits
                'key' => [
                    'increment' => '1 hour',
                    'limit' => 10
                ],
                // The variable below sets API method limits
                'method' => [
                    'increment' => '1 day',
                    'limit' => 1000
                ]
            ]
        ],

        'createCourse' => [
            'keyAuthentication' => true,
            'level' => 1,
            'limits' => [
                // The variable below sets API key limits
                'key' => [
                    'increment' => '1 hour',
                    'limit' => 10
                ],
                // The variable below sets API method limits
                'method' => [
                    'increment' => '1 day',
                    'limit' => 100
                ]
            ]
        ],

        'createUser' => [
            'keyAuthentication' => true,
            'level' => 1,
            'limits' => [
                // The variable below sets API key limits
                'key' => [
                    'increment' => '1 hour',
                    'limit' => 10
                ],
                // The variable below sets API method limits
                'method' => [
                    'increment' => '1 day',
                    'limit' => 10
                ]
            ]
        ]
    ];*/
    /**
     * Constructor
     *
     * @return Response
     */
    public function __construct()
    {
        //update last visited url for logged in user
        /*		$logged_user = Auth::user();
                if(Auth::check() && Route::currentRouteAction()!='logout')
                $update_url = $logged_user->updateLastVisitedURL(Request::url());*/

        $this->beforeFilter(function () {
            $apikey =Input::get('apikey');

            $valid = ApiKey::where('key', '=', $apikey)->first();

            if (count($valid) == 0) {
                // ApiKey not found
                return Response::json(array('success'=>'error','msg'=>'Invalid Api Key'));
            }
        });
    }
    /**
     * Check user using email address
     *
     */
    public function checkuser(){

        $user_email = Input::get('email');
        if($user_email == ''){
            return Response::json(array('success'=>'FAIL','msg'=>'Please provide a valid email address'));
        }

        $exist = User::where('email','=',$user_email)->first();
        if(count($exist)>0){
            return Response::json(array('success'=>'OK','user_id'=>$exist->id));
        }else{
            return Response::json(array('success'=>'FAIL'));
        }

    }

    /**
     * Portal courses list to synchronise course with e-commerce section
     *
     */

    public function getAllCourses(){

        $course = new Course();
        $all = $course::get(['id', 'title','ref_no'])->toJson();
        if(count($all)>0){
            return Response::json(array('success'=>'OK','msg'=>$all));
        }


    }

    /**
     * Create new course with basic data
     * params
     * title, description, qualification,refno, course overview title
     */
    public function createCourse(){
        $coursetitle = Input::get('title');
        $ctitle = Input::get('title');
        $lms_course_id = '';
        
        $exist = DB::table('courses')->where('title','LIKE','%'.$ctitle.'%')->first();
        if(count($exist)==0)
        $lms_course_id = 0;
        else
        $lms_course_id = $exist->id;
        //return $course_id;
        if($lms_course_id != 0 OR $lms_course_id!=''){ 
            $course = Course::find($exist->id);
            $lms_course_id = $exist->id;
            $course->title = Input::get('title');
            $course->description = Input::get('description');
            $course->qualification = Input::get('qualification');
            $course->ref_no = Input::get('refno');
            $course->type = Input::get('type');
            $course->ecommerce_course_id = Input::get('ecommerce_course_id');
            $course->save();
            
            $overview_module = DB::table('modules')->where('courseID',$lms_course_id)->where('is_overview',1)->first();
            
            
            
            $resource_title = Input::get('filename0'); 
            
            $resource_exist = DB::table('resources')->where('courseID',$lms_course_id)->where('moduleID',$overview_module->id)->where('title','LIKE','%'.$resource_title.'%')->first();
            $resource_id = '';
            if(count($resource_exist) == 0 && Input::has('file0')){
                $destinationPath = "uploads/user-submission/r-";
                if(!is_dir($destinationPath))
                    mkdir($destinationPath,0755);	

                //Add resource info into database
                $resource = new Resource();
                $resource->title = $resource_title;
                $resource->attachment_path = $destinationPath;

                $n = explode('/', Input::get('file0')); 
                $file = $n[count($n)-1];
                // Open the file to get existing content
                $current = file_get_contents(Input::get('file0')); 
                // Write the contents back to the file
                file_put_contents($destinationPath.'/'.$file, $current);
                $resource->attachment_name = $file;
                $attachment_type = explode('.', $n[count($n)-1]);
                $resource->attachment_type = $attachment_type[1];
                $resource->moduleID = $overview_module->id;
                $resource->courseID = $lms_course_id;
                $resource->save();
                $resource_id = DB::getPdo()->lastInsertId();
            }

            
            
            
            return Response::json(array('success'=>'OK','courseId'=>$course->id,'moduleID'=>$overview_module->id,'resourceID'=>$resource_id));
        }else{
            $course = new Course();
            $course->title = Input::get('title');
            $course->description = Input::get('description');
            $course->qualification = Input::get('qualification');
            $course->ref_no = Input::get('refno');
            $course->type = Input::get('type');
            $course->ecommerce_course_id = Input::get('ecommerce_course_id');
            //$course->expiry_date = (Input::get('expiry_date'))?Input::get('expiry_date'):date('Y-m-d h:i:s',strtotime('+2 years'));
            $slug = strtolower(str_replace(' ','-', Input::get('title')));
            $check_slug = DB::table('courses')
                ->where('course_slug',$slug)
                ->count();

            if($check_slug>0){
                $slug_alias = $check_slug+1;
                $slug = $slug.'-'.$slug_alias;
            }
            $course->course_slug = $slug;

            $course->save();
            $course_id = DB::getPdo()->lastInsertId();

            //add a course overview
            $module1 = new Module();
            $module1->title = Input::get('course_overview');
            $module1->description = Input::get('course_overview_details');
            $module1->courseID = $course_id;
            $module1->is_overview = 1;
            $module1->is_certificate = 0;
            $module1->save();
            $module_id = DB::getPdo()->lastInsertId();
             
            $resource_title = Input::get('filename0'); 

            $destinationPath = "uploads/user-submission/r-";
            if(!is_dir($destinationPath))
                mkdir($destinationPath,0755);	

            //Add resource info into database
            $resource = new Resource();
            $resource->title = $resource_title;
            $resource->attachment_path = $destinationPath;
             
            $n = explode('/', Input::get('file0')); 
            $file = $n[count($n)-1];
            // Open the file to get existing content
            $current = file_get_contents(Input::get('file0')); 
            // Write the contents back to the file
            file_put_contents($destinationPath.'/'.$file, $current);
            $resource->attachment_name = $file;
            $attachment_type = explode('.', $n[count($n)-1]);
            $resource->attachment_type = $attachment_type[1];
            $resource->moduleID = $module_id;
            $resource->courseID = $course_id;
            $resource->save();
            $resource_id = DB::getPdo()->lastInsertId();
            
            // add sample module
            if($course->type == 2 OR $course->type == 3){
            $module2 = new Module();
            $module2->title = "Module 1";
            $module2->description = "Module 1 description, please edit here";
            $module2->courseID = $course_id;
            $module2->is_overview = 0;
            $module2->is_certificate = 0;
            $module2->save();
            }

            if($course->type == 2){
            // add sample module
            $module3 = new Module();
            $module3->title = "Certification";
            $module3->description = "Certification Module description, please edit here";
            $module3->courseID = $course_id;
            $module3->is_overview = 0;
            $module3->is_certificate = 1;
            $module3->save();
            }

            //return $course_id;

            return Response::json(array('success'=>'OK','courseId'=>$course_id,'moduleID'=>$module_id,'resource_id'=>''));
        }

    }

    /**
     * @return mixed
     *
     * Add a new user through api route
     */
    public function createUser()
    {
        $user =  new User();
        $user->firstname = Input::get('firstname');
        $user->lastname = Input::get('lastname');
        $user->email = Input::get('email');
        $user->username = Input::get('username');
        $random_pass = str_random(8);
        $user->password = Hash::make($random_pass);
        $user->address1 = Input::get('address1');
        $user->address2 = Input::get('address2');
        $user->town = Input::get('town');
        $user->district = Input::get('district');
        $user->postcode = Input::get('postcode');
        $user->user_image = '';
        $user->user_type= (Input::get('user_type')=='Admin')?0:1;


        $user->save();

        $user_id = DB::getPdo()->lastInsertId();
        // save user courses
        /*$user_courses = (Input::has('user_courses'))?Input::get('user_courses'):array();

        if(Auth::user()->user_type==0 AND count($user_courses)>0){
            if(count($user_courses)>0){
                foreach($user_courses as $ucid){
                    // user course assingment
                    $uca = DB::table('user_courses')->insert(array('userID'=>$user_id,'courseID'=>$ucid,'enroll_date'=>date('Y-m-d'),'exp_date'=>Input::get('enroll_c'.$ucid)));
                }
            }
        }
        */

        // Log the activity
        $log =  new ActivityLog();
        $log->title = 'New user creation';
        $log->activity = 'User Create';
        $log->url = Request::url();
        $log->userID = $user_id;
        $log->save();

        //Send user creation email
        $maildata['firstname'] = $user->firstname;
        $maildata['lastname'] = $user->lastname;
        $maildata['username'] = $user->username;
        $maildata['password'] = $random_pass;
        $setting = Setting::where('name','=','usercreation_email')->first();
        $maildata['email_content'] = $setting->renderContent($setting->content, $maildata);

        Mail::send('emails.userCreateEmail', $maildata, function($message) {
            $message->to( Input::get('email'), Input::get('firstname').Input::get('lastname'))->subject('Your Harper Learner Portal is now ready');
        });

        return Response::json(array('success'=>'OK','msg'=>'User successfully created','userID'=>$user_id));
    }

    /**
     * User course assign
     *
     */
    public function userCourseAssign(){
        $user_id = Input::get('user_id');
        $user_courses = (Input::has('user_courses'))?explode(',',Input::get('user_courses')):array();

        if(count($user_courses)>0){
            foreach($user_courses as $ucid){
                // user course assingment
                $uca = DB::table('user_courses')->insert(array('userID'=>$user_id,'courseID'=>$ucid,'enroll_date'=>date('Y-m-d'),'exp_date'=>date('Y-m-d h:i:s',strtotime('+2 years'))));
            }
        }

        return Response::json(array('success'=>'OK','msg'=>'User course successfully added','userID'=>$user_id));

    }



}