<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/
	
	
	/**
	 * Constructor
	 *
	 * @return Response
	 */
	public function __construct()
	{
		//update last visited url for logged in user
/*		$logged_user = Auth::user();
		if(Auth::check() && Route::currentRouteAction()!='logout') 
		$update_url = $logged_user->updateLastVisitedURL(Request::url());*/
	}
	
	
	/**
	 * Dashboard functionality
	 *
	 * @return Response
	 */	
	public function showWelcome()
	{
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'dashboard';
		$data['template_part'] = 'userDashboard';
		$data['logged_in'] = (Auth::id() === null)?'0':'1';
		
		$data['users_total'] = User::count();
		$data['courses_total'] = Course::count();
		$data['user_latest'] = DB::table('users')->orderBy('created_at','desc')->get();	
		$data['course_latest'] = DB::table('courses')->orderBy('created_at','desc')->get();
		$data['history_latest'] = DB::table('logs')
									->where('userID',Auth::user()
									->id)->orderBy('created_at','desc')
									->first();
									
		$user_submissions = DB::table('user_submissions')
								    ->join('modules','user_submissions.moduleID','=','modules.id')
									->join('courses','courses.id','=','modules.courseID')
									->join('users','users.id','=','user_submissions.userID')
									->select('user_submissions.id as id','users.id as userID','users.username as username','users.firstname as firstname','users.lastname as lastname','modules.title as moduletitle','modules.id as moduleID','courses.title as coursetitle','courses.id as courseID','courses.course_slug as course_slug')
									->groupBy('modules.id')
									->orderBy('user_submissions.created_at','desc') 
									->get();
									

		$data['user_submission_pagination'] = array();		
						
		$data['user_submissions'] = $user_submissions;
		if(count($user_submissions)>0){
			$i = 0;
			foreach($data['user_submissions'] as $us){
                $check_enrollment = DB::table('user_courses')->where('userID',$us->userID)->where('courseID',$us->courseID)->first();

                if(count($check_enrollment)==0)
                    $us->show = false;

				$query = DB::table('user_module_status')
						->where('userID','=',$us->userID)
						->where('moduleID','=',$us->moduleID)
						->where('courseID','=',$us->courseID)
						->where('user_status','=',0)
						->first();
						
				if(count($query)==0)	
                    $us->show = false;
                else
                    $us->show = true;
                
				array_push($data['user_submission_pagination'] ,$us);
				 
				$i++;
			}
		} 
        
		$limit = 10;
		$page = (Input::get('page')!='')?Input::get('page'):1;	  
		$data['user_submissions_current_total'] = $page*$limit ;/*(($page*$limit)>count($data['user_submissions']))?count($data['user_submissions']):($page*$limit);*/

		$data['user_submissions_current_start'] = ($page*$limit)-$limit+1;		
		//var_dump($data['user_submissions']);
		//$data['submission_paginator'] = Paginator::make($data['user_submissions'], count($data['user_submissions']),10);
		$user =  new User();
		$data['user_feedbacks'] =  $query_user_feedbacks = DB::table('user_module_feedback')
								    ->join('modules','user_module_feedback.moduleID','=','modules.id')
									->join('courses','courses.id','=','modules.courseID')
									->join('users','user_module_feedback.reviewer_id','=','users.id')
									->where('users.user_type','=',1)
									->select('user_module_feedback.id as id', 'user_module_feedback.message as message','users.id as userID','users.username as username','users.firstname as firstname','users.lastname as lastname','modules.title as moduletitle','modules.id as moduleID','courses.title as coursetitle','courses.id as courseID','courses.course_slug as course_slug')
									->orderBy('user_module_feedback.created_at','desc') 
									->paginate(10);
       
		return View::make('dashboard',$data);
	}
	
	public function dashboard()
	{
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'dashboard';
		$data['template_part'] = 'userlist';
		$data['logged_in'] = (Auth::id() === null)?'0':'1';
		
		$data['users_total'] = User::count();
		$data['courses_total'] = Course::count();
		$data['user_latest'] = DB::table('users')->orderBy('created_at','desc')->get();	
		$data['course_latest'] = DB::table('courses')->orderBy('created_at','desc')->get();	
		
		return View::make('dashboard',$data);
	}

    
    protected function array_orderby()
    {
        $args = func_get_args();
        $data = array_shift($args);
        foreach ($args as $n => $field) {
            if (is_string($field)) {
                $tmp = array();
                foreach ($data as $key => $row)
                    $tmp[$key] = $row[$field];
                $args[$n] = $tmp;
                }
        }
        $args[] = &$data;
        call_user_func_array('array_multisort', $args);
        return array_pop($args);
    }
	//Settings page
	
	public function getSettings()
	{
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'settings';
		$data['setting'] = array();
		$settings = DB::table('settings')->get();
		if(count($settings)>0){
			$cls = array();
			foreach($settings as $s){
				$cls[$s->name] = $s->content; 				
			}
		$data['setting'] = 	(object)$cls;
		}
		
		return View::make('settings',$data);
	}
	
	
	public function getForgotUsername(){
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'settings';
		
		return View::make('forgotusername',$data);		
	}
	
	public function postForgotUsername(){
		$email =  Input::get('email');	
		
		$rules = array('email' => 'required');
		
		$validation = Validator::make(Input::all(), $rules);
		
		if($validation->fails()){
			$messages = $validation->messages();
			return Redirect::to('forgotusername')->withErrors($validation);	
		}			
		$query = DB::table('users')->where('email',$email)->first();
		$data = array();
		if(count($query)>0){
			$data['username'] = $query->username;
			$data['firstname'] = $query->firstname;
			$data['lastname'] = $query->lastname;
			$data['email'] = $query->email;
			
			//Send mail with username
			$setting = Setting::where('name','=','forgotusername_email')->first();
			$data['email_content'] = $setting->renderContent($setting->content, $data);			
			Mail::send('emails.forgotusername',$data, function($message) use($data)
			{
				$message->to(Input::get('email'), $data['firstname'].' '.$data['lastname'])->subject('Your username assitance');
			});
			
			// Log the activity	
			$log =  new ActivityLog();
			$log->title = 'Forgot Username Retrieve';
			$log->activity = 'Username retrieve';
			$log->url = Request::url();
			$log->userID = $query->id;
			$log->save();			
			
			return Redirect::to('forgotusername')->withFlashNotice('Please check your email for your username');
			
		}else{
			return Redirect::to('forgotusername')->withFlashNotice('Your provided email doesnt matched with our record');	
		}
	}	


	public function getForgotPassword(){
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'settings';
		
		return View::make('forgotpassword',$data);		
	}
		
	public function postForgotPassword(){
		$email =  Input::get('email');	
		
		$rules = array('email' => 'required');
		
		$validation = Validator::make(Input::all(), $rules);
		
		if($validation->fails()){
			$messages = $validation->messages();
			return Redirect::to('forgotpassword')->withErrors($validation);	
		}			
		$query = DB::table('users')->where('email',$email)->first();
		$data = array();
		if(count($query)>0){
			$data['username'] = $query->username;
			$data['firstname'] = $query->firstname;
			$data['lastname'] = $query->lastname;
			$data['email'] = $query->email;
			$data['token'] = md5($query->username);
			
			$update = DB::table('users')->where('email',$email)->update(array('token'=>$data['token'],'updated_at'=>date('Y-m-d h:i:s')));
			
			//Send mail with forgot password link 
			$setting = Setting::where('name','=','forgotpassword_email')->first();
			$data['email_content'] = $setting->renderContent($setting->content, $data);				
			Mail::send('emails.forgotpassword',$data, function($message) use($data)
			{
				$message->to(Input::get('email'), $data['firstname'].' '.$data['lastname'])->subject('Your password retireve assitance');
			});
			
			// Log the activity	
			$log =  new ActivityLog();
			$log->title = 'Forgot password Retrieve';
			$log->activity = 'Password retrieve';
			$log->url = Request::url();
			$log->userID = $query->id;
			$log->save();	
			
			return Redirect::to('forgotpassword')->withFlashNotice('Please check your email for the instruction to reset your password');
			
		}else{
			return Redirect::to('forgotpassword')->withFlashNotice('Your provided email doesnt matched with our record');	
		}		
	}
	
	/* Password Reset view
	*
	* return void
	*
	*/
	
	public function getPasswordReset($token){
		$data['title'] = 'Password reset form';
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'settings';
		$data['token'] = $token;
		

				
		$check = DB::table('users')->where('token',$token)->first();
		
		if(count($check)>0){
			$time = round((strtotime(date('Y-m-d h:i:s'))-strtotime($check->updated_at))/3600);
			
			if($time>1){
				return Redirect::to('/forgotpassword')->withFlashNotice('Your provided token is invalid or expired');
			}
			
			return View::make('passwordreset',$data);
		}
		else{
			return Redirect::to('/forgotpassword')->withFlashNotice('Your provided token is invalid or expired');				
		}
		
	}
	
	/* Password Reset action
	*
	* return void
	*
	*/
	
	public function postPasswordReset(){
		$data = array();
		$token = Input::get('token');
		$newpassword = Input::get('password');
		$con_password = Input::get('password_confirmation');

		
	    $validator = Validator::make( 
				Input::all(),
				array(
					'password' => 'required|min:6|confirmed',
					'password_confirmation' => 'required|min:6'
				)
			);
	
	    if($validator->fails()){
				return Redirect::to('reset-password/'.$token)->withErrors($validator)->withInput();
		}				
		$check = DB::table('users')->where('token',$token)->first();
		
		if(count($check)>0){
			$time = round((strtotime(date('Y-m-d h:i:s'))-strtotime($check->updated_at))/3600);
			
			if($time>1){
				return Redirect::to('reset-password/'.$token)->withFlashNotice('Your provided token is invalid or expired');
			}
			
			$query = DB::table('users')->where('token',$token)->update(array('password'=>Hash::make($newpassword),'token'=>'','updated_at'=>date('Y-m-d h:i:s')));
			
			$data['email'] = $check->email;	
			$data['username'] = $check->username;			
			$data['password'] = Input::get('password');
			$data['firstname'] = $check->firstname;
			$data['lastname'] = $check->lastname;
			
			//Send mail with username
			$setting = Setting::where('name','=','passwordchanged_email')->first();
			$data['email_content'] = $setting->renderContent($setting->content, $data);				
			Mail::send('emails.passwordchanged',$data, function($message) use($check)
			{
				$message->to($check->email, $check->firstname.' '.$check->lastname)->subject('Your password has changed');
			});			
			
			return Redirect::to('login')->withFlashNotice('Your password has been changed successfully. You may login now');	
		}
		else{
			return Redirect::to('reset-password/'.$token)->withFlashNotice('Your provided token is invalid or expired');				
		}
		
	}	
	
	/* Download user submitted file by admin
	*
	* return void
	*
	*/
	
	public function download($id){
		
		$attachment = DB::table('user_submissions')->where('id',base64_decode($id))->first();
		if(count($attachment)>0){
			$download_path = $attachment->attachment_path.'/'.$attachment->attachment_name;
			
			return Response::download($download_path);
		}
		
	}
	
	/* Download resource file by admin
	*
	* return void
	*
	*/
	
	public function downloadResource($id){
		
		$attachment = DB::table('resources')->where('id',base64_decode($id))->first();
		if(count($attachment)>0){
			$download_path = $attachment->attachment_path.'/'.$attachment->attachment_name;
            
			if($attachment->attachment_name == 'direct')
                return Response::download($attachment->attachment_path);
            else
			return Response::download($download_path);
		}
		
		
	}


	/* Download resource file by admin
	*
	* return void
	*
	*/

	public function downloadCertificate($courseid){

		$attachment = DB::table('user_certificates')->where('userID',Auth::user()->id)->where('courseID',base64_decode($courseid))->first();

		if(count($attachment)>0){
			$download_path = $attachment->filepath.'/'.$attachment->filename;

			return Response::download($download_path);
		}


	}


	/* Get the feedback details
	*
	* return void
	*
	*/
	
	public function feedbackDetails($id){
		
		$feedback = DB::table('user_module_feedback')
							->join('users','user_module_feedback.reviewer_id','=','users.id')
							->where('user_module_feedback.id',$id)
							->select('user_module_feedback.*', 'users.firstname as firstname', 'users.lastname as lastname')
							->first();
		if(count($feedback)>0){
			$response = array();
			$update = DB::table('user_module_feedback')->where('id',$id)->update(array('is_read'=>1));
			$response['message'] = $feedback->message;
			$response['sender'] = $feedback->firstname.' '.$feedback->lastname;
			$response['posted_on'] = $feedback->created_at;

            if($feedback->attachment_ids != ""){
                $attachment_ids = explode(',',$feedback->attachment_ids);
                $attachments = DB::table('resources')->whereIn('id',$attachment_ids)->get();
                if(count($attachments)>0){
                    foreach($attachments as $attm){
                        $response['message'].= '<br/><a href="'.Config::get('app.url').'/downloadResource/'.base64_encode($attm->id).'">'.$attm->attachment_name.'</a>';
                    }
                }
            }

			return Response::json($response);
		}		
		
	}
	
		
	/* Make the user logged in
	*
	* return void
	*
	*/
	
	public function loginAsUser($id){	
		if(Auth::user()->user_type == 0){	
			$log =  new ActivityLog();
			$log->title = 'Logout and Login as user';
			$log->activity = 'Logout';
			$log->url = Request::url();
			$log->userID = Auth::user()->id;
			$log->save();
			
			Auth::logout();				
			if(!Auth::check()){
				
				
				$user =  User::find($id);
				Auth::login($user);
				
				$log =  new ActivityLog();
				$log->title = 'Account Login';
				$log->activity = 'Login';
				$log->url = Request::url();
				$log->userID = Auth::user()->id;
				$log->save();
				
				if(isset($_COOKIE['course_slug']) and isset($_COOKIE['moduleid'])){
					return Redirect::intended('course/'.$_COOKIE['course_slug'].'/'.$_COOKIE['moduleid']);	
				}else{
					return Redirect::intended('users/'.Auth::user()->username.'/userinfo');	
				}						
				
			}
		}	
	}
	
	/* Setting insert/update
	*
	* return void
	*
	*/
	
	public function postSetting(){
		$rules = array(
					'admin_email' => 'required|email|min:3',
					'start_module_count' => 'required|min:1',
					'usercreation_email' => 'required|min:5',
					'usersubmission_email' => 'required|min:5',
					'usersubmission_moderated_email' => 'required|min:5',
					'adminfeedback_email' => 'required|min:6',
					'userfeedback_email' => 'required|min:6',
					'forgotpassword_email' => 'required|min:6',
					'forgotusername_email' => 'required|min:6',
					'passwordchanged_email' => 'required|min:6',
					'passwordgenerated_email' => 'required|min:6',
					'usercourse_certification_invite_code_email'=>'required|min:6'
				);
	    $validator = Validator::make( 
				Input::all(),$rules
				
			);
	
	    if($validator->fails()){
				return Redirect::to('settings')->withErrors($validator)->withInput();
		}
		$settings = new Setting();

        $inputs = Input::all();





        foreach($inputs as $key=>$value){
            $settings->checkExistOrSave($key,$value);

        }
		//
		/*$admin_email = $settings->checkExistOrSave('admin_email',Input::get('admin_email'));
		$start_module_count = $settings->checkExistOrSave('start_module_count',Input::get('start_module_count'));
		$usercreation_email = $settings->checkExistOrSave('usercreation_email',Input::get('usercreation_email'));
		
		$usersubmission_email = $settings->checkExistOrSave('usersubmission_email',Input::get('usersubmission_email'));
		
		$usersubmission_moderated_email = $settings->checkExistOrSave('usersubmission_moderated_email',Input::get('usersubmission_moderated_email'));
		$adminfeedback_email = $settings->checkExistOrSave('adminfeedback_email',Input::get('adminfeedback_email'));
		$userfeedback_email = $settings->checkExistOrSave('userfeedback_email',Input::get('userfeedback_email'));
		$forgotpassword_email = $settings->checkExistOrSave('forgotpassword_email',Input::get('forgotpassword_email'));
		$forgotusername_email = $settings->checkExistOrSave('forgotusername_email',Input::get('forgotusername_email'));
		$passwordchanged_email = $settings->checkExistOrSave('passwordchanged_email',Input::get('passwordchanged_email'));
		$passwordgenerated_email = $settings->checkExistOrSave('passwordgenerated_email',Input::get('passwordgenerated_email'));
		$usercourse_certification_invite_code_email = $settings->checkExistOrSave('usercourse_certification_invite_code_email',Input::get('usercourse_certification_invite_code_email'));
		*/
		return Redirect::to('settings')->withFlashNotice('Setting Updated Successfully');
		
	}

	public function getAllFeedback(){
		$data['selected'] = 'none';
		$data['template_part'] = 'allfeedback';
		$data['siteurl'] = Config::get('app.url');
		$data['history_latest'] = DB::table('logs')
			->where('userID',Auth::user()
				->id)->orderBy('created_at','desc')
			->first();
		$limit = 20;
		$page = (Input::get('page')!='')?Input::get('page'):1;
		$data['user_submissions_current_total'] = $page*$limit ;/*(($page*$limit)>count($data['user_submissions']))?count($data['user_submissions']):($page*$limit);*/

		$data['user_submissions_current_start'] = ($page*$limit)-$limit+1;
		//var_dump($data['user_submissions']);
		//$data['submission_paginator'] = Paginator::make($data['user_submissions'], count($data['user_submissions']),10);
		$user =  new User();
		$data['user_feedbacks'] =  $query_user_feedbacks = DB::table('user_module_feedback')
			->join('modules','user_module_feedback.moduleID','=','modules.id')
			->join('courses','courses.id','=','modules.courseID')
			->join('users','user_module_feedback.reviewer_id','=','users.id')
			->where('users.user_type','=',1)
			->select('user_module_feedback.id as id', 'user_module_feedback.message as message','users.id as userID','users.username as username','users.firstname as firstname','users.lastname as lastname','modules.title as moduletitle','modules.id as moduleID','courses.title as coursetitle','courses.id as courseID','courses.course_slug as course_slug')
			->orderBy('user_module_feedback.created_at','desc')
			->paginate(20);

		return View::make('dashboard',$data);
	}

	public function getAllSubmissions(){
		$data['selected'] = 'none';
		$data['template_part'] = 'allSubmissions';
		$data['siteurl'] = Config::get('app.url');
		$data['history_latest'] = DB::table('logs')
			->where('userID',Auth::user()
				->id)->orderBy('created_at','desc')
			->first();
		$limit = 20;
		$page = (Input::get('page')!='')?Input::get('page'):1;

		//var_dump($data['user_submissions']);
		//$data['submission_paginator'] = Paginator::make($data['user_submissions'], count($data['user_submissions']),10);
		$user =  new User();
		$user_submissions = DB::table('user_submissions')
			->join('modules','user_submissions.moduleID','=','modules.id')
			->join('courses','courses.id','=','modules.courseID')
			->join('users','users.id','=','user_submissions.userID')
			->select('user_submissions.id as id','users.id as userID','users.username as username','users.firstname as firstname','users.lastname as lastname','modules.title as moduletitle','modules.id as moduleID','courses.title as coursetitle','courses.id as courseID','courses.course_slug as course_slug','user_submissions.created_at as created_at')
			->orderBy('user_submissions.created_at','desc')
			->paginate(20);

		if(count($user_submissions)>0){
			$i = 0;
			foreach($user_submissions as $us){

				$query = DB::table('user_module_status')
					->where('userID','=',$us->userID)
					->where('moduleID','=',$us->moduleID)
					->where('courseID','=',$us->courseID)
					->where('user_status','=',0)
					->first();
				$us->status = '';
				if(count($query)>0){
					switch($query->user_status){
						case "0":
							$us->status = 'Submitted';
						break;
						case '1':
							$us->status = 'Referred';
						break;
						case '2':
							$us->status = 'Feedback';
						break;
						case '3':
							$us->status = 'Passed';
						break;

					}
				}

				//array_push($data['user_submissions'],$us);

				$i++;
			}
		}

		$data['user_submissions_current_start'] = ($page*$limit)-$limit+1;
		$data['user_submissions_current_total'] = $page*$limit ;/*(($page*$limit)>count($data['user_submissions']))?count($data['user_submissions']):($page*$limit);*/

		$data['user_submissions_current_start'] = ($page*$limit)-$limit+1;
		$data['user_submissions'] = $user_submissions;


		return View::make('dashboard',$data);
	}
}
