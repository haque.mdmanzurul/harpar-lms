<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsersTableSeeder');
	}

}

class UsersTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		
			DB::table('users')->insert(array(
				'firstname'=>'Richard',
				'lastname'=>'Brook',
				'email'=>'richard.brooke@gmail.com',
				'password'=>Hash::make('Richard12'),
				'username'=>'richard.brooke',
				'user_type'=>0
			));
			
			DB::table('users')->insert(
			array(
				'firstname'=>'Scott',
				'lastname'=>'Mash',
				'email'=>'scott.mash@gmail.com',
				'password'=>Hash::make('Scott12'),
				'username'=>'scott.mash',
				'user_type'=>1
			));
		

		// $this->call('UserTableSeeder');
	}

}
