<?php

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	use UserTrait, RemindableTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password', 'remember_token');
	protected $fillable = array('firstname','lastname','username','email','last_visited_url');
	protected $guarded = array('id', 'password');
	
	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
	  return $this->getKey();
	}
	 
	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
	  return $this->password;
	}
	 
	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
	  return $this->email;
	}	

	/**
	 * Get modules per user based on course
	 *
	 * @return array
	 */	
	public function getUserCourseModules($cid=NULL){
		$query = "SELECT modules.*,courses.id as cid,courses.course_slug as slug, courses.type as type FROM modules inner join user_courses on user_courses.courseID=modules.courseID and user_courses.userID = ".$this->id.' inner join courses on user_courses.courseID=courses.id';
		
		if($cid !=NULL)
		$query .= ' and courses.id='.$cid;
		$user_modules = DB::select( DB::raw($query));		
		$filtered_user_modules = array();
		if(count($user_modules)>0){
			foreach($user_modules as $um){
				$user_module_status = DB::table('user_module_status')
									->where('userID',$this->id)
									->where('moduleID',$um->id)
									->where('courseID',$um->cid)
									->first();	
									
				if(count($user_module_status)>0){
					$um->module_status = $user_module_status->user_status;
					
				}
				else{
					$um->module_status = '10';
				}
				array_push($filtered_user_modules,$um);
			}
		}
		
		return $user_modules;		
	}
	



	/**
	 * Get feedbacks based on user id and user module id
	 *
	 *@param int userid
	 *@param int moduleid
	 *@param int courseid	 
	 *	 	 	 
	 * @return array
	 */	
	public function getFeedbackOnUser($uid,$mid,$cid=NULL){
		
		$feedbacks = array();
		//get admin feedbacks
		$admin_feedbacks = 	DB::table('user_module_feedback')
											->where('user_module_feedback.moduleID',$mid)
											->where('user_module_feedback.reviewer_id',$uid)
											->join('users','users.id','=','user_module_feedback.reviewer_id')
											->orderBy('user_module_feedback.id', 'desc')
											->get();
											
											
		if(count($admin_feedbacks)>0){
			foreach($admin_feedbacks as $af){
				/*$user = 	DB::table('users')->where('id',$af->reviewer_id)->first();
				
				if($user){
				$af->user_image = $user->user_image;
				$af->user_type = $user->user_type;
				}*/

                //get attachments links
                $attachment_links = "";
                if($af->attachment_ids != ""){
                    $attachment_ids = explode(',',$af->attachment_ids);
                    $attachments = DB::table('resources')->whereIn('id',$attachment_ids)->get();
                    if(count($attachments)>0){
                        foreach($attachments as $attm){
                            $attachment_links.= '<br/><a href="'.Config::get('app.url').'/downloadResource/'.base64_encode($attm->id).'">'.$attm->attachment_name.'</a>';
                        }
                    }
                }

                $af->attachment_links = $attachment_links;
				
				array_push($feedbacks,$af);
			}
		}
		
		
		return $feedbacks;		
	}
	
	
	/**
	 * Get feedbacks based on user id and user module id
	 *
	 *@param int userid
	 *@param int moduleid
	 *@param int courseid	 
	 *	 	 	 
	 * @return array
	 */	
	public function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	/**
	 * Get total submission based on user id
	 *
	 *@param int userid 
	 *	 	 	 
	 * @return array
	 */	
	public function getUserSubmisions($id,$courseid) {
		//Get the users submission
		$query_user_submissions = DB::table('user_submissions')
								    ->join('modules','user_submissions.moduleID','=','modules.id')
									->join('courses','courses.id','=','modules.courseID')
									->join('users','users.id','=','user_submissions.userID')
									->where('users.id','=',$id)
									->where('courses.id','=',$courseid)
									->select('user_submissions.id as id','users.id as userID','users.username as username','users.firstname as firstname','users.lastname as lastname','modules.title as moduletitle','modules.id as moduleID','courses.title as coursetitle','courses.id as courseID','courses.course_slug as course_slug')
									->groupBy('modules.id')
									->orderBy('user_submissions.created_at','desc') 
									->get();
									

					
		$user_submissions = array();
		if(count($query_user_submissions)>0){
			foreach($query_user_submissions as $us){
				$query = DB::table('user_module_status')
						->where('userID','=',$us->userID)
						->where('moduleID','=',$us->moduleID)
						->where('courseID','=',$us->courseID)
						->where('user_status','=',0)
						->first();
						
				if(count($query)>0)		
				array_push($user_submissions,$us);
			}
		}
		
		return count($user_submissions);	
	}
	
	/**
	 * Get user module feedback	 	 
	 *	 	 	 
	 * @return array
	 */	
	public function getUserNewModuleFeedback() {
		//Get the users submission
		$query_user_feedbacks = DB::table('user_module_feedback')
								    ->join('modules','user_module_feedback.moduleID','=','modules.id')
									->join('courses','courses.id','=','modules.courseID')
									->join('users','user_module_feedback.reviewer_id','=','users.id')
									->where('users.user_type','=',1)
									->select('user_module_feedback.id as id', 'user_module_feedback.message as message','users.id as userID','users.username as username','users.firstname as firstname','users.lastname as lastname','modules.title as moduletitle','modules.id as moduleID','courses.title as coursetitle','courses.id as courseID','courses.course_slug as course_slug')
									->orderBy('user_module_feedback.created_at','desc') 
									->get();
									

					
		return 	$query_user_feedbacks;
	}
    
    /**
	 * Get user module feedback	 	 
	 *	 	 	 
	 * @return array
	 */	
	public function getAllFeedbackOnModule($moduleid) {
		//Get the users submission
		$query_user_feedbacks = DB::table('user_module_feedback')
								    ->join('modules','user_module_feedback.moduleID','=','modules.id')
									->join('courses','courses.id','=','modules.courseID')
									->join('users','user_module_feedback.reviewer_id','=','users.id')
									->where('user_module_feedback.moduleID','=',$moduleid)
									->select('user_module_feedback.id as id', 'user_module_feedback.message as message','users.id as userID','users.username as username','users.firstname as firstname','users.lastname as lastname','modules.title as moduletitle','modules.id as moduleID','courses.title as coursetitle','courses.id as courseID','courses.course_slug as course_slug')
									->orderBy('user_module_feedback.created_at','desc') 
									->get();
									

					
		return 	$query_user_feedbacks;
	}

	/**
	 * Get new module feedbacks	 	 
	 *	 	 	 
	 * @return array
	 */	
	public function getUnreadFeedbacks($uid, $mid) {
		//Get the users submission
		$query_user_feedbacks = DB::table('user_module_feedback')
									->join('users','user_module_feedback.reviewer_id','=','users.id')
									->where('users.user_type','=',0)
									->where('user_module_feedback.userID','=',$uid)
									->where('user_module_feedback.moduleID','=',$mid)									
									->where('user_module_feedback.is_read','=',0)
									->orderBy('user_module_feedback.created_at','desc') 
									->get();
									

					
		return 	$query_user_feedbacks;
	}
	
	/**
	 * Update user last visited url	 	 
	 *	 	 	 
	 * @return array
	 */	
	public function updateLastVisitedURL($url) {
		//Update user last visited url		
		$segments = explode('/',$url);
		if(!(in_array('logout',$segments) or in_array('login',$segments))){
			$this->last_visited_url = $url;
		
			if($this->save())
				return true;		
			else
				return false;	
		}

	}			
}
