<?php

//use Illuminate\Auth\UserTrait;
//use Illuminate\Auth\UserInterface;
//use Illuminate\Auth\Reminders\RemindableTrait;
//use Illuminate\Auth\Reminders\RemindableInterface;

class Payment extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'course_payments';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('payee_firstname','payee_lastname','payee_address','payee_country','payment_id','invoice_number','course_id','user_id','payement_date','payment_amount');
	protected $guarded = array('id');
	public $timestamps = false;

}
