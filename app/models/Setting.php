<?php

//use Illuminate\Auth\UserTrait;
//use Illuminate\Auth\UserInterface;
//use Illuminate\Auth\Reminders\RemindableTrait;
//use Illuminate\Auth\Reminders\RemindableInterface;

class Setting extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'settings';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('name','content','template');
	protected $guarded = array('id');
	
	public function checkExistOrSave($name,$content,$template="na"){
		$exist = DB::table('settings')->where('name','=',$name)->count();
		
		if($exist>0){
			$update = DB::table('settings')->where('name',$name)->update(array('content'=>$content,'template'=>$template));
		}
		else{
			$update = DB::table('settings')->insert(array('name'=>$name,'content'=>$content,'template'=>$template));	
		}
	}
	
	public function renderContent($buffer, $data){
		if(is_array($data) or is_object($data)){
			foreach($data as $key=>$value){
				$buffer = str_replace('{{'.$key.'}}',$value,$buffer);
			}
		}
		
		return $buffer;
	}	
}
