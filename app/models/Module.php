<?php

//use Illuminate\Auth\UserTrait;
//use Illuminate\Auth\UserInterface;
//use Illuminate\Auth\Reminders\RemindableTrait;
//use Illuminate\Auth\Reminders\RemindableInterface;

class Module extends Eloquent{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'modules';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $fillable = array('title','description');
	protected $guarded = array('id');
	
	
	/**
	 * Get modules status based on user
	 *
	 * @return module_status
	 */	
	public function getUserModuleStatus($uid,$cid){
		//
		$user_module_status = DB::table('user_module_status')
									->where('userID',$uid)
									->where('moduleID',$this->id)
									->where('courseID',$cid)
									->first();
									
		if(count($user_module_status)>0)
		return 	$user_module_status->user_status;
		else
		return '10';
	}
	/**
	 * Get modules status based on user
	 *
	 * @return module_status
	 */	
	public function getUserLeftModuleCount($uid,$cid){
		//get all module count
		$count = DB::table('modules')->where('courseID',$cid)->where('is_overview',0)->where('is_certificate',0)->count();
		$user_module_left = DB::table('user_module_status')
									->where('userID',$uid)
									->where('courseID',$cid)
									->where('user_status',3)
									->count();
									
		if(count($user_module_left)>0)
		return 	$count-$user_module_left;
		else
		return $count;						
	}
}
