<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
Route::get('/', 'HomeController@showWelcome')->before('auth')->before('auth.onlyadmin')->before('auth.loglastvisit');


Route::get('forgotusername', 'HomeController@getForgotUsername');
Route::post('forgotusername', 'HomeController@postForgotUsername');


Route::get('forgotpassword', 'HomeController@getForgotPassword');
Route::post('forgotpassword', 'HomeController@postForgotPassword');


Route::get('reset-password/{token}', 'HomeController@getPasswordReset');
Route::post('reset-password', 'HomeController@postPasswordReset');


//Add admin feedback on module
Route::post('postAdminFeedback', 'UserController@postAdminFeedback')->before('auth');

//logout action
Route::get('settings', 'HomeController@getSettings')->before('auth')->before('auth.onlyadmin');
Route::post('settings', 'HomeController@postSetting')->before('auth')->before('auth.onlyadmin');
Route::get('feedbacks', 'HomeController@getAllFeedback')->before('auth')->before('auth.onlyadmin');
Route::get('submissions', 'HomeController@getAllSubmissions')->before('auth')->before('auth.onlyadmin');

Route::get('courses', 'CourseController@index')->before('auth')->before('auth.loglastvisit');
Route::get('qualifications', 'CourseController@qualifications')->before('auth')->before('auth.loglastvisit');
Route::get('accreditations', 'CourseController@accreditations')->before('auth')->before('auth.loglastvisit');
Route::get('self-assessment/{courseid}', 'CourseController@selfAssessment')->before('auth')->before('auth.loglastvisit');
Route::get('getAssessmentQuestions/{courseid}', 'CourseController@getAssessmentQuestions')->before('auth');
Route::get('logAssessmentStatus/{courseid}/{passed}', 'CourseController@logAssessmentStatus')->before('auth');
Route::get('downloadmypdf', 'CourseController@downloadmypdf')->before('auth');


// Add new course
Route::get('addcourse', 'CourseController@addCourse')->before('auth')->before('auth.onlyadmin');
Route::post('addcourse', 'CourseController@postAddCourse')->before('auth')->before('auth.onlyadmin');

Route::get('course/delete/{id}', 'CourseController@deleteCourse')->before('auth')->before('auth.onlyadmin');

// Edit Course
Route::get('course/edit/{id}', 'CourseController@edit')->before('auth')->before('auth.onlyadmin');
Route::post('course/edit/{id}', 'CourseController@postEdit')->before('auth')->before('auth.onlyadmin');

//Module Details
Route::get('course/{slug}/{moduleid}', 'CourseController@show')->before('auth')->before('auth.loglastvisit');

// Update user module status
Route::post('postUserModuleStatus', 'UserController@postUserModuleStatus')->before('auth')->before('auth.onlyadmin');

// Add resource for modules ajax
Route::post('addresource', 'CourseController@addResource')->before('auth')->before('auth.onlyadmin');

// Delete resource for modules ajax
Route::post('deleteresource/{id}', 'CourseController@deleteResource')->before('auth')->before('auth.onlyadmin');


// Show all users
Route::get('users', 'UserController@show')->before('auth')->before('auth.onlyadmin')->before('auth.loglastvisit');

// Show all users
Route::post('addusersubmission', 'UserController@addusersubmission')->before('auth');

// Show single user information to admin
Route::get('users/{username}/submissionhistory/{moduleID}', 'UserController@submissionhistory')->before('auth')->before('auth.loglastvisit');


// Show single user information to admin
Route::get('users/{username}', 'UserController@displayUserInfoToAdmin')->before('auth')->before('auth.onlyadmin')->before('auth.loglastvisit');

// Show single user information to admin
Route::get('users/{username}/course/{course_slug}', 'UserController@displayUserInfoToAdminWithCourseSubmission')->before('auth')->before('auth.onlyadmin')->before('auth.loglastvisit');

Route::get('/users/{username}/userinfo', 'UserController@getUserInfo')->before('auth')->before('auth.loglastvisit');

Route::get('newuser', 'UserController@createNewUser')->before('auth')->before('auth.onlyadmin')->before('auth.loglastvisit');

Route::post('newuser', 'UserController@postCreateUser')->before('auth')->before('auth.onlyadmin');

Route::get('edituser/{id}', 'UserController@editUser')->before('auth')->before('auth.owneroradmin')->before('auth.loglastvisit');

Route::post('edituser/{id}', 'UserController@postEditUser')->before('auth')->before('auth.owneroradmin');

//delete user
Route::get('deleteuser/{id}', 'UserController@deleteuser')->before('auth')->before('auth.onlyadmin');

//change password view
Route::get('changepassword/{id}', 'UserController@getChangePassword')->before('auth')->before('auth.owneroradmin');
//change password action
Route::post('changepassword/{id}', 'UserController@postChangePassword')->before('auth')->before('auth.owneroradmin');

//change password action
Route::get('certificates', 'UserController@showCertificates')->before('auth')->before('auth.loglastvisit');

Route::get('downloadCertificate/{courseid}', 'HomeController@downloadCertificate')->before('auth')->before('auth.loglastvisit');


//show login form
Route::get('login', 'UserController@getLogin');

// Show single user information to admin
Route::post('login', 'UserController@postLogin');

//logout action
Route::get('logout', 'UserController@getLogout');

//download action
Route::get('download/{id}', 'HomeController@download')->before('auth')->before('auth.onlyadmin');
Route::get('downloadResource/{id}', 'HomeController@downloadResource')->before('auth');
Route::get('showFeedbackDetails/{id}', 'HomeController@feedbackDetails')->before('auth')->before('auth.onlyadmin');
Route::get('loginasuser/{id}', 'HomeController@loginAsUser')->before('auth')->before('auth.onlyadmin');
Route::post('addusercertificate', 'UserController@addUserCertificate')->before('auth')->before('auth.onlyadmin');
Route::get('deleteCertificate/{id}', 'UserController@deleteCertificate')->before('auth')->before('auth.onlyadmin');

//Upload attachment for feedback
Route::post('addfeedbackattachment', 'UserController@addFeedbackAttachment')->before('auth');

//Delete Attachment from feedback
Route::get('deleteattachment/{id}', 'UserController@deleteAttachment')->before('auth');

//get certified
Route::get('users/getcertified/{course_slug}/{token}', 'UserController@getCertified')->before('auth');

Route::post('payment', array(
    'as' => 'payment',
    'uses' => 'UserController@postPayment',
));
// this is after make the payment, PayPal redirect back to your site
Route::get('payment/status', array(
    'as' => 'payment.status',
    'uses' => 'UserController@getPaymentStatus',
));
//API
// Route group for API versioning
/*Route::group(array('prefix' => 'api/v1'), function()
{
    Route::resource('adduser', 'UserController@adduserthroughapi');
    //return "working";
});*/

//Route::get('api/v1/adduser', 'UserController@adduserthroughapi');
Route::post('api/v1/createUser', 'ApiController@createUser');
Route::post('api/v1/checkuser', 'ApiController@checkuser');
Route::get('api/v1/getAllCourses', 'ApiController@getAllCourses');
Route::post('api/v1/createCourse', 'ApiController@createCourse');
Route::post('api/v1/assignCourses', 'ApiController@userCourseAssign');


//Custom validation
Validator::extend('ccd', function($field,$value,$parameters){
    //return true if field value is foo
    $card = CreditCard::validCreditCard($value);

    return $card['valid'];
});

Validator::extend('cvv', function($field,$value,$parameters){
    //return true if field value is foo


    $validCvc = CreditCard::validCvc($value, Input::get('cardtype'));;

    return $validCvc;
});

Validator::extend('cardexp', function($field,$value,$parameters){
    //return true if field value is foo

    $validDate = CreditCard::validDate(Input::get('expiry-year'),$value);;

    return $validDate;
});
