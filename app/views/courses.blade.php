@extends('layouts/master')
@section('header')
  @include('parts.global.header')
@stop

@section('sidebar')
  @if (isset($module_sidebar) and Auth::user()->user_type=='1')
     @include('parts.users.usersidebar')
  @else
     @include('parts.global.sidebar')
  @endif
@stop

@section('content')
   @if ($template_part=='courselist')
  		@include('parts.courses.courselist')
   @elseif ($template_part=='qualifications')
        @include('parts.courses.qualifications')
   @elseif ($template_part=='accreditations')
       @include('parts.courses.accreditations')
   @elseif ($template_part=='addcourse')
   		@include('parts.courses.addCourseForm')
   @elseif ($template_part=='editcourse')
   		@include('parts.courses.editCourseForm') 
   @elseif ($template_part=='showcourse')
   		@include('parts.courses.courseDetails')
   @elseif ($template_part=='assessment')
       @include('parts.courses.assessment')
   @endif  
@stop

@section('footer')
  @include('parts.global.footer')
@stop