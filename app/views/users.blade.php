@extends('layouts/master')
@section('header')
  @include('parts.global.header')
@stop

@section('sidebar')
  @if (isset($module_sidebar) and Auth::user()->user_type=='1')
     @include('parts.users.usersidebar')
  @else
     @include('parts.global.sidebar')
  @endif
@stop

@section('content')
  @if ($template_part=='viewuser')
  	@include('parts.users.viewuser')  
      
  @elseif ($template_part=='userinfo')  
  	@include('parts.users.userinfo')   
     
  @elseif ($template_part=='createnewuser')  
  	@include('parts.users.createuser')
    
  @elseif ($template_part=='edituser')  
  	@include('parts.users.editUser') 
    
  @elseif ($template_part=='submissionhistory')  
  	@include('parts.users.submissionhistory') 

  @elseif ($template_part=='changepassword')  
  	@include('parts.users.changepassword')
    
  @elseif ($template_part=='certificates')  
  	@include('parts.users.certificates')

  @elseif($template_part == 'getCertified')
    @include('parts.users.getCertified')
  @else  
  	@include('parts.users.userlist')    
  @endif
@stop

@section('footer')
  @include('parts.global.footer')
@stop