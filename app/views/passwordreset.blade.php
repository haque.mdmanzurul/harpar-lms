@include('parts.global.header')
<div class="pagewrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 content-wrapper">
                <div class="login">
                @if (Session::has('flash_notice'))
                	<span class="error">
                    {{ Session::get('flash_notice') }}
                    </span>
                @endif
                	{{ Form::open(array('url' => 'reset-password','method'=>'post')) }}
                    {{ Form::hidden('token',$token)}}
                     <p>
                     {{ Form::label('New Password')}}
                     {{ Form::password('password')}}
                     <span class="error">
                     	@if ($errors->has('password'))
                        {{ $errors->first('password') }}
                        @endif
                     </span>
                     </p>
                     
                      <p>
                     {{ Form::label('Confirm Password')}}
                     {{ Form::password('password_confirmation')}}
                     <span class="error">
                        @if ($errors->has('password_confirmation'))
                        {{ $errors->first('password_confirmation') }}
                        @endif 
                     </span>                   
                     </p>
                     
                      <div class="row">
                     <div class="col-md-9"></div>
                     <div class="col-md-3">{{ Form::submit('Submit')}}</div>
                     </div>    
                     
                     {{ Form::close()}}                 
                </div>
            </div>
        </div>
    </div>
</div>
@include('parts.global.footer')