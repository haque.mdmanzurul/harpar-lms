@extends('layouts/master')
@section('header')
  @include('parts.global.header')
@stop

@section('sidebar')
  @include('parts.global.sidebar')
@stop

@section('content')
   @include('parts.global.settingsForm')
@stop

@section('footer')
  @include('parts.global.footer')
@stop