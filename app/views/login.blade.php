@include('parts.global.header')
<div class="pagewrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 content-wrapper">
                <div class="login">
                @if (Session::has('flash_notice'))
                	<span class="error">
                    {{ Session::get('flash_notice') }}
                    </span>
                @endif
                	{{ Form::open(array('url' => 'login','method'=>'post')) }}
                     <p>
                     {{ Form::label('username')}}
                     {{ Form::text('username')}}
                     <span class="error">
                     	@if ($errors->has('username'))
                        {{ $errors->first('username') }}
                        @endif
                     </span>
                     </p>
                     
                      <p>
                     {{ Form::label('password')}}
                     {{ Form::password('password')}}
                     <span class="error">
                        @if ($errors->has('password'))
                        {{ $errors->first('password') }}
                        @endif 
                     </span>                   
                     </p>
                     
                      <div class="row">
                     <div class="col-md-9"><a class="forgot-password no-bottom-margin" href="/forgotusername">Forgot username?</a><a class="forgot-password" href="/forgotpassword">Forgot password?</a></div>
                     <div class="col-md-3">{{ Form::submit('Login')}}</div>
                     </div>    
                     
                     {{ Form::close()}}                 
                </div>
            </div>
        </div>
    </div>
</div>
@include('parts.global.footer')