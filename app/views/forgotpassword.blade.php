@include('parts.global.header')
<div class="pagewrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 content-wrapper">
                <div class="login">
                <h2>Get your password</h2>
                @if (Session::has('flash_notice'))
                	<span class="error">
                    {{ Session::get('flash_notice') }}
                    </span>
                @endif
                	{{ Form::open(array('url' => 'forgotpassword','method'=>'post')) }}

                     
                      <div class="row">
                     <div class="col-md-9"> 
                     {{ Form::text('email','',array('placeholder'=>'Enter your email'))}}
                     {{ Form::hidden('password_help','yes') }}
                     <span class="error">
                     	@if ($errors->has('email'))
                        {{ $errors->first('email') }}
                        @endif
                     </span>
                     </div>
                     <div class="col-md-3">{{ Form::submit('Submit')}}</div>
                     </div>    
                     
                     {{ Form::close()}}                 
                </div>

            </div>
        </div>
    </div>
</div>
@include('parts.global.footer')