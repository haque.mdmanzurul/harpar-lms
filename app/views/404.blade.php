
@section('header')
  @include('parts.global.header')
@stop
<div class="pagewrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 content-wrapper">
                <h3>Page not found. Please try later.</h3>
            </div>
        </div>
    </div>
</div>
@section('footer')
  @include('parts.global.footer')
@stop