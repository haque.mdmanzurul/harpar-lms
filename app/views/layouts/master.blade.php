
    @yield('header')
    <div class="pagewrapper">
    	<div class="container-fluid">
    		<div class="row">
    			<div class="col-md-2 col-sm-2 sidebar-wrapper">
    				@yield('sidebar')
    			</div>
    			<div class="col-md-10 col-sm-10 content-wrapper">
    				@yield('content')
    			</div>
    		</div>
    	</div>
    </div>
    @yield('footer')
