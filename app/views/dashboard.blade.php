@extends('layouts/master')
@section('header')
  @include('parts.global.header')
@stop

@section('sidebar')
  @include('parts.global.sidebar')
@stop

@section('content')
  @if ($template_part=='allfeedback')
    @include('parts.dashboard.allfeedback')

  @elseif ($template_part=='allSubmissions')
    @include('parts.dashboard.allsubmissions')

  @elseif ($template_part=='userDashboard')
    @include('parts.dashboard.userDashboard')

  @endif
@stop

@section('footer')
  @include('parts.global.footer')
@stop