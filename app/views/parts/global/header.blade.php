<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Harpar LMS @if(isset($page_title) AND $page_title!="") {{ $page_title }} @endif</title>

    <!-- Bootstrap -->
    <link href="{{ $siteurl }}/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ $siteurl }}/css/style.css" rel="stylesheet">
    <link href="{{ $siteurl }}/css/responsive-nav.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ $siteurl }}/css/font-awesome.min.css">
	<link href="{{ $siteurl }}/images/favicon.png" rel="shortcut icon">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
	var base_url = "{{ $siteurl }}";
	</script>
  </head>
  <body>

  <div class="header-wrapper">
  	<div class="container-fluid header">
  		<div class="row">
  			<div class="col-md-2 col-sm-2">
  				<a href="{{ $siteurl }}" class="logo"><img src="{{ $siteurl }}/images/logo.png" class="img-responsive"></a>
  			</div>
  			<div class="col-md-3 col-sm-3">
	  			<ul class="header-left-nav pull-left hidden-sm hidden-xs">
	  				<li><a href="{{ $siteurl }}">Harpar LMS</a></li>
	  				<li><a href="{{ $siteurl }}">@if (Auth::check() and Auth::user()->user_type == 0) Admin Area @elseif (Auth::check() and Auth::user()->user_type == 1) Client Area @else Login Area @endif</a></li>
	  			</ul>
  			</div>
  			<div class="col-md-7 col-sm-10">

            @if (Auth::check())
 	  			<ul class="header-left-nav pull-right">
	  				<li><a href="/users/{{Auth::user()->username}}/userinfo">My Account</a></li>
                    @if (Auth::user()->user_type == 1)
                    
                   @if (isset($user_courses) and count($user_courses)>0)
                   <li>
                         <a aria-expanded="true" role="button" aria-haspopup="true" data-toggle="dropdown" class="dropdown-toggle" href="#" id="drop4">
                           @if(isset($user_current_course)) 
                           {{ $user_current_course->title }} 
                           @else
                           Select a Course
                           @endif
                          <span class="caret"></span> 
                        </a>        
                    @define $i = 1
                         @if (count($user_courses)>0)
                        <ul aria-labelledby="drop4" role="menu" class="dropdown-menu" id="menu1">
                        
                        
                        @foreach ($user_courses as $uc)
                            @if(isset($user_current_course) and $user_current_course->id == $uc->id)
                            @define continue
                            @endif
                             <li role="presentation"><a href="{{ $siteurl }}/course/{{ $uc->course_slug }}/{{ base64_encode($uc->moduleID) }}" tabindex="-1" role="menuitem">{{ $uc->title }}</a></li>
                         @define $i++
                        @endforeach
                         
                         </ul>
                         @endif
                    
                    </li>
                    @endif
                    @else
                    @if (Auth::user()->user_type == 1)
                    <li><a href="#">No Course Found</a></li>
                    @endif
            @endif
	  				<li><a href="/logout">Logout</a></li>
	  			</ul> 
            @endif    			
  			</div>
  		</div>
  	</div>
  </div>
@if (isset($template_part) and ($template_part == "userinfo" or $template_part == "viewuser" or $template_part == "showcourse" or $template_part == "edituser" or  $template_part == "courselist" or  $template_part == "accreditations" or  $template_part == "qualifications" or $template_part == "certificates")  and Auth::user()->user_type==1)
<div class="user-welcome-wrapper">
	<div class="container-fluid">  
		<div class="row ">
            <div class="col-md-2 col-sm-2 user-image">
                @if ($user->user_image != '')<img src="/uploads/{{ $user->user_image }}" class="img-circle user-account-image" /> @endif
            </div>
            <div class="col-md-10 col-sm-10 user-details">
                <h1> Hi {{ Auth::user()->firstname }} {{ Auth::user()->lastname }}, Welcome to the Harpar LMS. 
       </h1>
                <p> @if(isset($user_current_course)) You have {{ $user_left_modules }} modules to complete by {{ date('jS F Y',strtotime($user_current_course->exp_date)) }} for course:  {{ $user_current_course->title }} @endif</p>
            </div>
		</div> 
    </div>
</div>  
@endif
