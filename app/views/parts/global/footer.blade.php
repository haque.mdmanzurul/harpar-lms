	<div class="footer-wrapper">
	<div class="container-fluid footer">
		<div class="row">
        	<div class="col-md-6">
            	<span class="copyright">Copyright © 2015 - {{date('Y')}} Harpar</span>
            </div>
            <div class="col-md-6">
            <span class="footer-logo"><img class="img-responsive pull-right" src="{{ $siteurl }}/images/footer-right-image.png" /></span>
            </div>
        </div>
	</div>
	</div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="{{ $siteurl }}/js/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="{{ $siteurl }}/js/fancybox/source/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="{{ $siteurl }}/js/fancybox/source/jquery.fancybox.css?v=2.1.5" media="screen" />

    <!-- Add Media helper (this is optional) -->
    <script type="text/javascript" src="{{ $siteurl }}/js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{ $siteurl }}/js/bootstrap.min.js"></script> 
    <script src="{{ $siteurl }}/js/jquery.tablesorter.js"></script>     
    <!-- <script src="{{ $siteurl }}/js/select.js"></script>  -->
    <script src="{{ $siteurl }}/js/custom.js"></script>


    
    <!--Date Pikcer -->
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script type="text/javascript">
	  $(function() {

		  $( "#expiry_date" ).datepicker({dateFormat: "yy-mm-dd"});

	  });	
	</script>
    <script type="text/javascript">

        $(document).ready(function() {
            var vcode, type;
            $(".youtube").click(function(){
                vcode = $(this).attr('data-video-code');
                type  = $(this).attr('data-media-type');
                $(".youtube").fancybox({
                    maxWidth	: 800,
                    maxHeight	: 600,
                    fitToView	: false,
                    width		: '70%',
                    height		: '70%',
                    autoSize	: false,
                    closeClick	: false,
                    openEffect	: 'none',
                    closeEffect	: 'none',
                    afterLoad   : function(){
                        //console.log();
                        if(type == 'youtube')
                            $('#harpar-media-player').html('<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'+vcode+'" frameborder="0" autoplay="1" allowfullscreen></iframe>');
                        else if(type == 'vimeo')
                            $('#harpar-media-player').html('<iframe src="//player.vimeo.com/video/'+vcode+'" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
                        else if(type == 'story'){
                            $('#harpar-media-player').html('<video width="100%" controls>' +
                                    '<source src="'+vcode+'" type="video/mp4">' +
                                    'Your browser does not support HTML5 video.</video>');
                        }

                    },
                    afterClose  : function(){
                        $('#harpar-media-player').html('');
                    }
                });
            });
            $(".story").click(function(){
                vcode = $(this).attr('data-video-code');
                type  = $(this).attr('data-media-type');
                $(".story").fancybox({
                    maxWidth	: 800,
                    maxHeight	: 600,
                    fitToView	: false,
                    width		: '70%',
                    height		: '70%',
                    autoSize	: false,
                    closeClick	: false,
                    openEffect	: 'none',
                    closeEffect	: 'none',
                    afterLoad   : function(){
                        //console.log();
                        if(type == 'youtube')
                            $('#harpar-media-player').html('<iframe width="100%" height="100%" src="https://www.youtube.com/embed/'+vcode+'" frameborder="0" autoplay="1" allowfullscreen></iframe>');
                        else if(type == 'vimeo')
                            $('#harpar-media-player').html('<iframe src="//player.vimeo.com/video/'+vcode+'" width="100%" height="100%" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>');
                        else if(type == 'story'){
                            $('#harpar-media-player').html('<video width="100%" controls>' +
                                    '<source src="'+vcode+'" type="video/mp4">' +
                                    'Your browser does not support HTML5 video.</video>');
                        }

                    },
                    afterClose  : function(){
                        $('#harpar-media-player').html('');
                    }
                });
            });

        });


    </script>



    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.0.8/angular.min.js"></script>
    <script src="{{ $siteurl }}/js/app.js"></script>
    <script src="{{ $siteurl }}/js/responsive-nav.js"></script>
    <script>
        var navigation = responsiveNav("sidebar-menu", {customToggle: ".nav-toggle"});
    </script>
    <script src="{{ $siteurl }}/js/tinymce/tinymce.min.js"></script>
    <script>tinymce.init({
            mode : "specific_textareas",
            editor_selector : "mceEditor",
            height: 300,
            menubar: false,
            plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table contextmenu paste code'
            ],
            toolbar: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
        });</script>
  </body>
</html>