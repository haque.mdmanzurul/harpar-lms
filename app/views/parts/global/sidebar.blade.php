<a href="#sidebar-menu" class="nav-toggle mobile-menu">

	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>

</a>
{{--<button type="button" href="#sidebar-menu" class="nav-togglenavbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-menu" aria-expanded="false" aria-controls="navbar">
	<span class="sr-only">Toggle navigation</span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
	<span class="icon-bar"></span>
</button>--}}
<div class="sidebar" id="sidebar-menu">
	<ul class="main-nav">

		@if(Auth::user()->user_type == 0)
		<li {{ ($selected=='dashboard') ? 'class="active"' : '' }}><a href="{{ $siteurl }}">Dashboard <i class="fa fa-play"></i></a> </li>
		<li {{ ($selected=='courses') ? 'class="active"' : '' }}><a href="{{ $siteurl }}/courses">Courses <i class="fa fa-play"></i></a></li>
        <li {{ ($selected=='accreditations') ? 'class="active"' : '' }}><a href="{{ $siteurl }}/accreditations">Accreditations <i class="fa fa-play"></i></a></li>
			<li {{ ($selected=='qualifications') ? 'class="active"' : '' }}><a href="{{ $siteurl }}/qualifications">Qualifications <i class="fa fa-play"></i></a></li>
			
			<li {{ ($selected=='users') ? 'class="active"' : '' }}><a href="{{ $siteurl }}/users">Users <i class="fa fa-play"></i></a></li>
		<li {{ ($selected=='settings') ? 'class="active"' : '' }}><a href="{{ $siteurl }}/settings">Settings <i class="fa fa-play"></i></a></li>
        @else
		<li {{ ($selected=='users') ? 'class="active"' : '' }}><a href="{{ $siteurl }}/users/{{Auth::user()->username}}/userinfo/">My Account <i class="fa fa-play"></i></a> </li>
		<li {{ ($selected=='courses') ? 'class="active"' : '' }}><a href="{{ $siteurl }}/courses">Courses <i class="fa fa-play"></i></a></li>
        <li {{ ($selected=='accreditations') ? 'class="active"' : '' }}><a href="{{ $siteurl }}/accreditations">Accreditations <i class="fa fa-play"></i></a></li>
			<li {{ ($selected=='qualifications') ? 'class="active"' : '' }}><a href="{{ $siteurl }}/qualifications">Qualifications <i class="fa fa-play"></i></a></li>
			
        <li {{ ($selected=='certificates') ? 'class="active"' : '' }}><a href="{{ $siteurl }}/certificates">Certificates <i class="fa fa-play"></i></a></li>           
        @endif
	</ul>
</div>