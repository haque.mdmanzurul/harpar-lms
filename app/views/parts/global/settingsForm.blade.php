<h3>Settings</h3>
       @if (Session::has('flash_notice'))
            <p class="bg-primary">
            {{ Session::get('flash_notice') }}
            </p>
        @endif
<div class="settings-container">
{{ Form::open(array('url' => 'settings','method'=>'post')) }}
	<div class="adduser-email general">
 
        <div class="col-md-9">
             <p>
                 {{ Form::label('Admin Email')}}
                 {{ Form::text('admin_email', $setting->admin_email)}}
                 <span class="error">
                    @if ($errors->has('admin_email'))
                    {{ $errors->first('admin_email') }}
                    @endif
                 </span>
             </p>  
             
             <p style="display: none">
                 {{ Form::label('Default Modules Block On Course Add')}}
                 {{ Form::text('start_module_count', $setting->start_module_count)}}
                 <span class="error">
                    @if ($errors->has('start_module_count'))
                    {{ $errors->first('start_module_count') }}
                    @endif
                 </span>
             </p>

            @if(!isset($setting->paypal_client_id))
                @define $paypal_client_id=''
            @else
                @define $paypal_client_id= $setting->paypal_client_id
            @endif
            <p style="display: none">
                {{ Form::label('Paypal Client ID')}}
                {{ Form::text('paypal_client_id', $paypal_client_id)}}
                <span class="error">
                    @if ($errors->has('paypal_client_id'))
                        {{ $errors->first('paypal_client_id') }}
                    @endif
                 </span>
            </p>

            @if(!isset($setting->paypal_client_secret))
                @define $paypal_client_secret = ''
            @else
                @define $paypal_client_secret = $setting->paypal_client_secret
            @endif
            <p style="display: none">
                {{ Form::label('Paypal Client Secret')}}

                {{ Form::text('paypal_client_secret', $paypal_client_secret)}}
                <span class="error">
                    @if ($errors->has('paypal_client_secret'))
                        {{ $errors->first('paypal_client_secret') }}
                    @endif
                 </span>
            </p>

            @if(!isset($setting->paypal_mode))
                @define $paypal_mode = 'Sandbox'
            @else
                @define $paypal_mode = $setting->paypal_mode
            @endif
            <p style="display: none">
                {{ Form::label('Paypal Environment')}}
                @if($paypal_mode == 'sandbox')
                    {{ Form::radio('paypal_mode', 'sandbox', true)}}
                @else
                    {{ Form::radio('paypal_mode', 'sandbox')}}
                @endif
                Sandbox
                @if($paypal_mode == 'live')
                    {{ Form::radio('paypal_mode', 'live', true)}}
                @else
                    {{ Form::radio('paypal_mode', 'live')}}
                @endif
                Live
                <span class="error">
                    @if ($errors->has('paypal_mode'))
                        {{ $errors->first('paypal_mode') }}
                    @endif
                 </span>
            </p>

            @if(!isset($setting->certification_flat_fee))
                @define $certification_flat_fee=''
            @else
                @define $certification_flat_fee = $setting->certification_flat_fee
            @endif
            <p style="display: none">
                {{ Form::label('Certification Flat Fee')}}
                {{ Form::text('certification_flat_fee', $certification_flat_fee)}}
                <span class="error">
                    @if ($errors->has('certification_flat_fee'))
                        {{ $errors->first('certification_flat_fee') }}
                    @endif
                 </span>
            </p>

            <p>
             {{ Form::submit('Save > ') }}
             </p>              
 
       </div>         
 </div>


	<div class="adduser-email">
 
        <div class="col-md-9">
            
             <p>
                 {{ Form::label('New User Added Email Confirmation')}}
                 {{ Form::textarea('usercreation_email', $setting->usercreation_email)}}
                 <span class="error">
                    @if ($errors->has('usercreation_email'))
                    {{ $errors->first('usercreation_email') }}
                    @endif
                 </span>
             </p> 
              <p>
  			 {{ Form::hidden('usercreation_template','userCreateEmail')}}  
             {{ Form::submit('Save > ') }}
             </p>   
       </div>
       <div class="col-md-3">
       Used tags(Do not modify)<br />
       
       Firstname:  @{{firstname}} <br/>
       Lastname:   @{{lastname}}  <br/>
       Username:   @{{username}} <br/>
       Password:   @{{password}}  <br/>
       </div>         
 </div>
 
 	<div class="adduser-email">
  
        <div class="col-md-9">
             <p>
                 {{ Form::label('Admin: New Document Pending Submission Email ')}}
                 {{ Form::textarea('usersubmission_email', $setting->usersubmission_email)}}
                 <span class="error">
                    @if ($errors->has('usersubmission_email'))
                    {{ $errors->first('usersubmission_email') }}
                    @endif
                 </span>
             </p>    
              <p>
  			 {{ Form::hidden('usersubmission_template','userSubmissionEmail')}} 
             {{ Form::submit('Save > ') }}
             </p>   
 
       </div>
       <div class="col-md-3">
       Used tags(Do not modify)<br />
       
       Course Title:   @{{coursetitle}} <br/>
       Module Title:   @{{moduletitle}} <br/>
       </div>         
 </div>
 
 
 	<div class="adduser-email">
 
        <div class="col-md-9">
             <p>
                 {{ Form::label('User: Moderation Confirmed Email')}}
                 {{ Form::textarea('usersubmission_moderated_email', $setting->usersubmission_moderated_email)}}
                 <span class="error">
                    @if ($errors->has('usersubmission_moderated_email'))
                    {{ $errors->first('usersubmission_moderated_email') }}
                    @endif
                 </span>
             </p>    
              <p>
  			 {{ Form::hidden('usersubmission_moderated_template','userSubmissionModeratedEmail') }} 
             {{ Form::submit('Save > ') }}
             </p>   
 
       </div>
       <div class="col-md-3">
       Used tags(Do not modify)<br />
       
       Firstname:  @{{firstname}} <br/>
       Lastname:   @{{lastname}}  <br/>
       Course Title:   @{{coursetitle}} <br/>
       Module Title:   @{{moduletitle}} <br/>
       </div>         
        
 </div>
 
 	<div class="adduser-email">
        <div class="col-md-9">
             <p>
                 {{ Form::label('User: Admin feedback email')}}
                 {{ Form::textarea('adminfeedback_email', $setting->adminfeedback_email)}}
                 <span class="error">
                    @if ($errors->has('adminfeedback_email'))
                    {{ $errors->first('adminfeedback_email') }}
                    @endif
                 </span>
             </p>    
              <p>
 			 {{ Form::hidden('adminfeedback_template','adminFeedbackToUser')}}              
             {{ Form::submit('Save > ') }}
             </p>   
  
       </div>
       <div class="col-md-3">
       Used tags(Do not modify)<br />
       
       Firstname:  @{{firstname}} <br/>
       Lastname:   @{{lastname}}  <br/>
       Course Title:   @{{coursetitle}} <br/>
       Module Title:   @{{moduletitle}} <br/>
       </div>         
 </div>
 
 	<div class="adduser-email">
 
        <div class="col-md-9">

             <p>
                 {{ Form::label('Admin: User Feedback Email')}}
                 {{ Form::textarea('userfeedback_email', $setting->userfeedback_email)}}
                 <span class="error">
                    @if ($errors->has('userfeedback_email'))
                    {{ $errors->first('userfeedback_email') }}
                    @endif
                 </span>
             </p>    
              <p>
 			 {{ Form::hidden('userfeedback_template','userFeedbackToAdmin')}} 
             {{ Form::submit('Save > ') }}
             </p>   
            </div>
       <div class="col-md-3">
       Used tags(Do not modify)<br />
       
       Firstname:  @{{firstname}} <br/>
       Lastname:   @{{lastname}}  <br/>
       Course Title:   @{{coursetitle}} <br/>
       Module Title:   @{{moduletitle}} <br/>
       </div>         
 	</div>
 	
    <!-- Forgot Password -->
 	<div class="adduser-email">
 
        <div class="col-md-9">
             <p>
                 {{ Form::label('User: Forgot Password Email')}}
                 {{ Form::textarea('forgotpassword_email', $setting->forgotpassword_email)}}
                 <span class="error">
                    @if ($errors->has('forgotpassword_email'))
                    {{ $errors->first('forgotpassword_email') }}
                    @endif
                 </span>
             </p>    
              <p>
  			 {{ Form::hidden('forgotpassword_template','forgotpassword')}}
             {{ Form::submit('Save > ') }}
             </p>   
            </div>
       <div class="col-md-3">
       Used tags(Do not modify)<br />
       
       Firstname:  @{{firstname}} <br/>
       Lastname:   @{{lastname}}  <br/>
       Password Reset Token:   @{{token}}  (Do not dlete this.) <br/> 
       </div>         
 </div>
 
    <!-- Forgot Username -->
 	<div class="adduser-email">
        <div class="col-md-9">
             <p>
                 {{ Form::label('User: Forgot Username Email')}}
                 {{ Form::textarea('forgotusername_email', $setting->forgotusername_email)}}
                 <span class="error">
                    @if ($errors->has('forgotusername_email'))
                    {{ $errors->first('forgotusername_email') }}
                    @endif
                 </span>
             </p>    
              <p>
 			 {{ Form::hidden('forgotusername_template','forgotusername')}} 
             {{ Form::submit('Save > ') }}
             </p>   
 
       </div>
       <div class="col-md-3">
       Used tags(Do not modify)<br />
       
       Firstname:  @{{firstname}} <br/>
       Lastname:   @{{lastname}}  <br/>
       Username:   @{{username}} <br/> 
       </div>         
 </div> 
 
    <!-- Passsword Changed -->
 	<div class="adduser-email">
 
        <div class="col-md-9">
             <p>
                 {{ Form::label('User: Password changed confirmation email')}}
                 {{ Form::textarea('passwordchanged_email', $setting->passwordchanged_email)}}
                 <span class="error">
                    @if ($errors->has('passwordchanged_email'))
                    {{ $errors->first('passwordchanged_email') }}
                    @endif
                 </span>
             </p>    
              <p>
 			 {{ Form::hidden('passwordchanged_template','passwordchanged')}}
             {{ Form::submit('Save > ') }}
             </p>   
            </div>
       <div class="col-md-3">
       Used tags(Do not modify)<br />
       
       Firstname:  @{{firstname}} <br/>
       Lastname:   @{{lastname}}  <br/>
       Username:   @{{username}} <br/> 
       Password:   @{{password}} <br/> 
       </div>         
 </div> 
 
    <!-- Password Emailed -->
 	<div class="adduser-email">
 
        <div class="col-md-9">
             <p>
                 {{ Form::label('User:  Admin Password Reset ')}}
                 {{ Form::textarea('passwordgenerated_email', $setting->passwordgenerated_email)}}
                 <span class="error">
                    @if ($errors->has('passwordgenerated_email'))
                    {{ $errors->first('passwordgenerated_email') }}
                    @endif
                 </span>
             </p>    
              <p>
             {{ Form::hidden('passwordgenerated_template','passwordemailed')}}
             {{ Form::submit('Save > ') }}
             </p>   

       </div>
       <div class="col-md-3">
       Used tags(Do not modify)<br />
       
       Firstname:  @{{firstname}} <br/>
       Lastname:   @{{lastname}}  <br/>
       Username:   @{{username}} <br/> 
       </div>         
 </div>

    <!-- Password Emailed -->
    <div class="adduser-email hidden">

        <div class="col-md-9">
            <p>
                {{ Form::label('Certification Invitation Email ')}}
                {{ Form::textarea('usercourse_certification_invite_code_email', $setting->usercourse_certification_invite_code_email)}}
                <span class="error">
                    @if ($errors->has('usercourse_certification_invite_code_email'))
                        {{ $errors->first('usercourse_certification_invite_code_email') }}
                    @endif
                 </span>
            </p>
            <p>
                {{ Form::hidden('usercourse_certification_invite_code_template','usercourse_certification_invite_code')}}
                {{ Form::submit('Save > ') }}
            </p>

        </div>
        <div class="col-md-3">
            Used tags(Do not modify)<br />

            Firstname:  @{{firstname}} <br/>
            Lastname:   @{{lastname}}  <br/>
            Course Title:   @{{coursetitle}} <br/>
            Token : @{{token}}
        </div>
    </div>

    <!-- Password Emailed -->
    <div class="adduser-email">

        <div class="col-md-9">
            <p>
                @if(!isset($setting->user_certification_completion_email))
                    @define $user_certification_completion_email = ''
                    @else
                    @define $user_certification_completion_email = $setting->user_certification_completion_email
                @endif
                {{ Form::label('Certification Completion Email ')}}
                {{ Form::textarea('user_certification_completion_email', $user_certification_completion_email)}}
                <span class="error">
                    @if ($errors->has('user_certification_completion_email'))
                        {{ $errors->first('user_certification_completion_email') }}
                    @endif
                 </span>
            </p>
            <p>
                {{ Form::hidden('user_certification_completion_email_template','user_certification_completion_email')}}
                {{ Form::submit('Save > ') }}
            </p>

        </div>
        <div class="col-md-3">
            Used tags(Do not modify)<br />

            Firstname:  @{{firstname}} <br/>
            Lastname:   @{{lastname}}  <br/>
            Course Title:   @{{coursetitle}} <br/>
        </div>
    </div>
    
    <!-- Password Emailed -->
    <div class="adduser-email">

        <div class="col-md-9">
            <p>
                @if(!isset($setting->certificateDownloadEmail))
                    @define $certificateDownloadEmail = ''
                    @else
                    @define $certificateDownloadEmail = $setting->certificateDownloadEmail
                @endif
                {{ Form::label('Assessment email with certificate attached')}}
                {{ Form::textarea('certificateDownloadEmail', $certificateDownloadEmail)}}
                <span class="error">
                    @if ($errors->has('certificateDownloadEmail'))
                        {{ $errors->first('certificateDownloadEmail') }}
                    @endif
                 </span>
            </p>
            <p>
                {{ Form::hidden('certificateDownloadEmail_template','certificateDownloadEmail')}}
                {{ Form::submit('Save > ') }}
            </p>

        </div>
        <div class="col-md-3">
            Used tags(Do not modify)<br />

            Firstname:  @{{firstname}} <br/>
            Lastname:   @{{lastname}}  <br/>
            Course Title:   @{{coursetitle}} <br/>
        </div>
    </div>

    <!-- Password Emailed -->
    <div class="adduser-email">

        <div class="col-md-9">
            <p>
                @if(!isset($setting->adminSuccessFailEmail))
                    @define $adminSuccessFailEmail = ''
                @else
                    @define $adminSuccessFailEmail = $setting->adminSuccessFailEmail
                @endif
                {{ Form::label('Admin: Learner Assessment Success/Fail Email')}}
                {{ Form::textarea('adminSuccessFailEmail', $adminSuccessFailEmail)}}
                <span class="error">
                    @if ($errors->has('adminSuccessFailEmail'))
                        {{ $errors->first('adminSuccessFailEmail') }}
                    @endif
                 </span>
            </p>
            <p>
                {{ Form::hidden('adminSuccessFailEmail_template','adminSuccessFailEmail')}}
                {{ Form::submit('Save > ') }}
            </p>

        </div>
        <div class="col-md-3">
            Used tags(Do not modify)<br />

            Firstname:  @{{firstname}} <br/>
            Lastname:   @{{lastname}}  <br/>
            Course Title:   @{{coursetitle}} <br/>
        </div>
    </div>

    <!-- Password Emailed -->
    <div class="adduser-email">

        <div class="col-md-9">
            <p>
                @if(!isset($setting->accreditationEmail))
                    @define $accreditationEmail = ''
                @else
                    @define $accreditationEmail = $setting->accreditationEmail
                @endif
                {{ Form::label('Learner Accreditation Course Completion Email')}}
                {{ Form::textarea('accreditationEmail', $accreditationEmail)}}
                <span class="error">
                    @if ($errors->has('accreditationEmail'))
                        {{ $errors->first('accreditationEmail') }}
                    @endif
                 </span>
            </p>
            <p>
                {{ Form::hidden('accreditationEmail_template','accreditationEmail')}}
                {{ Form::submit('Save > ') }}
            </p>

        </div>
        <div class="col-md-3">
            Used tags(Do not modify)<br />

            Firstname:  @{{firstname}} <br/>
            Lastname:   @{{lastname}}  <br/>
            Course Title:   @{{coursetitle}} <br/>
        </div>
    </div>

    <!-- Password Emailed -->
    <div class="adduser-email">

        <div class="col-md-9">
            <p>
                @if(!isset($setting->qualificationEmail))
                    @define $qualificationEmail = ''
                @else
                    @define $qualificationEmail = $setting->qualificationEmail
                @endif
                {{ Form::label('Learner Qualification Completion/Pass Email')}}
                {{ Form::textarea('qualificationEmail', $qualificationEmail)}}
                <span class="error">
                    @if ($errors->has('qualificationEmail'))
                        {{ $errors->first('qualificationEmail') }}
                    @endif
                 </span>
            </p>
            <p>
                {{ Form::hidden('qualificationEmail_template','qualificationEmail')}}
                {{ Form::submit('Save > ') }}
            </p>

        </div>
        <div class="col-md-3">
            Used tags(Do not modify)<br />

            Firstname:  @{{firstname}} <br/>
            Lastname:   @{{lastname}}  <br/>
            Course Title:   @{{coursetitle}} <br/>
        </div>
    </div>
    {{ Form::close() }}
</div>