<div class="userview">

<div class="row user-review">  
	<div class="col-md-8">  	
        <h3>Submission History</span></h3>
    </div>
    <div class="col-md-4"><a onclick="history.back(-1); return false;" href="#" class="pinkbutton pull-right"><i class="fa fa-angle-left"></i> Back 
</a></div>
	<div class="col-md-12"> 
    	
    	<div class="module-review"> 	
        	
            
   <table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="head">
    <td width="5%">&nbsp;</td>
    <td width="20%">Title</td>
    <td width="35%">Latest Submission</td>
    <td width="20%"></td>
    <td width="20%">Date</td> 
  </tr>
  @define $i=count($user_submissions)
  @foreach($user_submissions as $us)
  <tr class="odd">
    <td>{{ $i }}</td>
    <td>{{ $us->title }}</td>
    <td>{{ $us->attachment_name }}  </td>
<td>
    <span class="download"><a href="{{ $siteurl }}/download/{{ isset($us)?base64_encode($us->submission_id):'' }}" class="pinkbutton">Download <i class="fa fa-angle-right"></i>
</a></span>
</td>      
    <td>
      {{ $us->submission_time }}
</td> 
  </tr>
  @define $i--
  @endforeach
</table>

        </div>
    </div>    
</div>

<div class="row">
<div class="col-md-12 examiner-feedback">     	
    	<div class="module-review pinkbg"> 
        	<div class="module-review-inner"> 	
                <h3>Feedback/Help Requests</h3>
                <a href="#inline1" moduleID="{{ $module->id }}" courseID="{{ $module->courseID }}" userID="{{ Auth::user()->id }}" class="user_feedback fancybox pull-right margin-top-float">Post New Feedback</a>
                <div class="col-md-12 feedbacks">
                @if (count($user_feedbacks)>0)
                
                    @foreach($user_feedbacks as $uf)
                      <div class="row">
                      	 <div class="col-md-1">
                         @if ($uf->user_image!="")
                         <img class="img-circle feedback-user-image" src="{{ $siteurl.'/uploads/'.$uf->user_image }}" />
                         @else
                         <img class="img-circle feedback-user-image" src="{{ $siteurl }}/images/default-user.png" />
                         @endif
                         </div>
                         <div class="col-md-2">{{ date('d/m/Y', strtotime($uf->created_at)) }}</div>
                         <div class="col-md-9">{{ $uf->message}} {{ $uf->attachment_links}}</div>
                      </div>
                    @endforeach
                @endif  
                </div>
            </div>
        </div>
    </div>
    </div>
</div>
<div id="inline1" style="width:500px;display: none;">
    <form name="feedbackForm" id="adminFeedbackForm" class="feedbackForm" method="post" action="#">
        <h3>Questions or feedback? </h3>
        <p>
            Complete the form below
        </p>

        <p>
            <input type="hidden" name="feedbackModuleID" id="feedbackModuleID" value="" />
            <input type="hidden" name="feedbackCourseID" id="feedbackCourseID" value="" />
            <input type="hidden" name="feedbackUserID" id="feedbackUserID" value="" />
            <input type="hidden" name="feedbackAttachmentsID" id="feedbackAttachmentsID" value="" />

            <textarea name="feedbackmessage" id="feedbackmessage" cols="40" rows="5" required="required"></textarea>
            <br/>
        <div class="module-upload">
            <h3>Attachments</h3>
            <div class="attachment-container">
                <div class="attachment-container-inner">
                    <div class="fields-container">
                        <div class="drop">

                            <input type="file" multiple="" id="attachment_submission" name="attachment_submission">



                            <a id="addnew_attachment_trigger"  onclick="return addNewAttachment({{ Auth::user()->id }})" user_id="{{ Auth::user()->id }}" disabled-message="" href="#" class="pinkbutton"> Add <i class="fa fa-angle-right"></i></a>


                            <a tabindex="0" href="javascript:void()" id="lock_msg"  role="button" data-toggle="popover" data-trigger="focus" data-content="Browse for certificate, supported type: jpg, jpeg, gif, pdf,doc,docx"><i class="fa fa-info-circle"></i>
                            </a>


                        </div>

                        <ul class="user-attachment-items" id="attachment-items">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </p>
        <p id="feedbackProgress"></p>
        <a class="pinkbutton" href="#" id="feedbackFormTrigger">Submit <i class="fa fa-angle-right"></i> </a>
    </form>
</div>