<style type="text/css">
    .form-group{
        overflow: hidden;
    }
    .user-info .module-review input[type="submit"] {
        background: #c91d82 none repeat scroll 0 0;
        border: 1px solid #c91d82;
        border-radius: 18px;
        box-shadow: none !important;
        color: #fff;
        font-family: "Open Sans",sans-serif;
        font-size: 17px;
        font-weight: 500;
        height: 36px;
        letter-spacing: 0.03cm;
        margin: 10px auto 0;
        padding: 2px 25px 5px;
        text-shadow: none !important;
        text-transform: uppercase;
        width: auto;
    }
</style>
<div class="row courses">
    <div class="col-md-7">
        <h3>Please complete the payment to unlock certification</h3>
    </div>
</div>


<div class="row user-info">
    <div class="col-md-12">
        <div class="module-review">

            <div class="adduser-email">

                <div class="col-md-9">
                    {{ Form::open(array('url' => 'payment','method'=>'post')) }}
                    <p>
                        {{ Form::label('Certification Course Title: '.$coursetitle)}}
                        {{ Form::hidden('coursetitle',$coursetitle)}}

                    </p>
                    <p>
                        {{ Form::label('Certifiication Price: '.$certification_price.' GBP')}}
                        {{ Form::hidden('amount',$certification_price)}}

                    </p>

                        <h3 style="margin-left: 0; margin-right: 0">Billing Details Information</h3>
                    @if ($errors->has())
                        <div class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="card-holder-firstname">First Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="card-holder-firstname" id="card-holder-firstname" placeholder="Card Holder's Name">
                            </div>
                        </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="card-holder-lastname">Last Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="card-holder-lastname" id="card-holder-lastname" placeholder="Card Holder's Name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="card-holder-name">Address Line 1</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="addressline1" id="addressline1" placeholder="Address Line 1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label" for="card-holder-name">Address Line 2</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="addressline2" id="addressline2" placeholder="Address Line 2">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="zip" class="col-sm-3 control-label">
                            Zip Code
                        </label>
                        <div class="col-sm-2"><input name="zip" type="text" class="form-control" value="" id="zip">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="city" class="col-sm-3 control-label">
                            City
                        </label>
                        <div class="col-sm-6"><input name="city" type="text" class="form-control" value="" id="city">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="country" class="col-sm-3 control-label">
                            Country
                        </label>
                        <div class="col-sm-9">
                            <select name="country" id="country" class="form-control">
                                <option value="AR">Argentina</option>
                                <option value="AU">Australia</option>
                                <option value="AT">Austria</option>
                                <option value="BY">Belarus</option>
                                <option value="BE">Belgium</option>
                                <option value="BA">Bosnia and Herzegovina</option>
                                <option value="BR">Brazil</option>
                                <option value="BG">Bulgaria</option>
                                <option value="CA">Canada</option>
                                <option value="CL">Chile</option>
                                <option value="CN">China</option>
                                <option value="CO">Colombia</option>
                                <option value="CR">Costa Rica</option>
                                <option value="HR">Croatia</option>
                                <option value="CU">Cuba</option>
                                <option value="CY">Cyprus</option>
                                <option value="CZ">Czech Republic</option>
                                <option value="DK">Denmark</option>
                                <option value="DO">Dominican Republic</option>
                                <option value="EG">Egypt</option>
                                <option value="EE">Estonia</option>
                                <option value="FI">Finland</option>
                                <option value="FR">France</option>
                                <option value="GE">Georgia</option>
                                <option value="DE">Germany</option>
                                <option value="GI">Gibraltar</option>
                                <option value="GR">Greece</option>
                                <option value="HK">Hong Kong S.A.R., China</option>
                                <option value="HU">Hungary</option>
                                <option value="IS">Iceland</option>
                                <option value="IN">India</option>
                                <option value="ID">Indonesia</option>
                                <option value="IR">Iran</option>
                                <option value="IQ">Iraq</option>
                                <option value="IE">Ireland</option>
                                <option value="IL">Israel</option>
                                <option value="IT">Italy</option>
                                <option value="JM">Jamaica</option>
                                <option value="JP">Japan</option>
                                <option value="KZ">Kazakhstan</option>
                                <option value="KW">Kuwait</option>
                                <option value="KG">Kyrgyzstan</option>
                                <option value="LA">Laos</option>
                                <option value="LV">Latvia</option>
                                <option value="LB">Lebanon</option>
                                <option value="LT">Lithuania</option>
                                <option value="LU">Luxembourg</option>
                                <option value="MK">Macedonia</option>
                                <option value="MY">Malaysia</option>
                                <option value="MT">Malta</option>
                                <option value="MX">Mexico</option>
                                <option value="MD">Moldova</option>
                                <option value="MC">Monaco</option>
                                <option value="ME">Montenegro</option>
                                <option value="MA">Morocco</option>
                                <option value="NL">Netherlands</option>
                                <option value="NZ">New Zealand</option>
                                <option value="NI">Nicaragua</option>
                                <option value="KP">North Korea</option>
                                <option value="NO">Norway</option>
                                <option value="PK">Pakistan</option>
                                <option value="PS">Palestinian Territory</option>
                                <option value="PE">Peru</option>
                                <option value="PH">Philippines</option>
                                <option value="PL">Poland</option>
                                <option value="PT">Portugal</option>
                                <option value="PR">Puerto Rico</option>
                                <option value="QA">Qatar</option>
                                <option value="RO">Romania</option>
                                <option value="RU">Russia</option>
                                <option value="SA">Saudi Arabia</option>
                                <option value="RS">Serbia</option>
                                <option value="SG">Singapore</option>
                                <option value="SK">Slovakia</option>
                                <option value="SI">Slovenia</option>
                                <option value="ZA">South Africa</option>
                                <option value="KR">South Korea</option>
                                <option value="ES">Spain</option>
                                <option value="LK">Sri Lanka</option>
                                <option value="SE">Sweden</option>
                                <option value="CH">Switzerland</option>
                                <option value="TW">Taiwan</option>
                                <option value="TH">Thailand</option>
                                <option value="TN">Tunisia</option>
                                <option value="TR">Turkey</option>
                                <option value="UA">Ukraine</option>
                                <option value="AE">United Arab Emirates</option>
                                <option value="GB">United Kingdom</option>
                                <option value="US">USA</option>
                                <option value="UZ">Uzbekistan</option>
                                <option value="VN">Vietnam</option>
                            </select>
                        </div>
                        <h3 style="margin-left: 0; padding-top: 40px; margin-right: 0">Card Information</h3>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="card-number">Card Number</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="card-number" id="card-number" placeholder="Debit/Credit Card Number">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="expiry-month">Expiration Date</label>
                            <div class="col-sm-9">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <select class="form-control col-sm-2" name="expiry-month" id="expiry-month">

                                            <option value="01">Jan (01)</option>
                                            <option value="02">Feb (02)</option>
                                            <option value="03">Mar (03)</option>
                                            <option value="04">Apr (04)</option>
                                            <option value="05">May (05)</option>
                                            <option value="06">June (06)</option>
                                            <option value="07">July (07)</option>
                                            <option value="08">Aug (08)</option>
                                            <option value="09">Sep (09)</option>
                                            <option value="10">Oct (10)</option>
                                            <option value="11">Nov (11)</option>
                                            <option value="12">Dec (12)</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-3">
                                        <select class="form-control" name="expiry-year">

                                            <option value="2016">2016</option>
                                            <option value="2017">2017</option>
                                            <option value="2018">2018</option>
                                            <option value="2019">2019</option>
                                            <option value="2020">2020</option>
                                            <option value="2021">2021</option>
                                            <option value="2022">2022</option>
                                            <option value="2023">2023</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label" for="cvv">Card CVV</label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="cvv" id="cvv" placeholder="Security Code">
                            </div>

                        </div>

                    <div class="form-group">

                        <label class="col-sm-3 control-label" for="cvv">Card Type</label>
                        <div class="col-sm-3">
                            <select class="form-control col-sm-2" name="cardtype" id="cardtype">
                                <option value="visa">Visa</option>
                                <option value="mastercard">Master Card</option>
                                <option value="discover">Maestro</option>
                                <option value="amex">American Express</option>
                            </select>
                        </div>
                    </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-9">
                                {{ Form::submit('Pay Now')}}
                            </div>
                        </div>
                    {{ Form::hidden('courseslug',$courseslug)}}
                        {{ Form::hidden('course_id',$course_id)}}
                    {{ Form::hidden('invitetoken',$invitetoken)}}

                    {{ Form::close() }}
                </div>

            </div>
        </div>
    </div>
</div>