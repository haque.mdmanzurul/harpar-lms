<a href="#sidebar-menu" class="nav-toggle mobile-menu">

    <span class="icon-bar"></span>
    <span class="icon-bar"></span>
    <span class="icon-bar"></span>

</a>
<div class="sidebar" id="sidebar-menu">
    <ul class="main-nav user-sidebar">
        <li class=""> <a href="{{$siteurl}}">Dashboard <i class="fa fa-backward pull-right"></i></a>  </li>
        @if ( count($user_modules)>0)
            @define $upload_disabled = ''
            @define $i = 1
            @foreach ($user_modules as $um)
                @if($um->is_certificate == 1)
                    @define continue
                @endif

                @if($course->type == 1 and $i>1)
                    @define continue
                @endif

                @define $active = (isset($module) AND $um->id==$module->id)?"active":""

                <li class="{{ $active }}">
                    <a href="{{ $siteurl }}/course/{{ $um->slug }}/{{ base64_encode($um->id) }} ">
                        @if($um->module_status == 2)
                            <i class="fa fa-circle feedback"></i>
                        @elseif ($um->module_status == 3 )
                            <i class="fa fa-circle completed"></i>
                        @elseif ($um->module_status == 1 )
                            <i class="fa fa-circle referred"></i>
                        @elseif ($um->module_status == 0 )
                            <i class="fa fa-lock submitted"></i>

                        @else
                            <i class="fa fa-circle not-started"></i>
                        @endif
                        @if ($i==1)
                            Overview

                        @else
                            Module {{ $i-1 }}

                            @if($um->new_feedback_count != 0 && $um->module_status != 3)
                                <sup class="feedback-count" title="{{ $um->new_feedback_count }} new feedback">
                                    <i class="fa fa-comment comment-count"></i> {{ $um->new_feedback_count }}</sup>
                            @endif
                        @endif

                        <i class="fa fa-play rhs"></i>
                        @if(!$um->is_overview)
                            <span>
                @if($um->module_status == 2)
                                    Feedback
                                @elseif ($um->module_status == 3 )
                                    Passed
                                @elseif ($um->module_status == 1 )
                                    Referred
                                @elseif ($um->module_status == 0 )
                                    Submitted
                                @else
                                    Not Started
                                @endif
            </span>
                        @endif
                    </a>
                </li>
                @define $i++
                @endforeach


                        <!-- User Certification Module -->
                @foreach ($user_modules as $um)
                    @if($um->is_certificate == 1 OR  $um->is_certificate == 0)
                        @define continue
                    @endif

                        @if($course->type == 1 )
                            @define continue
                        @endif

                    {{--@if($user_current_course->certificate_payment == 0)
                        @define continue
                    @endif--}}

                    @define $active = (isset($module) AND $um->id==$module->id)?"active":""

                    <li class="{{ $active }}">
                        <a href="{{ $siteurl }}/course/{{ $um->slug }}/{{ base64_encode($um->id) }} ">
                            @if($um->module_status == 2)
                                <i class="fa fa-circle feedback"></i>
                            @elseif ($um->module_status == 3 )
                                <i class="fa fa-circle completed"></i>
                            @elseif ($um->module_status == 1 )
                                <i class="fa fa-circle referred"></i>
                            @elseif ($um->module_status == 0 )
                                <i class="fa fa-lock submitted"></i>

                            @else
                                <i class="fa fa-circle not-started"></i>
                            @endif
                            @if($um->is_certificate == 1)

                               @if($um->title !='') {{$um->title}} @else
                                        Certification @endif
                                    @if($um->new_feedback_count != 0 && $um->module_status != 3)  <sup class="feedback-count" title="{{ $um->new_feedback_count }} new feedback"> <i class="fa fa-comment comment-count"></i> {{ $um->new_feedback_count }}</sup>@endif
                            @endif

                            <i class="fa fa-play rhs"></i>
                            @if(!$um->is_overview)
                                <span>
                                    @if($um->module_status == 2)
                                        Feedback
                                    @elseif ($um->module_status == 3 )
                                        Passed
                                    @elseif ($um->module_status == 1 )
                                        Referred
                                    @elseif ($um->module_status == 0 )
                                        Submitted
                                    @else
                                        Not Started
                                    @endif
                                </span>
                            @endif
                        </a>
                    </li>
                    @define $i++
                @endforeach

                @endif
    </ul>

    <a href="#inline1" moduleID="{{ $module->id }}" courseID="{{ $course->id }}" userID="{{ $user->id }}" class="user_feedback fancybox">Help</a>
</div>