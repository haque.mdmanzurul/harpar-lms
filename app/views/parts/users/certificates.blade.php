
<div class="row courses">

	<div class="col-md-7">  	
        <h3>Certificates <span>(Displaying {{ $certificates_current_start }} - {{ $certificates_current_total }} of {{ $certificates_total }} certificates)</span></h3>
    </div>
    <div class="col-md-3">
    	{{ $certificates_links->links() }}
    </div>
    
    <div class="col-md-2">
    	
    </div>
    @if(Session::get('flash_notice'))
    <div class="col-md-12">
    	<p class="bg-primary">{{ Session::get('flash_notice') }}</p>
    </div>
    @endif
</div>
<div class="row courses">
    @if (count($certificates) > 0)
        @foreach($certificates as $cf)
	<div class="col-md-3">
      <div class="pending-review-container">
        <h3>{{$cf->title}} </h3>
            <br/><span class="download" style="width: 100%">
              @if($cf->type!=0)
                  <a userid="{{$cf->userID}}"
                     courseid="{{$cf->courseID}}"
                     course_start_date="{{ $cf->start_date}}"
                     course_end_date="{{ $cf->end_date}}"
                     style="width: 100%"
                     href="{{ $siteurl }}/downloadCertificate/{{ isset($cf->courseID)?base64_encode($cf->courseID):'' }}" class="pinkbutton">Download Certificate Now<i class="fa fa-angle-right"></i>
                  </a>
               @else
              <a userid="{{$cf->userID}}"
                 courseid="{{$cf->courseID}}"
                 course_start_date="{{ $cf->start_date}}"
                 course_end_date="{{ $cf->end_date}}"
                 style="width: 100%"
                 href="{{ $siteurl }}/downloadCertificate/{{ isset($cf->userID)?base64_encode($cf->userID):'' }}/{{ isset($cf->courseID)?base64_encode($cf->courseID):'' }}" class="pinkbutton"
                 onclick="return generate_certificate('{{$cf->title}}','{{$certificate_name}}','{{ $cf->start_date}}','{{ $cf->end_date}}')">Download Certificate Now<i class="fa fa-angle-right"></i>
                </a>
                  @endif
          </span>

        </div>
    </div>
   
        @endforeach
    @else
    <div class="col-md-3">
      <div class="pending-review-container">
    <p style="color: #fff">You do not have any certificates</p>
          </div>
    </div>
    @endif
</div>
<div id="fromHTMLtestdiv" style="display: none; position: relative">
    <div id="bypassme"></div>
    <div class="main-pdf-area">

        {{--<div class="back-image">
            <img src="{{ $siteurl }}/images/1610_certificate_empty.jpg"/>
        </div>--}}
        <span class="name">Md Manzurul Haque</span>

    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.3.2/jspdf.debug.js"></script>
<script type="text/javascript">
    function generate_certificate( coursename,name,startdate, enddate){/*
//        var doc = new jsPDF();
//
//        doc.text('Hello world!', 10, 10);
//        doc.save('a4.pdf');

        var pdf = new jsPDF('l', 'px', [3508, 2410])

// source can be HTML-formatted string, or a reference
// to an actual DOM element from which the text will be scraped.
                , source = $('#fromHTMLtestdiv')[0]

// we support special element handlers. Register them with jQuery-style
// ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
// There is no support for any other type of selectors
// (class, of compound) at this time.
                , specialElementHandlers = {
            // element with id of "bypass" - jQuery style selector
            '#bypassme': function(element, renderer){
                // true = "handled elsewhere, bypass text extraction"
                return true
            }
        }

        margins = {
            top: 0,
            bottom: 0,
            left: 0,
            width: 3508
        };
        // all coords and widths are in jsPDF instance's declared units
        // 'inches' in this case
        pdf.fromHTML(
                source // HTML string or DOM elem ref.
                , margins.left // x coord
                , margins.top // y coord
                , {
                    'width': margins.width // max width of content on PDF
                    , 'elementHandlers': specialElementHandlers
                },
                function (dispose) {
                    // dispose: object with X, Y of the last line add to the PDF
                    //          this allow the insertion of new lines after html
                    pdf.save('Manzurul.pdf');
                },
                margins
        )*/

        var getImageFromUrl = function(url, callback) {
            var img = new Image();

            img.onError = function() {
                alert('Cannot load image: "'+url+'"');
            };
            img.onload = function() {
                callback(img);
            };
            img.src = url;
        }

// Since images are loaded asyncronously, we must wait to create
// the pdf until we actually have the image.
// If we already had the jpeg image binary data loaded into
// a string, we create the pdf without delay.
        var createPDF = function(imgData) {
            var doc = new jsPDF('l', 'px', [3508, 2410]);

            // This is a modified addImage example which requires jsPDF 1.0+
            // You can check the former one at <em>examples/js/basic.js</em>

            //doc.addImage(imgData, 'JPEG', 10, 10, 50, 50, 'monkey'); // Cache the image using the alias 'monkey'
            //doc.addImage('monkey', 70, 10, 100, 120); // use the cached 'monkey' image, JPEG is optional regardless
            // As you can guess, using the cached image reduces the generated PDF size by 50%!

            // Rotate Image - new feature as of 2014-09-20
            doc.addImage({
                imageData : imgData,
                angle     : 0,
                x         : 0,
                y         : 0,
                w         : 3508,
                h         : 2401
            });


            doc.setFont("courier");
            doc.setFontSize(160);
            doc.setTextColor(198,22,141);
            doc.text(1270, 1050, name);

            doc.setFont("courier");
            doc.setFontSize(100);
            doc.setTextColor(198,22,141);
            doc.text(1015, 1370, coursename);


            doc.setFont("courier");
            doc.setFontSize(80);
            doc.setTextColor(0,0,0);
            doc.text(1200, 1680, startdate);

            doc.setFont("courier");
            doc.setFontSize(80);
            doc.setTextColor(0,0,0);
            doc.text(1900, 1680, enddate);

            doc.setFont("courier");
            doc.setFontSize(80);
            doc.setTextColor(0,0,0);
            doc.text(1655, 1945, 'Harpar');



            // Output as Data URI
            doc.output('datauri');
        }
        getImageFromUrl('{{ $siteurl }}/images/1610_certificate_empty.jpg', createPDF);
        return false;
    }
</script>