        <div class="row">
            <div class="col-md-12 content-wrapper">
            
                <div class="register">
                <h3>Change Password</h3>
                @if (Session::has('flash_notice'))
                	<span class="error">
                    {{ Session::get('flash_notice') }}
                    </span>
                @endif             
        
                 @if (Session::has('success_message'))
                	<p class="bg-primary">
                    {{ Session::get('success_message') }}
                    </p>
                @endif           
                {{ Form::open(array('url' => 'changepassword/'.$userid,'method'=>'post')) }}
                 <p>
                     {{ Form::label('Current Password')}}
                     {{ Form::password('currentpassword')}}
                     <span class="error">
                        @if ($errors->has('currentpassword'))
                        {{ $errors->first('currentpassword') }}
                        @endif
                     </span>
                 </p>
             
                         
                 <p>
                     {{ Form::label('New Password')}}
                     {{ Form::password('newpassword')}}
                     <span class="error">
                        @if ($errors->has('newpassword'))
                        {{ $errors->first('newpassword') }}
                        @endif
                     </span>
                 </p>                             
                          
                 <p>
                     {{ Form::label('Confirm New Password')}}
                     {{ Form::password('newpassword_confirmation')}}
                     <span class="error">
                        @if ($errors->has('newpassword_confirmation'))
                        {{ $errors->first('newpassword_confirmation') }}
                        @endif
                     </span>
                 </p>
                                   
                  <p>
                 {{ Form::submit('Update')}}
                 </p>    
                 
                 {{ Form::close()}}                 
                </div>
            </div>
        </div>