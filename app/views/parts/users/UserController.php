<?php

class UserController extends \BaseController {

    private $_ClientId='AdyAdrnKemCHznHH6BQwZZ9i30RAtAdlmo8V_LoZiofWYDcl-WTFuJ6WyvSxkekofSibfvj9hb2mljOS';
    private $_ClientSecret='EJyyoKjvb0JYe-9BkBNbZ3V6hjDI_aXCnZ_c1fyQ56OW1JOEslDR0aRVQFo2ni8wijC39geBdO4Xul2k';
	/**
	 * Constructor
	 *
	 * @return Response
	 */
	public function __construct()
	{

		$client_id = DB::table('settings')->where('name','paypal_client_id')->first();
		if(count($client_id)>0)
			$this->_ClientId = $client_id->content;

		$client_secret = DB::table('settings')->where('name','paypal_client_secret')->first();
		if(count($client_secret)>0)
			$this->_ClientSecret = $client_secret->content;

		$mode = DB::table('settings')->where('name','paypal_mode')->first();
		$paypal_mode = 'sandbox';
		if(count($mode)>0)
			$paypal_mode = $client_secret->content;

		//Set paypal configuration
        $this->_apiContext = Paypalpayment::apiContext($this->_ClientId, $this->_ClientSecret);

        // Uncomment this step if you want to use per request
        // dynamic configuration instead of using sdk_config.ini

        $this->_apiContext->setConfig(array(
            'mode' => $paypal_mode,
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path().'/logs/paypal.log',
            'log.LogLevel' => 'FINE'
        ));


	}


	/**
	 * Display all the users.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show()
	{
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'users';
		$data['template_part'] = 'userlist';
		$limit = 20;
		$page = (Input::get('page')!='')?Input::get('page'):1;	 
		$users = User::paginate($limit);
		$data['users_total'] = DB::table('users')->count(); 
		$data['users_current_total'] = (($page*$limit)>$data['users_total'])?$data['users_total']:($page)*$limit;

		$data['users_current_start'] = (($page-1)*$limit)+1;
		
		$data['users_pagination'] = $users;
		$data['users'] = array();
		if(count($users)>0){
			foreach($users as $user){
				$user_courses = DB::select( DB::raw("SELECT courses.*,user_courses.exp_date as exp_date FROM courses inner join user_courses on  user_courses.courseID=courses.id AND user_courses.userID=".$user->id." order by courses.created_at DESC"));
				
				if(count($user_courses)>0){
					foreach($user_courses as $us){
						$us->submission_count = $user->getUserSubmisions($user->id,$us->id);
					}
				}
				
				$user->courses = $user_courses;
				array_push($data['users'],$user);
			}
		}
		
			
		return View::make('users',$data);
	}

	/**
	 * Create New User View
	 *
	 * @return Response
	 */	
	public function createNewUser()
	{
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'users';
		$data['template_part'] = 'createnewuser'; 
		$data['courses'] = DB::table('courses')->orderBy('title','ASC')->get();
	
		return View::make('users',$data);		
	}


	/**
	 * Create new user action 
	 *
	 * @return Response
	 */	
	public function postCreateUser()	
	{
		$rules = array(
					'firstname' => 'required|min:3',
					'lastname' => 'required|min:2',
					'email' => 'required|email|min:5',
					'username' => 'required|unique:users,username|min:5',
					'password' => 'required|min:6|confirmed',
					'password_confirmation' => 'required|min:6'
				);
	    $validator = Validator::make( 
				Input::all(),$rules
				
			);
	
	    if($validator->fails()){
				return Redirect::to('newuser')->withErrors($validator)->withInput();
		}
		
		
		$user =  new User();
		$user->firstname = Input::get('firstname');	
		$user->lastname = Input::get('lastname');	
		$user->email = Input::get('email');	
		$user->username = Input::get('username');	
		$user->password = Hash::make(Input::get('password'));	
		$user->address1 = Input::get('address1');	
		$user->address2 = Input::get('address2');	
		$user->town = Input::get('town');	
		$user->district = Input::get('district');	
		$user->postcode = Input::get('postcode');
		$user->user_image = '';

		if (Input::hasFile('user_image')){
			$destinationPath = "uploads";
			$file_extension = Input::file('user_image')->getClientOriginalExtension();
			$fileName = time().$user->username.'.'.$file_extension;
			Input::file('user_image')->move($destinationPath, $fileName);
			$user->user_image = $fileName;
		}

		$user->user_type= (Input::get('user_type')=='Admin')?0:1;
		$user->save();

		$user_id = DB::getPdo()->lastInsertId();	
		// save user courses
		$user_courses = (Input::has('user_courses'))?Input::get('user_courses'):array();
		
		if(Auth::user()->user_type==0 AND count($user_courses)>0){
			if(count($user_courses)>0){
				foreach($user_courses as $ucid){
					// user course assingment
					$uca = DB::table('user_courses')->insert(array('userID'=>$user_id,'courseID'=>$ucid,'enroll_date'=>date('Y-m-d'),'exp_date'=>Input::get('enroll_c'.$ucid)));
				}
			}
		}
		

		// Log the activity	
		$log =  new ActivityLog();
		$log->title = 'New user creation';
		$log->activity = 'User Create';
		$log->url = Request::url();
		$log->userID = Auth::user()->id;
		$log->save();	
		
		//Send user creation email
		$maildata['firstname'] = $user->firstname;
		$maildata['lastname'] = $user->lastname;
		$maildata['username'] = $user->username;
		$maildata['password'] = Input::get('password');		
		$setting = Setting::where('name','=','usercreation_email')->first();
		$maildata['email_content'] = $setting->renderContent($setting->content, $maildata);	
	
		Mail::send('emails.userCreateEmail', $maildata, function($message) {
			$message->to( Input::get('email'), Input::get('firstname').Input::get('lastname'))->subject('Your Harper Learner Portal is now ready');
		});
		return Redirect::to('users')->with('flash_notice','User successfully added.');
	}

	/**
	 * Post user data for update purpose
	 * @param $id
	 * @return mixed
	 */
	public function editUser($id)
	{
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'users';
		$data['template_part'] = 'edituser'; 
		$data['user'] = User::find($id); 
		//Find user modules
		$data['user_modules'] = $data['user']->getUserCourseModules(NULL);	
		
		$user_courses = DB::select( DB::raw("SELECT courses.*,user_courses.exp_date as exp_date FROM courses inner join user_courses on  user_courses.courseID=courses.id AND user_courses.userID=".$data['user']->id." order by courses.created_at DESC"));
		
		//find the first module of each course and add those to course objects
		$data['user_courses'] = array();		
		$data['user_enrolled_courses'] = array();
		$data['courses_enroll_date'] = array();	
		if(count($user_courses)>0){
			$i=0;
			foreach( $user_courses as $course){							 
				 $module = DB::table('modules')->where('courseID',$course->id)->first();				 
				 $course->moduleID = $module->id;
				 array_push($data['user_courses'],$course);
				 array_push($data['user_enrolled_courses'],$course->id);
				 $data['courses_enroll_date'][$course->id] = array('exp_date'=>$course->exp_date);
				 $i++;
			}
		}
		
		$data['user_current_course'] = DB::table('user_courses')
										->where('userID',$data['user']->id)
										->join('courses','courses.id','=','user_courses.courseID')
										->join('course_modules','course_modules.courseID','=','courses.id')
										->groupBy('courses.id')
										->first();
		$data['courses'] = DB::table('courses')->get();
		foreach($data['courses'] as &$c){
			if(isset($data['courses_enroll_date'][$c->id]) && is_array($data['courses_enroll_date'][$c->id])){
				$c->exp_date = $data['courses_enroll_date'][$c->id]['exp_date'];
			}
		}
		
		
		$data['certificates'] = DB::table('user_certificates')->where('userID',$id)->get();
			
		return View::make('users',$data);		
	}
	
	public function postEditUser($id)	
	{
		$rule = array(
					'firstname' => 'required|min:3',
					'lastname' => 'required|min:2',
					'email' => 'required|email|min:5'
				);

		if(Input::has('password') && Input::has('password_confirmation')){
			$rule['password'] = 'required|min:6|confirmed';
			$rule['password_confirmation'] = 'required|min:6';
		}
		$validator = Validator::make( Input::all(),$rule);
	
	    if($validator->fails()){
				return Redirect::to('edituser/'.$id)->withErrors($validator)->withInput();
		}
		
		
		$user =  User::find($id);
		$user->firstname = (Input::has('firstname'))?Input::get('firstname'):$user->firstname;	
		$user->lastname = (Input::has('lastname'))?Input::get('lastname'):$user->lastname;	
		$user->email = (Input::has('email'))?Input::get('email'):$user->email;
		
		if(Auth::user()->user_type==0){
		$user->username = (Input::has('username'))?Input::get('username'):$user->username;
			if(Input::has('password'))	
			$user->password = Hash::make(Input::get('password'));
		}
				
		$user->address1 = (Input::has('address1'))?Input::get('address1'):$user->address1;
		$user->address2 = (Input::has('address2'))?Input::get('address2'):$user->address2;	
		$user->town = (Input::has('town'))?Input::get('town'):$user->town;
		$user->district = (Input::has('district'))?Input::get('district'):$user->district;	
		$user->postcode = (Input::has('postcode'))?Input::get('postcode'):$user->postcode;		
				
		if (Input::hasFile('user_image')){
			$destinationPath = "uploads";
			$file_extension = Input::file('user_image')->getClientOriginalExtension();
			$fileName = time().$user->username.'.'.$file_extension;	
			Input::file('user_image')->move($destinationPath, $fileName);
			$user->user_image = $fileName;
		}
		
		if (Input::has('user_type'))
		$user->user_type= (Input::get('user_type')=='Admin')?0:1;	
		$user->save();
		
		// save user courses
		$user_courses = (Input::has('user_courses'))?Input::get('user_courses'):array();
		$user_courses_old = (Input::has('user_courses_old'))?explode(',',Input::get('user_courses_old')):array();
		
		if(Auth::user()->user_type==0 AND count($user_courses_old)>=0){
			//$delete = DB::table('user_courses')->where('userID',$id)->whereIn('courseID',$user_courses_old)->delete();
			if(count($user_courses)>0){
				foreach($user_courses as $ucid){
					// user course assingment 
					$uca = DB::table('user_courses')->insert(array('userID'=>$id,'courseID'=>$ucid,'enroll_date'=>date('Y-m-d'),'exp_date'=>Input::get('enroll_c'.$ucid)));
				}
			}
		}
		

		// Log the activity	
		$log =  new ActivityLog();
		$log->title = 'User Information Updated';
		$log->activity = 'User Update';
		$log->url = Request::url();
		$log->userID = Auth::user()->id;
		$log->save();	
		
		//Upload and save the certificates for user

		//Sends the email to user
		/*Mail::send('emails.userCreateEmail', Input::all(), function($message) {
			$message->to( Input::get('email'), Input::get('firstname').Input::get('lastname'))->subject('Dear user, Welcome to Harpar LMS');
		});*/
		
		//
		if(Auth::user()->user_type==0)
			return Redirect::to('users')->with('flash_notice','User information successfully updated.');
		else
			return Redirect::to('users/'.Auth::user()->username.'/userinfo');
	}	
	
	
	/**
	 * Display single user information to admin.
	 *
	 * @param  int  $username
	 * @return Response
	 */
	public function displayUserInfoToAdmin($username)
	{
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'users';
		$data['template_part'] = 'viewuser';
		$data['module_sidebar'] = 'yes';
		$data['user_admin_view'] = true;	
	
		
		$data['user'] = DB::table('users')->where('username',$username)->first();
		$user = User::find($data['user']->id);

        if($user->user_type == 0){
            return Redirect::to('users/'.Auth::user()->username.'/userinfo');
        }
		
		$data['user_current_course'] = DB::table('user_courses')->where('userID',$data['user']->id)->join('courses','courses.id','=','user_courses.courseID')->first();
		//Find user modules
		$data['user_modules'] = isset($data['user_current_course'])?$user->getUserCourseModules(NULL):NULL;		
		//find the user submission		
		$data['user_submissions'] = array();
		//get the user module status
		if(count($data['user_modules'])>0){
			foreach($data['user_modules'] as $usb){
				if($usb->is_overview ==1)
				continue;
				
				$user_submissions = DB::table('user_submissions')
											->where('user_submissions.userID','=',$data['user']->id)
											->where('user_submissions.moduleID','=',$usb->id) 
											->join('modules', 'modules.id', '=', 'user_submissions.moduleID')
											->join('courses', 'courses.id', '=', 'modules.courseID')
											->select('user_submissions.*')
											->orderBy('user_submissions.created_at','DESC')
											->first();
				if(count($user_submissions)>0){
					$usb->user_submission = $user_submissions;					
				}
				else{
					$usb->user_submissions = NULL;
					continue;
				}											
				array_push($data['user_submissions'],$usb);
			}			
		}
		
		function build_sorter($key) {
			return function ($a, $b) use ($key) {
				return strnatcmp($a->$key, $b->$key);
			};
		}
		
		usort($data['user_submissions'], build_sorter('module_status'));
		
		$data['user_courses'] = DB::select( DB::raw("SELECT courses.* FROM courses inner join user_courses on  user_courses.courseID=courses.id AND user_courses.userID=".$data['user']->id." order by courses.created_at DESC"));
		
		
		
		return View::make('users',$data);
	}	


	/**
	 * Display user submission on specific course to admin.
	 *
	 * @param  string  $username
	 * @param  string  $course_slug	 	 
	 * @return Response with view
	 */
	public function displayUserInfoToAdminWithCourseSubmission($username,$course_slug)
	{
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'users';
		$data['template_part'] = 'viewuser';
		$data['module_sidebar'] = 'yes';	
		$data['user_admin_view'] = false;	
		
		$data['user'] = DB::table('users')->where('username',$username)->first();
		$user = User::find($data['user']->id);
		
		$data['user_current_course'] = DB::table('courses')->where('course_slug',$course_slug)->first();
		//Find user modules
		$data['user_modules'] = isset($data['user_current_course'])?$user->getUserCourseModules($data['user_current_course']->id):NULL;		
		//find the user submission		
		$data['user_submissions'] = array();
		//get the user module status
		if(count($data['user_modules'])>0){
			foreach($data['user_modules'] as $usb){
				if($usb->is_overview ==1)
				continue;
				
				$user_submissions = DB::table('user_submissions')
											->where('user_submissions.userID','=',$data['user']->id)
											->where('user_submissions.moduleID','=',$usb->id)
											->where('courses.id','=',$data['user_current_course']->id)
											->join('modules', 'modules.id', '=', 'user_submissions.moduleID')
											->join('courses', 'courses.id', '=', 'modules.courseID')
											->select('user_submissions.*')
											->orderBy('user_submissions.created_at','DESC')
											->first();
				if(count($user_submissions)>0){
					$usb->user_submission = $user_submissions;					
				}
				else{
					$usb->user_submissions = NULL;
					continue;
				}											
				array_push($data['user_submissions'],$usb);
			}			
		}
		
		function build_sorter($key) {
			return function ($a, $b) use ($key) {
				return strnatcmp($a->$key, $b->$key);
			};
		}
		
		usort($data['user_submissions'], build_sorter('module_status'));	
				
		$data['user_courses'] = DB::select( DB::raw("SELECT courses.* FROM courses inner join user_courses on  user_courses.courseID=courses.id AND user_courses.userID=".$data['user']->id." order by courses.created_at DESC"));
		
		
		
		
		return View::make('users',$data);
	}	



	/**
	 * Show the login form 
	 *
	 * @return Response
	 */
	public function getLogin()
	{
		if(Auth::check()){
			return Redirect::to('users/'.Auth::user()->username);	
		}
		$data = array();
		$data['siteurl'] = Config::get('app.url');	
		return View::make('login',$data);
	}
	
	/**
	 * Login functionality after posted login form
	 *
	 * @return Response
	 */
	public function postLogin()
	{
		$rules = array('username' => 'required','password' => 'required');
		
		$validation = Validator::make(Input::all(), $rules);
		
		if($validation->fails()){
			$messages = $validation->messages();
			return Redirect::to('login')->withErrors($validation);	
		}
		$creds = array(
			'username' => Input::get('username'),
			'password' => Input::get('password')
		);
			
		if(Auth::attempt($creds)){
			$log =  new ActivityLog();
			$log->title = 'Account Login';
			$log->activity = 'Login';
			$log->url = Request::url();
			$log->userID = Auth::user()->id;
			$log->save();
			
			if(Auth::user()->last_visited_url!=""){
				return Redirect::to(Auth::user()->last_visited_url);	
			}else{
				return Redirect::intended('users/'.Auth::user()->username.'/userinfo');	
			}

		}
		else{
			
			$log =  new ActivityLog();
			$log->title = 'Failed login attempt';
			$log->activity = 'Login Failed';
			$log->url = Request::url();
			$log->userID = '';
			$log->save();			
	
			return Redirect::to('login')->withInput()->withFlashNotice('Invalid username or password');	
		}
	}

	
	/**
	 * Logout action 
	 *
	 * @return Response
	 */
	public function getLogout()
	{	
		if(!Auth::check()){	
			return Redirect::to('login');
		}else{
			$log =  new ActivityLog();
			$log->title = 'Account Logout';
			$log->activity = 'Logout';
			$log->url = Request::url();
			$log->userID = Auth::user()->id;
			$log->save();
			
			Auth::logout();	
			return Redirect::to('login');
		}
	}	
	
	/**
	 * get user info 
	 *
	 * @return Response
	 */	
	public function getUserInfo($username)
	{
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'users';
		$data['template_part'] = 'userinfo';	
		
		$data['user'] = DB::table('users')->where('username',$username)->first();
		$user = User::find($data['user']->id);


		//Find user modules
		$data['user_modules'] = $user->getUserCourseModules(NULL);	
		
		
		//Get users activity
		$limit = 20;
		$page = (Input::get('page')!='')?Input::get('page'):1;
		$data['history'] = DB::table('logs')->where('userID',$data['user']->id)->orderby('created_at','desc')->paginate(20);
		$data['history_total'] = DB::table('logs')->where('userID',$data['user']->id)->count();
		$data['history_current_total'] = (($page*$limit)>$data['history_total'])?$data['history_total']:($page)*$limit;
		$data['history_current_start'] = (($page-1)*$limit)+1;
		
				
		$user_courses = DB::select( DB::raw("SELECT courses.* FROM courses inner join user_courses on  user_courses.courseID=courses.id AND user_courses.userID=".$data['user']->id." order by courses.created_at DESC"));
		
		//find the first module of each course and add those to course objects
		$data['user_courses'] = array();		
		if(count($user_courses)>0){
			$i=0;
			foreach( $user_courses as $course){							 
				 $module = DB::table('modules')->where('courseID',$course->id)->first();				 
				 $course->moduleID = $module->id;
				 array_push($data['user_courses'],$course);
				 $i++;
			}
		}
		
		$data['user_current_course'] = DB::table('user_courses')
										->where('userID',$data['user']->id)
										->join('courses','courses.id','=','user_courses.courseID')
										->join('course_modules','course_modules.courseID','=','courses.id')
										->groupBy('courses.id')
										->first();								
		return View::make('users',$data);		
	}

	
	/**
	 * Upload user submissions.
	 *
	 * 
	 * @return Response(JSON)
	 */
	public function addusersubmission()
	{

		//$fileName = $_FILES["file1"]["name"];
		$moduleid = Input::get('moduleID');
		$courseid = Input::get('courseID');
		$userid = Auth::user()->id;
		$fileName = '';
		$file_extension = '';
		
		$destinationPath = "uploads/user-submission/".$courseid.$moduleid.$userid;
		if(!is_dir($destinationPath))
			mkdir($destinationPath,0777);
					
		if(Input::hasFile('file1')){
			$fileName = $_FILES["file1"]["name"];
			$file_extension = Input::file('file1')->getClientOriginalExtension();
			Input::file('file1')->move($destinationPath, $fileName);
		}
		
		//Add submission info into database

		$response = array(
									'msg'=>'Successfully uploaded',
									'filename'=>$fileName
									);
		if(Input::hasFile('file1')){
			$response['filetype'] = $file_extension;	
									
		}
		
		//Insert user submission
		DB::table('user_submissions')->insert(array(
			'attachment_name' => $fileName, 
			'attachment_path' => $destinationPath, 
			'userID' => $userid, 
			'moduleID' => $moduleid,
			'created_at' => date('Y-m-d h:i:s'),
			'updated_at' => date('Y-m-d h:i:s')  
		));
		
		//Insert Module status
		
		$exist = DB::table('user_module_status')
						->where('userID',$userid)
						->where('moduleID',$moduleid)
						->where('courseID',$courseid)
						->first();
						
		if(count($exist)>0)	{
			DB::table('user_module_status')->where('id',$exist->id)
						->update(array('user_status'=>0,'updated_at'=>date('Y-m-d h:i:s')));	
		}else{
			DB::table('user_module_status')->insert(array(
				'userID' => $userid, 
				'moduleID' => $moduleid, 
				'courseID' => $courseid, 
				'user_status' => 0,
				'created_at' => date('Y-m-d h:i:s'),
				'updated_at' => date('Y-m-d h:i:s')  
			));	
		}
		$response['submission_id'] = DB::getPdo()->lastInsertId();
		$response['created_at'] = date('d/m/Y');
		
		
		//Log activity
		$log =  new ActivityLog();
		$log->title = 'Document submission';
		$log->activity = 'User Submission';
		$log->url = Request::url();
		$log->userID = Auth::user()->id;
		$log->save();
		
		//Send email to the admin
		$user = User::find($userid);
		$course = Course::find($courseid);
		$module = Module::find($moduleid);
		$maildata = array();
		$maildata['firstname'] = $user->firstname;
		$maildata['lastname'] = $user->lastname;
		$maildata['email'] = $user->firstname;
		$maildata['coursetitle'] = $course->title;
		$maildata['moduletitle'] = $module->title;
		
		$setting = Setting::where('name','=','admin_email')->first();
		$maildata['to_email'] = $setting->content;
		
		$message_template = Setting::where('name','=','usersubmission_email')->first();
		$maildata['email_content'] = $message_template->renderContent($message_template->content, $maildata);		
		Mail::send('emails.userSubmissionEmail', $maildata, function($message) use($maildata) {
			$message->to( $maildata['to_email'], 'Harpar Admin')->subject('You have a new document pending submission');
		});		
		
		return Response::json($response);		
	}


	/**
	 * @param $username
	 * @param $moduleID
	 * @return mixed
	 *
	 */
	public function submissionhistory($username,$moduleID){

		$data['siteurl']        = Config::get('app.url');
		$data['selected']       = 'users';
		$data['template_part']  = 'submissionhistory'; 
		$data['module_sidebar'] = 'yes';		
		$user                   = User::where('username',$username)->first();
		$data['module']         = DB::table('modules')->where('id',$moduleID)->first();
		
		$data['user_modules']   = DB::select( DB::raw("SELECT modules.*,courses.id as cid,courses.course_slug as slug FROM modules inner join user_courses on user_courses.courseID=modules.courseID and user_courses.userID = ".$user->id.' inner join courses on user_courses.courseID=courses.id'));
		
		$data['user_submissions'] = DB::select( DB::raw("SELECT *,user_submissions.id as submission_id,user_submissions.created_at as submission_time FROM user_submissions, modules WHERE user_submissions.userID = ".$user->id." and user_submissions.moduleID=modules.id and modules.id=".$moduleID.'  order by user_submissions.created_at DESC'));
		
		$data['user_feedbacks'] = $user->getFeedbackOnUser($user->id,$moduleID);

		return View::make('users',$data);
				
	}


	/**
	 * @return mixed
	 */
	public function postAdminFeedback(){

		$response = array();
		$response['userid'] = Input::get('user_id');
		$response['moduleid'] = Input::get('module_id');
		$response['courseid'] = Input::get('course_id');;
		$response['message'] = 'Feedback addedd Sucessfully';

        $msg = Input::get('message');

		
		$addFeedback = DB::table('user_module_feedback')
						->insert(
						array(
							'parent'=>0,
							'userID'=>$response['userid'],
							'moduleID'=>$response['moduleid'],
							'courseID'=>$response['courseid'],
							'message' => $msg,
							'reviewer_id' => Auth::user()->id,
							'created_at'  => date('Y-m-d h:i:s'),
							'updated_at'  => date('Y-m-d h:i:s'),
                            'attachment_ids'  => Input::get('attachment_id')
						));
		
		//Log Activity
		$log =  new ActivityLog();
		$log->title = 'Feedback for learner added';
		$log->activity = 'Admin feedback';
		$log->url = Request::url();
		$log->userID = Auth::user()->id;
		$log->save();
		
		//Send email to the user
		$user = User::find($response['userid']);
		$course = Course::find($response['courseid']);
		$module = Module::find($response['moduleid']);
		$maildata = array();
		$maildata['firstname'] = $user->firstname;
		$maildata['lastname'] = $user->lastname;
		$maildata['email'] = $user->firstname;
		$maildata['coursetitle'] = $course->title;
		$maildata['moduletitle'] = $module->title;		
		if(Auth::user()->user_type == 1){
			$setting = Setting::where('name','=','admin_email')->first();
			$maildata['to_email'] = $setting->content;
			
			$message_template = Setting::where('name','=','userfeedback_email')->first();
			$maildata['email_content'] = $message_template->renderContent($message_template->content, $maildata);	
			Mail::send('emails.userFeedbackToAdmin', $maildata, function($message) use($maildata) {
				$message->to($maildata['to_email'], 'Harpar Admin' )->subject('You have a new feedback');
			});
		}else{
			
			$message_template = Setting::where('name','=','adminfeedback_email')->first();
			$maildata['email_content'] = $message_template->renderContent($message_template->content, $maildata);			
			Mail::send('emails.adminFeedbackToUser', $maildata, function($message) use($user) {
				$message->to($user->email, $user->firstname.$user->lastname)->subject('You have a new feedback for request');
			});			
		}
		
		return Response::json($response);
		
	}
	
	
	/**
	 * Update user status on a module
	 *	 	 
	 * @return JSON Response
	 */
	public function postUserModuleStatus()
	{
		//Form values
		$userid = Input::get('userID');
		$moduleid = Input::get('moduleID'); 
		$courseid = Input::get('courseID');
		$statuscode = Input::get('status');
		
		//Email data
		$user = User::find($userid);
		$course = Course::find($courseid);
		$module = Module::find($moduleid);
		$maildata = array();
		$maildata['firstname'] = $user->firstname;
		$maildata['lastname'] = $user->lastname;
		$maildata['email'] = $user->email;
		$maildata['coursetitle'] = $course->title;
		$maildata['moduletitle'] = $module->title;
		
		//Response
		$response = array();
		$response['msg'] = "";
		
		$exist = DB::table('user_module_status')->where('userID',$userid)
						->where('moduleID',$moduleid)
						->where('courseID',$courseid)
						->first();
						
		if(count($exist)>0)	{
			DB::table('user_module_status')->where('id',$exist->id)
						->update(array('user_status'=>$statuscode,'updated_at'=>date('Y-m-d h:i:s')));	
			$response['msg'] = "Module status updated";	
			
			//Send email to the user
			$setting = Setting::where('name','=','usersubmission_moderated_email')->first();
			$maildata['email_content'] = $setting->renderContent($setting->content, $maildata);
			
			Mail::send('emails.userSubmissionModeratedEmail', $maildata, function($message) use($maildata) {
				$message->to( $maildata['email'], $maildata['firstname'].' '.$maildata['lastname'])->subject('Your Harpar module has been moderated');
			});		
			
							
		}else{
			 DB::table('user_module_status')->insert(array(
				'userID' => $userid, 
				'moduleID' => $moduleid, 
				'courseID' => $courseid, 
				'user_status' => $statuscode,
				'created_at' => date('Y-m-d h:i:s'),
				'updated_at' => date('Y-m-d h:i:s')  
			));
			
			//Log Activity
			$log =  new ActivityLog();
			$log->title = 'Update module status';
			$log->activity = 'module status';
			$log->url = Request::url();
			$log->userID = Auth::user()->id;
			$log->save();

			//Send email to the user
			$setting = Setting::where('name','=','usersubmission_moderated_email')->first();
			$maildata['email_content'] = $setting->renderContent($setting->content, $maildata);			
						
			Mail::send('emails.userSubmissionModeratedEmail', $maildata, function($message) use($user) {
				$message->to( $user->email, $user->firstname.$user->lastname)->subject('Your Harpar module has been moderated');
			});	
							
			$response['msg'] = "Module status updated";
		}

		//check if user completed all modules.
        $count = DB::table('modules')->where('courseID',$courseid)->count();

        $user_module_left = DB::table('user_module_status')
            ->where('userID',$userid)
            ->where('courseID',$courseid)
            ->where('user_status',3)
            ->count();




        $user_certificate_invitation = DB::table('user_courses')->where('courseID','=',$courseid)->where('userID','=',$userid)->first();
        //print_r($user_certificate_invitation->certification_invitation_code);

        //exit;
        if($count==$user_module_left+1 && $user_certificate_invitation->certificate_payment==1) {
            if($module->is_certificate == 1 && $statuscode == 3){
                $user_certificate_ready= DB::table('user_courses')
                    ->where('courseID','=',$courseid)
                    ->where('userID','=',$userid)
                    ->update(array('certificate_ready'=>1));

                //Send certification complettion email
                $setting = Setting::where('name','=','user_certification_completion_email')->first();
                $maildata['course_slug'] = $course->course_slug;
                $maildata['email_content'] = $setting->renderContent($setting->content, $maildata);

                Mail::send('emails.userSubmissionModeratedEmail', $maildata, function($message) use($maildata) {
                    $message->to( $maildata['email'], $maildata['firstname'].' '.$maildata['lastname'])->subject('Congratulations! You have successfully completed your certification');
                });
        	}
		}

		if($count==$user_module_left+2 && $user_certificate_invitation->certificate_payment==0){

                $random_code = Str::quickRandom(200);


                $certificate_invitation = DB::table('user_courses')->where('courseID','=',$courseid)->where('userID','=',$userid)->update(array('certification_invitation_code'=>$random_code));

                //Send invitation email to user
                $setting = Setting::where('name','=','usercourse_certification_invite_code_email')->first();
                $maildata['token'] = $random_code;
                $maildata['course_slug'] = $course->course_slug;
                $maildata['email_content'] = $setting->renderContent($setting->content, $maildata);

                Mail::send('emails.userSubmissionModeratedEmail', $maildata, function($message) use($maildata) {
                    $message->to( $maildata['email'], $maildata['firstname'].' '.$maildata['lastname'])->subject('Congratulations! You have successfully passed the course. Get Your certification');
                });
		}
		
		
		return Response::json($response);			
	}
	
	/**
	 * Update user password view
	 *	 	 
	 * 
	 */
	public function getChangePassword($userid)
	{

		if(Auth::user()->user_type == 0){
			$user =  User::find($userid);
			$password = $user->generateRandomString(10);
			$user->password = Hash::make($password);
			$user->save();
			
			//Log Activity
			$log =  new ActivityLog();
			$log->title = 'Auto user password generate and mailed to user email.';
			$log->activity = 'password emailed';
			$log->url = Request::url();
			$log->userID = Auth::user()->id;
			$log->save();
			
			$maildata['username'] = $user->username;
			$maildata['firstname'] = $user->firstname;
			$maildata['lastname'] = $user->lastname;
			$maildata['email'] = $user->email;
			$maildata['password'] = $password;
			
			//Send mail with username
			$setting = Setting::where('name','=','passwordgenerated_email')->first();
			$maildata['email_content'] = $setting->renderContent($setting->content, $maildata);			
			
			Mail::send('emails.passwordemailed',$maildata, function($message) use($maildata)
			{
				$message->to($maildata['email'], $maildata['firstname'].' '.$maildata['lastname'])->subject('Your new password');
			});						
			
			return Redirect::to('users')->withFlashNotice('Password generated for user: '.$user->firstname.' '.$user->lastname.' successfully');			
		}
				
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'users';
		$data['template_part'] = 'changepassword';
		$data['userid'] = $userid;
		
		$data['user_modules']   = DB::select( DB::raw("SELECT modules.*,courses.id as cid,courses.course_slug as slug FROM modules inner join user_courses on user_courses.courseID=modules.courseID and user_courses.userID = ".$userid.' inner join courses on user_courses.courseID=courses.id'));

		$user_courses = DB::select( DB::raw("SELECT courses.* FROM courses inner join user_courses on  user_courses.courseID=courses.id AND user_courses.userID=".$userid." order by courses.created_at DESC"));
		
		//find the first module of each course and add those to course objects
		$data['user_courses'] = array();		
		if(count($user_courses)>0){
			$i=0;
			foreach( $user_courses as $course){							 
				 $module = DB::table('modules')->where('courseID',$course->id)->first();				 
				 $course->moduleID = $module->id;
				 array_push($data['user_courses'],$course);
				 $i++;
			}
		}
		
		$data['user_current_course'] = DB::table('user_courses')
										->where('userID',$userid)
										->join('courses','courses.id','=','user_courses.courseID')
										->join('course_modules','course_modules.courseID','=','courses.id')
										->groupBy('courses.id')
										->first();

		return View::make('users',$data);		
	}
	
	/**
	 * Action Update user password 
	 *	 	 
	 * 
	 */
	public function postChangePassword($userid)
	{
		
		$data = array();
		$data['siteurl']              = Config::get('app.url');
		$data['selected']             = 'users';
		$data['template_part']        = 'changepassword';
		
		$data['userid']               = $userid;
		
	    $validator                    = Validator::make( 
											Input::all(),
											array(
												'currentpassword' => 'required|min:5',
												'newpassword' => 'required|min:6|confirmed',
												'newpassword_confirmation' => 'required|min:6'
											)
										);
	
	    if($validator->fails()){
				return Redirect::to('changepassword/'.$userid)->withErrors($validator);
		}
		
		$cur_password                 = Input::get('currentpassword');
		$new_password                 = Input::get('newpassword');

		$user = User::find($userid);
		$credentials = array('username'=>$user->username,'password'=>$cur_password );
		if (Auth::validate($credentials))
		{
			$user->password = Hash::make($new_password);
			$user->save();
			
			//Log Activity
			$log =  new ActivityLog();
			$log->title = 'Change user password';
			$log->activity = 'password change';
			$log->url = Request::url();
			$log->userID = Auth::user()->id;
			$log->save();			
			
			return Redirect::to('changepassword/'.$userid)->withSuccessMessage('Password changed successfully');
		}		
		else{
			
			//Log Activity
			$log =  new ActivityLog();
			$log->title = 'Failed password change attempt';
			$log->activity = 'password change';
			$log->url = Request::url();
			$log->userID = Auth::user()->id;
			$log->save();	
					
			return Redirect::to('changepassword/'.$userid)->withFlashNotice('Your current password didnt matched with our record. Please try again.');
		}			
	}
	
	/**
	 * Delete Users
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteUser($id)
	{
		//Delete resource info into database
		DB::table('users')->where('id',$id)->delete();
		DB::table('user_courses')->where('userID',$id)->delete();
		DB::table('user_module_feedback')->where('userID',$id)->delete();
		DB::table('user_submissions')->where('userID',$id)->delete();
		return Redirect::to('users')->withFlashNotice('User deleted successfully');		
	}	
	
	/**
	 * Upload users certificates
	 *
	 * @param  int  $userid
	 * @return Response
	 */
	public function addUserCertificate()
	{
		//$fileName = $_FILES["file1"]["name"];
		$uid = Input::get('userID');
		$uploader_id = Auth::user()->id;
		$fileName = '';
		$file_extension = '';
		
		//validation
/*		 $file = array('image' => Input::file('file1'));
	  	// setting up rules
	  	 $rules = array('image' => 'max:10000|mimes:jpeg,bmp,png.gif,pdf,doc,docx,ppt,pptx'); //
		 $response = array();
		 $validator = Validator::make($file, $rules);
		  if ($validator->fails()) {
			// send back to the page with the input data and errors
			$errors =  $validator->messages();
			foreach($errors as $e)
			$response['errormessage'] .= $e.'<br/>';
			return Response::json($response);
		  }*/	
		  
		  	
		$destinationPath = "uploads/user-submission/certificates-".$uid;
		if(!is_dir($destinationPath))
			mkdir($destinationPath,0777);

        $fileName = '';
					
		if(Input::hasFile('file1')){
			$fileName = $_FILES["file1"]["name"];
			$file_extension = Input::file('file1')->getClientOriginalExtension();
			
			if(!in_array($file_extension,array('jpg','jpeg','gif','pdf','doc')))
				return Response::json(array('success'=>'error','errormessage'=>'Invalid file type. Please upload only pdf,jpg, jpeg, gif, doc'));
			else
			Input::file('file1')->move($destinationPath, $fileName);
		}
		
		//Add submission info into database
		//Insert user submission
		$created_at = date('Y-m-d h:i:s');
		DB::table('user_certificates')->insert(array(
			'filename' => $fileName, 
			'filepath' => $destinationPath, 
			'userID' => $uid, 
			'approved_by' => $uploader_id,
			'created_at' => $created_at,
			'updated_at' => date('Y-m-d h:i:s')  
		));

		$response = array(
						'success'=>'OK',
						'msg'=>'Successfully uploaded',
						'filename'=>$fileName,
						'filetype'=>$file_extension,
						'certificate_id'=> DB::getPdo()->lastInsertId(),
						'created_at' => date('d/m/Y')
						);		

		
		//Log activity
		$log =  new ActivityLog();
		$log->title = 'Certificate Upload';
		$log->activity = 'upload';
		$log->url = Request::url();
		$log->userID = Auth::user()->id;
		$log->save();	
		
		return Response::json($response);	
	}
	
	/**
	 * Delete Certificates
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function deleteCertificate($id)
	{
		//Delete resource info into database
		DB::table('user_certificates')->where('id',$id)->delete();
		return Response::json(array('success'=>'OK','msg'=>'Certificate successfully deleted'));		
	}
	
	
	/**
	 * Get all Certificates
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function showCertificates()
	{
		$data = array();
		$data['siteurl'] = Config::get('app.url');
		$data['selected'] = 'certificates';
		$data['template_part'] = 'certificates';
		$data['user'] = Auth::user();
		
		$limit = 20;
		$page = (Input::get('page')!='')?Input::get('page'):1;

		$data['certificates'] = array();
		$certificates = DB::table('user_courses')
            ->where('user_courses.userID',$data['user']->id)
            ->where('user_courses.certificate_payment',1)
            ->where('user_courses.certificate_ready',1)
            ->join('courses','courses.id','=','user_courses.courseID')
            ->paginate($limit);

		$data['certificates_links'] = $certificates;

		if(count($certificates)>0){

			foreach($certificates as $key=>$row){

				$certification_module = DB::table('modules')
						->where('modules.is_certificate',1)
						->where('modules.courseID','=',$row->courseID)
						->first();


				$cert_mod_status = DB::table('user_module_status')
						->where('userID',Auth::user()->id)
						->where('courseID',$row->courseID)
						->where('moduleID',$certification_module->id)
						->first();

				$course = DB::table('courses')->where('id',$row->courseID)->first();

				$row->start_date = date('d/m/Y', strtotime($row->enroll_date));
				$row->end_date = date('d/m/Y',strtotime($cert_mod_status->updated_at));

				array_push($data['certificates'], $row);
				//$data['certificates']->{$key}->start_date = 'additional_data';
			}
		}

		$data['certificate_name'] = $data['user']->firstname.' '.$data['user']->lastname;


		$data['certificates_total'] = count($certificates);
		
		$data['certificates_current_total'] = (($page*$limit)>$data['certificates_total'])?$data['certificates_total']:($page)*$limit;

		$data['certificates_current_start'] = (($page-1)*$limit)+1;
		return View::make('users',$data);		
	}

    /**
     * Upload users certificates
     *
     * @param  int  $userid
     * @return JSON Response
     */
    public function addFeedbackAttachment()
    {
        //$fileName = $_FILES["file1"]["name"];
        $uid = Input::get('userID');
        $uploader_id = Auth::user()->id;
        $fileName = '';
        $file_extension = '';


        $destinationPath = "/uploads/user-submission/feedback-".$uid;
        if(!is_dir($destinationPath))
            mkdir($destinationPath,0777);

        $fileName = '';

        if(Input::hasFile('file1')){
            $fileName = $_FILES["file1"]["name"];
            $file_extension = Input::file('file1')->getClientOriginalExtension();

            if(!in_array($file_extension,array('jpg','jpeg','gif','png','pdf','doc','ppt','pptx','docx')))
                return Response::json(array('success'=>'error','errormessage'=>'Invalid file type. Please upload only pdf,jpg, jpeg, gif, doc'));
            else
                Input::file('file1')->move($destinationPath, $fileName);
        }

        //Add submission info into database
        //Insert user submission
        $created_at = date('Y-m-d h:i:s');
        $title = (Auth::user()->user_type == 0)?"Admin Feedback Attachment for Learner":"Learner Feedback Attachment for Admin";
        DB::table('resources')->insert(array(
            'title' => $title,
            'attachment_name' => $fileName,
            'attachment_type' => $file_extension,
            'attachment_path' => $destinationPath,
            'created_at' => $created_at,
            'updated_at' => date('Y-m-d h:i:s')
        ));

        $response = array(
            'success'=>'OK',
            'msg'=>'Successfully uploaded',
            'filename'=>$fileName,
            'filetype'=>$file_extension,
            'attachment_id'=> DB::getPdo()->lastInsertId(),
            'created_at' => date('d/m/Y')
        );


        //Log activity
        $log =  new ActivityLog();
        $log->title = 'Attachment Upload With Feedback';
        $log->activity = 'feedback attachment';
        $log->url = Request::url();
        $log->userID = Auth::user()->id;
        $log->save();

        return Response::json($response);
    }

    /**
     * Delete Attachment
     *
     * @param  int  $id
     * @return Response
     */
    public function deleteAttachment($id)
    {
        //Delete resource info into database
        DB::table('resources')->where('id',$id)->delete();
        return Response::json(array('success'=>'OK','msg'=>'Attachment successfully deleted'));
    }

    /**
     * Get Certified View
     *
     */
    public function getCertified($courseslug,$token){
        $data = array();
        $data['siteurl'] = Config::get('app.url');
        $data['selected'] = 'users';
        $data['template_part'] = 'getCertified';
        $data['courseslug'] = $courseslug;
        $data['invitetoken'] = $token;

        //echo $courseslug;

        $course = DB::table('courses')->where('course_slug',$courseslug)->first();


		$settings = DB::table('settings')->where('name','certification_flat_fee')->first();
		if(count($settings)>0)
			$data['certification_price'] = $settings->content;



        $data['coursetitle'] = $course->title;
        $data['course_id'] = $course->id;


        return View::make('users',$data);

    }

    public function postPayment()
    {
        $rules = array(
            'card-holder-firstname'             => 'required',                        // just a normal required validation
            'card-holder-lastname'             => 'required',
            'addressline1'            => 'required',     // required and must be unique in the ducks table
            'city'             => 'required',
            'zip'              => 'required',
            'card-number'         => 'required|numeric',
            'cvv' => 'required|numeric'           // required and has to match the password field
        );
        // do the validation ----------------------------------
        // validate against the inputs from our form
        $validator = Validator::make(Input::all(), $rules);
        $courseslug = Input::get('courseslug');
        $invitetoken = Input::get('invitetoken');

        // check if the validator failed -----------------------
        if ($validator->fails()) {
            // get the error messages from the validator
            $messages = $validator->messages();

            // redirect our user back to the form with the errors from the validator
            return Redirect::to('/users/getcertified/'.$courseslug.'/'.$invitetoken)
                ->withErrors($validator);
        }else{


        // ### Address
        // Base Address object used as shipping or billing
        // address in a payment. [Optional]
        $addr= Paypalpayment::address();
        $addr->setLine1(Input::get('addressline1'));
        $addr->setLine2(Input::get('addressline2'));
        $addr->setCity(Input::get('city'));
        $addr->setPostalCode(Input::get('zip'));
        $addr->setCountryCode(Input::get('country'));
        $addr->setPhone(Input::get('phone'));

        // ### CreditCard
        $card = Paypalpayment::creditCard();
        $card->setType(Input::get('cardtype'))
            ->setNumber(Input::get('card-number'))
            ->setExpireMonth(Input::get('expiry-month'))
            ->setExpireYear(Input::get('expiry-year'))
            ->setCvv2(Input::get('cvv'))
            ->setFirstName(Input::get('card-holder-firstname'))
            ->setLastName(Input::get('card-holder-lastname'));

        // ### FundingInstrument
        // A resource representing a Payer's funding instrument.
        // Use a Payer ID (A unique identifier of the payer generated
        // and provided by the facilitator. This is required when
        // creating or using a tokenized funding instrument)
        // and the `CreditCardDetails`
        $fi = Paypalpayment::fundingInstrument();
        $fi->setCreditCard($card);

        // ### Payer
        // A resource representing a Payer that funds a payment
        // Use the List of `FundingInstrument` and the Payment Method
        // as 'credit_card'
        $payer = Paypalpayment::payer();
        $payer->setPaymentMethod("credit_card")
            ->setFundingInstruments(array($fi));

        $item1 = Paypalpayment::item();
        $item1->setName(Input::get('coursetitle'))
            ->setDescription('Payment for the certification')
            ->setCurrency('USD')
            ->setQuantity(1)
            ->setTax(0.0)
            ->setPrice(Input::get('amount'));



        $itemList = Paypalpayment::itemList();
        $itemList->setItems(array($item1));


        $details = Paypalpayment::details();
        $details->setShipping("0.0")
            ->setTax("0.0")
            //total of items prices
            ->setSubtotal(Input::get('amount'));

        //Payment Amount
        $amount = Paypalpayment::amount();
        $amount->setCurrency("USD")
            // the total is $17.8 = (16 + 0.6) * 1 ( of quantity) + 1.2 ( of Shipping).
            ->setTotal(Input::get('amount'))
            ->setDetails($details);

        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it. Transaction is created with
        // a `Payee` and `Amount` types

        $transaction = Paypalpayment::transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber(uniqid());

        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent as 'sale'

        $payment = Paypalpayment::payment();

        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setTransactions(array($transaction));

        try {
            // ### Create Payment
            // Create a payment by posting to the APIService
            // using a valid ApiContext
            // The return object contains the status;
             $payment->create($this->_apiContext);

            $payment_id = $payment->id;
            $invoice_no = $payment->invoice_number;

            //echo $payment->update_time;

            $payment_data = array('payee_firstname'=>Input::get('card-holder-firstname'),
                'payee_lastname'=>Input::get('card-holder-lastname'),
                'payee_address'=>Input::get('addressline1').' '.Input::get('addressline2'),
                'payee_country'=>Input::get('country'),
                'payment_id'=>$payment_id,
                'payment_amount'=>Input::get('amount'),

                'course_id'=>Input::get('course_id'),
                'user_id'=>Auth::user()->id,
                'payment_date'=>str_replace('Z','',str_replace('T',' ',$payment->update_time)));

            // Log the activity
            $log =  new ActivityLog();
            $log->title = 'Payment Done for the course '.Input::get('coursetitle');
            $log->activity = 'Certification Payment';
            $log->url = Request::url();
            $log->userID = Auth::user()->id;
            $log->save();

            $update = DB::table('user_courses')
                ->where('userID', Auth::user()->id)
                ->where('courseID', Input::get('course_id'))
                ->update(array('certificate_payment' => 1));

            $storepaymentdata = Payment::create($payment_data);

            $course_certificate_module = DB::table('modules')->where('courseID','=',Input::get('course_id'))->where('is_certificate','=',1)->first();

        } catch (PayPal\Exception\PayPalConnectionException $ex) {
            echo $ex->getCode();
            echo $ex->getData();
            die($ex);
        }

        //dd($payment);

        return Redirect::to('/course/'.$courseslug.'/'.base64_encode($course_certificate_module->id))
            ->with('msg', 'Your payment was successful. Please refer to certification module to complete');
        }
    }


    public function getPaymentStatus()
    {
        $payment = Paypalpayment::getById('PAY-0A6045542K5577912LAJ7AWI',$this->_apiContext);

        print_r($payment->item_list);
    }

	/**
	 * Download certificate code
	 */
	public  function downloadCertificate($userid,$courseid){
		$pdf = App::make('dompdf.wrapper');
		$pdf->loadHTML('<h1>Test</h1>');
		return $pdf->download('certificate.pdf');
	}

}
