<div class="row courses">  
	<div class="col-md-7">  	
        <h3>{{ $user->firstname }} {{ $user->lastname }}
        </h3>
    </div>
</div>


<div class="row user-info">    
	<div class="col-md-12">     	
    	<div class="module-review">
        {{ Form::open(array('url' => 'login','method'=>'post')) }}
        <div class="row">
        	<div class="col-md-2">
            @if ($user->user_image=='')	
            <img src="{{ $siteurl }}/images/default-user.png" class="img-circle user-account-image"/>
            @else
            <img src="{{ $siteurl }}/uploads/{{ $user->user_image }}" class="img-circle user-account-image"/>
            @endif
           </div>
            <div class="col-md-5">
                     <p>
                     {{ Form::label('Firstname') }}
                     {{ Form::text('firstname', $user->firstname, array('class'=>'input-block-level','readonly'=>'readonly')) }}
                     </p>
            </div>
            <div class="col-md-5">
                      <p >
                     {{ Form::label('Lastname') }}
                     {{ Form::text('lastname', $user->lastname, array('class'=>'input-block-level','readonly'=>'readonly')) }}
                     </p>           
            </div>
        </div>
        <div class="row">
        	<div class="col-md-2">Password</div>
            <div class="col-md-5">
                     <p>
                     *********
                     </p>
            </div>
            <div class="col-md-5">
                      <p>
                     <a href="/changepassword/{{ $user->id }}" class="view-account pull-right">Change Password</a>
                     </p>           
            </div>
        </div>
        <div class="row">
        	<div class="col-md-2">Email</div>
            <div class="col-md-5">
                     <p>
                     {{ $user->email }}
                     </p>
            </div>
            <div class="col-md-5">
                      <p>
                     <a href="/edituser/{{ $user->id }}" class="view-account pull-right">Edit</a>
                     </p>           
            </div>
        </div>                
        {{ Form::close()}}	
            

        </div>
    </div>       
</div>


<div class="row courses">  
	<div class="col-md-9">  	
        <h3>History <span>(Displaying {{ $history_current_start }} - {{ $history_current_total }} of {{ $history_total }} activity)</span></h3>
    </div>
    <div class="col-md-3">
    <div class="pull-right">
    	{{ $history->links() }}
    </div>    
    </div>
</div>


<div class="row user-history">    
	<div class="col-md-12">     	
    	<div class="module-review"> 	
            <table class="table history" width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr class="head">
                <td width="5%">&nbsp;</td>
                <td width="20%">Date</td>
                <td width="35%">Description</td>
                <td width="20%">Activity</td> 
              </tr>
              @define $i=$history_total-($history_current_start-1)
              @foreach ($history as $h)
              <tr class="odd">
                <td>{{ $i }}</td>
                <td>{{ $h->created_at }}</td>
                <td>{{ $h->title }}</td>
                <td>{{ $h->activity }}</td>                 
              </tr>
              @define $i--
              @endforeach
            </table>

        </div>
    </div>       
</div>
</div>
