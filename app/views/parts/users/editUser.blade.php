<div class="row">
            <div class="col-md-12">
            		<p>
                     <a href="/changepassword/{{ $user->id }}" class="edit-account pull-right">Change Password</a>
                     </p>
            </div>
</div>
<div class="row">
            <div class="col-md-12 content-wrapper">
            
                <div class="register">
                <h3>Edit user info</h3>
                @if (Session::has('flash_notice'))
                	<span class="error">
                    {{ Session::get('flash_notice') }}
                    </span>
                @endif
                
                                
                @foreach ($errors->all() as $message)
                {{ $message }}
                @endforeach
                
                
                	{{ Form::open(array('url' => 'edituser/'.$user->id,'method'=>'post', 'enctype'=>'multipart/form-data')) }}
                     <p>
                         {{ Form::label('First Name')}}
                         {{ Form::text('firstname',$user->firstname)}}
                         <span class="error">
                            @if ($errors->has('firstname'))
                            {{ $errors->first('firstname') }}
                            @endif
                         </span>
                     </p>


                     <p>
                         {{ Form::label('Last Name')}}
                         {{ Form::text('lastname',$user->lastname)}}
                         <span class="error">
                            @if ($errors->has('lastname'))
                            {{ $errors->first('lastname') }}
                            @endif
                         </span>
                     </p> 
                     
                     <p>
                         {{ Form::label('Email Address')}}
                         {{ Form::email('email',$user->email)}}
                         <span class="error">
                            @if ($errors->has('email'))
                            {{ $errors->first('email') }}
                            @endif
                         </span>
                     </p> 
                     @if (Auth::user()->user_type == 0)
                     <p>
                         {{ Form::label('Username')}}
                         {{ Form::text('username',$user->username)}}
                         <span class="error">
                            @if ($errors->has('username'))
                            {{ $errors->first('username') }}
                            @endif
                         </span>
                     </p>
                     
 
                    <p>
                         {{ Form::label('Password')}}
                         {{ Form::password('password')}}
                         <span class="error">
                            @if ($errors->has('password'))
                            {{ $errors->first('password') }}
                            @endif
                         </span>
                     </p> 
 
                       <p>
                         {{ Form::label('Confirm Password')}}
                         {{ Form::password('password_confirmation')}}
                         <span class="error">
                            @if ($errors->has('password_confirmation'))
                            {{ $errors->first('password_confirmation') }}
                            @endif
                         </span>
                     </p>                     
                     @endif 
                     
                                         
                      <p>
                         {{ Form::label('Address Line 1')}}
                         {{ Form::text('address1',$user->address1)}}
                         <span class="error">
                            @if ($errors->has('address1'))
                            {{ $errors->first('address1') }}
                            @endif
                         </span>
                     </p>                    
                             
                     <p>
                         {{ Form::label('Address Line 2')}}
                         {{ Form::text('address2',$user->address2)}}
                         <span class="error">
                            @if ($errors->has('address2'))
                            {{ $errors->first('address2') }}
                            @endif
                         </span>
                     </p>                             


                     <p>
                         {{ Form::label('Town')}}
                         {{ Form::text('town',$user->town)}}
                         <span class="error">
                            @if ($errors->has('town'))
                            {{ $errors->first('town') }}
                            @endif
                         </span>
                     </p> 
                     
                     <p>
                         {{ Form::label('District')}}
                         {{ Form::text('district',$user->district)}}
                         <span class="error">
                            @if ($errors->has('district'))
                            {{ $errors->first('district') }}
                            @endif
                         </span>
                     </p>                      
                     
                     <p>
                         {{ Form::label('Postcode')}}
                         {{ Form::text('postcode',$user->postcode)}}
                         <span class="error">
                            @if ($errors->has('postcode'))
                            {{ $errors->first('postcode') }}
                            @endif
                         </span>
                     </p>                                                                                                              
                      <p>
                         {{ Form::label('User Image')}}
                         {{ Form::file('user_image')}}
                         <span class="error">
                            @if ($errors->has('user_image'))
                            {{ $errors->first('user_image') }}
                            @endif
                         </span>
                         @if ($user->user_image=="")
                         <img src="/images/default-user.png" class="img-responsive edit-image"/>
                         @else
                         <img src="/uploads/{{ $user->user_image }}" class="img-responsive edit-image"/>
                         @endif
                     </p> 
                     @if (Auth::user()->user_type == 0)
                      <p>
                         {{ Form::label('User Access')}}
                         
                         <div class="radio-group">                         
                            
                             <div class="field">
                             @if ($user->user_type=="0")
                             {{ Form::radio('user_type', 'Admin', true) }}
                             @else
                             {{ Form::radio('user_type', 'Admin') }}
                             @endif
                             </div>
                             <div class="label">Admin</div>
                             <div class="field">
                             @if ($user->user_type=="1")
                             {{ Form::radio('user_type', 'Learner', true) }}
                             @else
                             {{ Form::radio('user_type', 'Learner') }} 
                             @endif                            
                              </div>
                             <div class="label">Learner</div>                             
                         </div>                                           
                     </p> 
                     
                     <p>
                     
                     <div class="radio-group user-courses">  
                     {{ Form::label('Select Courses')}}
<!--                     <select name="number">
                      <option value="">Select a course</option>-->
                    
                     @if  (isset($courses))
                     	@foreach($courses as $course)
                        @define $sel= in_array($course->id,$user_enrolled_courses)?true:false
                         
                        <div class="row">
                        <div class="col-md-8">
                           <div class="course-item">
                           <div class="field">
                                {{ Form::checkbox('user_courses[]', $course->id, $sel) }}
                           </div>
                           <div class="label">{{ $course->title }}  </div>
                           </div>
                        </div>
                        <div class="col-md-2">
                        	{{ Form::label('Expires On')}}
                        </div> 
                        <div class="col-md-2" id="enrolldate-hidden-{{ $course->id }}">
                        	 
                        	{{ Form::text('enroll_c'.$course->id, isset($course->exp_date)?date('Y-m-d',strtotime($course->exp_date)):date('Y-m-d',strtotime('+2 years')),array('id'=>'enroll_c'.$course->id,'class'=>'enroldate'))}}
                        </div>
                       </div>                       
                                             
                        @endforeach
                     @endif
                     <!--</select>-->
                     </div>
                     </p>
    
                     <div class="module-upload hidden">  
                     <h3>Certificates</h3>   	
                    	<div class="certificate-container"> 
                        <div class="certificate-container-inner"> 
                            <div class="fields-container">
                          		<div class="drop"> 
                              
                                 <input type="file" multiple="" id="certificate_submission" name="certificate_submission">
                                 
                                        
                                 
                                 <a id="addnew_certificate_trigger"  onclick="return addNewCertificate({{ $user->id }})" user_id="{{ Auth::user()->id }}" disabled-message="" href="#" class="pinkbutton"> Add <i class="fa fa-angle-right"></i></a>
                                 
                                  
                                  <a tabindex="0" href="javascript:void()" id="lock_msg"  role="button" data-toggle="popover" data-trigger="focus" data-content="Browse for certificate, supported type: jpg, jpeg, gif, pdf,doc,docx"><i class="fa fa-info-circle"></i>
            </a> 
                                   
                              
                                 </div> 
                                 
             					<ul class="user-certificate-items" id="certificate-items">
                              		@if(isset($certificates))
                                    @foreach($certificates as $ct)
                                    <li id="cer-{{ $ct->id }}"> <span class="date">{{ date('d/m/Y',strtotime($ct->created_at)) }}</span><span class="filename">{{ $ct->filename }}</span> <span class="date"> <a href="#" onclick="return deleteCertificate({{ $ct->id }})">Delete</a></span> </li>
                                         
                                    @endforeach
                                    @endif
                         		</ul>
                            </div>
                        </div>
                    </div>
    				 </div>              
                     @endif
                   
                      <p>
                     {{ Form::submit('Update')}}
                     </p>    
                     <input name="user_courses_old" value="{{ implode(',',$user_enrolled_courses) }}" type="hidden"/>
                     {{ Form::close()}}                 
                </div>
            </div>
        </div>