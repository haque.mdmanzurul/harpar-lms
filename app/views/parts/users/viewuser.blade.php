<div class="userview">
<div class="row">
	<div class="col-md-12"><span class="breadcumb">Users > {{$user->firstname}} {{$user->lastname}}</span></div>
</div>

<div class="row courses">  
	<div class="col-md-7">  	
        <h3>Registered Users</h3>
    </div>
    
    <div class="col-md-3">    	
    </div>
    
    <div class="col-md-2">
    @if (Auth::user()->user_type == 0)
    	<a class="pinkbutton pull-right" href="{{ $siteurl }}/users">See All <i class="fa fa-angle-right"></i></a>
    @endif
    </div>
</div>

<div class="row userinfo">  
	<div class="col-md-1">  
    	@if ($user->user_image=='')	
        <img src="{{ $siteurl }}/images/default-user.png" class="img-circle"/>
        @else
        <img src="{{ $siteurl }}/uploads/{{ $user->user_image }}" class="img-circle"/>
        @endif
    </div>
    
    <div class="col-md-7"> 
    	<h3>{{$user->firstname}} {{$user->lastname}} <span>( <a href="{{ $siteurl }}/edituser/{{$user->id}}">Edit User</a> )</span></h3>  
        	
    </div>
    
    <div class="col-md-2">
   @if (Auth::user()->user_type==0) <a class="pull-right view-account" href="{{ $siteurl }}/loginasuser/{{$user->id}}">Login as user</a> @endif
    </div>
    <div class="col-md-2">
    <a class="pull-right view-account" href="{{ $siteurl }}/users/{{$user->username}}/userinfo">View Account</a></li>
    
    </div>
</div>

<div class="row user-review">  
	<div class="col-md-12">  		      
        @if (count($user_courses)>1)
             <a aria-expanded="true" role="button" aria-haspopup="true" data-toggle="dropdown" class="dropdown-toggle" href="#" id="drop4">
             @if($user_admin_view)
             Select a course
             @else
             {{ $user_current_course->title }}
             @endif
              <span class="caret"></span>
            </a>        
        @define $i = 1
        	@if (count($user_courses)>1)
            <ul aria-labelledby="drop4" role="menu" class="dropdown-menu" id="menu1">
            @endif
            
        	@foreach ($user_courses as $uc)
				@if($user_current_course->id == $uc->id and !$user_admin_view)
                @define continue
                @endif
             	 <li role="presentation"><a href="{{ $siteurl }}/users/{{ $user->username }}/course/{{ $uc->course_slug }}" tabindex="-1" role="menuitem">{{ $uc->title }}</a></li>
             @define $i++
            @endforeach
             @if (count($user_courses)>1)
                 </ul>
             @endif
        @else        
             <a aria-expanded="true" role="button" aria-haspopup="true" data-toggle="dropdown" class="dropdown-toggle" href="#" id="drop4">
             {{ isset($user_current_course)?$user_current_course->title:'' }}
            </a>       
      
        @endif
        
    </div>
    
	<div class="col-md-12"> 
    	
    	<div class="module-review"> 	
        	<h3>Modules <span>
            @define $i=0
              @if(isset($user_submissions) and count($user_submissions)>0)
                  
                  @foreach($user_submissions as $us)
                    @if($us->module_status == 0)
                    @define $i++
                    @endif
                  @endforeach
              @endif
            @if($i > 0)* You have <span class="text-primary">{{ $i }}</span> new  @if($i > 1) documents @else document @endif to review * @endif
            
            </span></h3>
            
            <table class="table" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr class="head">
    <td width="5%">&nbsp;</td>
    <td width="20%">Title</td>
    <td width="35%">Latest Submission</td>
    <td width="20%">&nbsp;</td>
    <td width="20%">Status</td>
  </tr>
  @if(isset($user_submissions) and count($user_submissions)>0)
  @define $i=count($user_submissions)
  @foreach($user_submissions as $us)
  
  <tr class="odd">
    <td>{{ $i }}</td>
    <td>{{ $us->title }}</td>
    <td>{{ isset($us->user_submission)?$us->user_submission->attachment_name:'' }}<br/>
    <a class="view-account" href="/users/{{$user->username}}/submissionhistory/{{ $us->id }}">View History</a>
    </td>
    <td>
    <span class="download"><a href="{{ $siteurl }}/download/{{ isset($us->user_submission)?base64_encode($us->user_submission->id):'' }}" class="pinkbutton">Download <i class="fa fa-angle-right"></i>
</a></span>
</td>
    <td>
    <div class="status-control">  
    <select name="status" id="status-{{ $user->id }}-{{ $us->id }}-{{ $us->cid }}" class="submission-status" onchange="change_user_module_status({{ $user->id }} ,{{ $us->id }},{{ $us->cid }})">
        <option value="0" @if($us->module_status==0) selected="selected" @endif >Submitted</option>
        <option value="1" @if($us->module_status==1) selected="selected" @endif >Referred</option>
        <option value="2" @if($us->module_status==2) selected="selected" @endif >Feedback</option>
        <option value="3" @if($us->module_status==3) selected="selected" @endif >Passed</option>
    </select> 
    <span class="status-message" id="status-message-{{ $user->id }}-{{ $us->id }}-{{ $us->cid }}"></span>    
    <span class="feedback-link">
    	<a class="add-feedback viewaccount fancybox" href="#inline1" moduleID="{{ $us->id }}" courseID="{{ $us->cid }}" userID="{{ $user->id }}">Add Feedback <i class="fa fa-angle-right"></i> </a>
    </span>
    </div>
    
    </td>
  </tr>
  @define $i--
  @endforeach
  @endif
</table>
	
        </div>
    </div>    
</div>
    @if(!$user_admin_view && $user_current_course->type!=1)
<div class="row user-review user-certificate-upload" style="margin-top: 30px">
    <div class="col-md-12">
        <div class="module-review">
            <h3> Upload Certificate</h3>
            <div class="module-upload">
                
                <div class="certificate-container" style="margin-left: 25px">
                    <div class="certificate-container-inner">
                        <div class="fields-container">
                            <div class="drop">

                                <input type="file" multiple="" id="certificate_submission" name="certificate_submission">



                                <a id="addnew_certificate_trigger"  onclick="return addNewCertificate({{ $user->id }}, {{$user_current_course->id}})" user_id="{{ Auth::user()->id }}" disabled-message="" href="#" class="pinkbutton"> Add <i class="fa fa-angle-right"></i></a>


                                <a tabindex="0" href="javascript:void()" id="lock_msg"  role="button" data-toggle="popover" data-trigger="focus" data-content="Browse for certificate, supported type: jpg, jpeg, gif, pdf,doc,docx"><i class="fa fa-info-circle"></i>
                                </a>


                            </div>
<h3>Certificates</h3>
                            <ul class="user-certificate-items" id="certificate-items">
                                @if(isset($certificates))
                                    @foreach($certificates as $ct)
                                        <li id="cer-{{ $ct->id }}"> <span class="date">{{ date('d/m/Y',strtotime($ct->created_at)) }}</span><span class="filename">{{ $ct->filename }}</span> <span class="date"> <a href="#" onclick="return deleteCertificate({{ $ct->id }})">Delete</a></span> </li>

                                    @endforeach
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
    @endif

</div>
<div id="inline1" style="width:500px;display: none;">
    <form name="feedbackForm" id="adminFeedbackForm" class="feedbackForm" method="post" action="#">
		<h3>Questions or feedback? </h3>
        <p>
            Complete the form below
        </p>
        
        <p>
        	<input type="hidden" name="feedbackModuleID" id="feedbackModuleID" value="" />
            <input type="hidden" name="feedbackCourseID" id="feedbackCourseID" value="" />
            <input type="hidden" name="feedbackUserID" id="feedbackUserID" value="" />
            <input type="hidden" name="feedbackAttachmentsID" id="feedbackAttachmentsID" value="" />

			<textarea name="feedbackmessage" id="feedbackmessage" cols="40" rows="5" required="required"></textarea>
            <br/>
        <div class="module-upload">
            <h3>Attachments</h3>
            <div class="attachment-container">
                <div class="attachment-container-inner">
                    <div class="fields-container">
                        <div class="drop">

                            <input type="file" multiple="" id="attachment_submission" name="attachment_submission">



                            <a id="addnew_attachment_trigger"  onclick="return addNewAttachment({{ $user->id }})" user_id="{{ Auth::user()->id }}" disabled-message="" href="#" class="pinkbutton"> Add <i class="fa fa-angle-right"></i></a>


                            <a tabindex="0" href="javascript:void()" id="lock_msg"  role="button" data-toggle="popover" data-trigger="focus" data-content="Browse for certificate, supported type: jpg, jpeg, gif, pdf,doc,docx"><i class="fa fa-info-circle"></i>
                            </a>


                        </div>

                        <ul class="user-attachment-items" id="attachment-items">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </p>
        <p id="feedbackProgress"></p>
        <a class="pinkbutton" href="#" id="feedbackFormTrigger">Submit <i class="fa fa-angle-right"></i> </a>
    </form>    
</div>