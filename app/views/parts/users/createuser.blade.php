        <div class="row">
            <div class="col-md-12 content-wrapper">
            
                <div class="register">
                <h3>Enter new user info</h3>
                @if (Session::has('flash_notice'))
                	<span class="error">
                    {{ Session::get('flash_notice') }}
                    </span>
                @endif
                	{{ Form::open(array('url' => 'newuser','method'=>'post', 'enctype'=>'multipart/form-data')) }}
                     <p>
                         {{ Form::label('First Name')}}
                         {{ Form::text('firstname')}}
                         <span class="error">
                            @if ($errors->has('firstname'))
                            {{ $errors->first('firstname') }}
                            @endif
                         </span>
                     </p>


                     <p>
                         {{ Form::label('Last Name')}}
                         {{ Form::text('lastname')}}
                         <span class="error">
                            @if ($errors->has('lastname'))
                            {{ $errors->first('lastname') }}
                            @endif
                         </span>
                     </p> 
                     
                     <p>
                         {{ Form::label('Email Address')}}
                         {{ Form::email('email')}}
                         <span class="error">
                            @if ($errors->has('email'))
                            {{ $errors->first('email') }}
                            @endif
                         </span>
                     </p> 
                     
                     <p>
                         {{ Form::label('Username')}}
                         {{ Form::text('username')}}
                         <span class="error">
                            @if ($errors->has('username'))
                            {{ $errors->first('username') }}
                            @endif
                         </span>
                     </p> 
 
                      <p>
                         {{ Form::label('Password')}}
                         {{ Form::password('password')}}
                         <span class="error">
                            @if ($errors->has('password'))
                            {{ $errors->first('password') }}
                            @endif
                         </span>
                     </p> 
 
                       <p>
                         {{ Form::label('Confirm Password')}}
                         {{ Form::password('password_confirmation')}}
                         <span class="error">
                            @if ($errors->has('password_confirmation'))
                            {{ $errors->first('password_confirmation') }}
                            @endif
                         </span>
                     </p> 
                      
                     <p>
                         {{ Form::label('Address Line 1')}}
                         {{ Form::text('address1')}}
                         <span class="error">
                            @if ($errors->has('address1'))
                            {{ $errors->first('address1') }}
                            @endif
                         </span>
                     </p>                    
                             
                     <p>
                         {{ Form::label('Address Line 2')}}
                         {{ Form::text('address2')}}
                         <span class="error">
                            @if ($errors->has('address2'))
                            {{ $errors->first('address2') }}
                            @endif
                         </span>
                     </p>                             


                     <p>
                         {{ Form::label('Town')}}
                         {{ Form::text('town')}}
                         <span class="error">
                            @if ($errors->has('town'))
                            {{ $errors->first('town') }}
                            @endif
                         </span>
                     </p> 
                     
                     <p>
                         {{ Form::label('District')}}
                         {{ Form::text('district')}}
                         <span class="error">
                            @if ($errors->has('district'))
                            {{ $errors->first('district') }}
                            @endif
                         </span>
                     </p>                      
                     
                     <p>
                         {{ Form::label('Postcode')}}
                         {{ Form::text('postcode')}}
                         <span class="error">
                            @if ($errors->has('postcode'))
                            {{ $errors->first('postcode') }}
                            @endif
                         </span>
                     </p>                                                                                                              
                      <p>
                         {{ Form::label('User Image')}}
                         {{ Form::file('user_image')}}
                         <span class="error">
                            @if ($errors->has('user_image'))
                            {{ $errors->first('user_image') }}
                            @endif
                         </span>
                     </p> 
                     
                      <p>
                         {{ Form::label('User Access')}}
                         <div class="radio-group">
                             <div class="field">{{ Form::radio('user_type', 'Admin') }}</div>
                             <div class="label">Admin</div>
                             <div class="field">{{ Form::radio('user_type', 'Learner', true) }} </div>
                             <div class="label">Learner</div>                             
                         </div>                                           
                     </p>                      
                     
                      <p>
                      <div class="radio-group user-courses">  
                     {{ Form::label('Select Courses')}}
                    
                     @if  (isset($courses))
                     	@foreach($courses as $course)
                        <div class="row">
                        <div class="col-md-8">
                           <div class="course-item">
                           <div class="field">
                                {{ Form::checkbox('user_courses[]', $course->id) }}
                           </div>
                           <div class="label">{{ $course->title }}</div>
                           </div>
                        </div>
                        <div class="col-md-2">
                        	{{ Form::label('Expires On')}}
                        </div> 
                        <div class="col-md-2" id="enrolldate-hidden-{{ $course->id }}">
                        	 
                        	{{ Form::text('enroll_c'.$course->id, date('Y-m-d',strtotime('+2 years')),array('id'=>'enroll_c'.$course->id,'class'=>'enroldate'))}}
                        </div>
                       </div>   
                        @endforeach
                     @endif
                     <!--</select>-->
                     </div>
                     </p>
                   
                      <p>
                     {{ Form::submit('Create User')}}
                     </p>    
                     
                     {{ Form::close()}}                 
                </div>
            </div>
        </div>