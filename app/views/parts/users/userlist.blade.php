<div class="row courses">  

	<div class="col-md-7">  	
        <h3>Registered Users <span>(Displaying {{ $users_current_start }} - {{ $users_current_total }} of {{ $users_total }} users)</span></h3>
    </div>
    <div class="col-md-3">
    	{{ $users_pagination->links() }}
    </div>
    
    <div class="col-md-2">
    	<a href="{{ $siteurl }}/newuser" class="pinkbutton pull-right">Add <i class="fa fa-angle-right"></i>
</a>
    </div>
    @if(Session::get('flash_notice'))
    <div class="col-md-12">
    	<p class="bg-primary">{{ Session::get('flash_notice') }}</p>
    </div>
    @endif
</div>
<div class="row courses">
	<div class="col-md-12">
      <div class="pending-review-container">
        <table border="0" cellpadding="0" cellspacing="0" id="myTable" class="tablesorter table"> 
            <thead> 
            <tr> 
                <th width="15%">Name </th> 
                <th>Email</th> 
                <th>Course</th> 
                <th>EXP Date</th>
                <th>Status</th>    
            </tr> 
            </thead> 
            <tbody> 
            @if (count($users) > 0)
            @foreach($users as $user)
            <tr> 
                <td>{{$user->firstname}} {{$user->lastname}} @if($user->submission_count>0) <sup class="user-submission-count" title="{{ $user->submission_count }} new submissions"> {{ $user->submission_count }} </sup> @endif</td> 
                <td>{{$user->email}} </td> 
                <td>
                @define $c_date = ''
                @define $c_count = count($user->courses)
                @if (count($user->courses)>0)
                	@foreach ($user->courses as $c)
                    @if ($c->exp_date != '0000-00-00 00:00:00')
                    @define	$c_date = date('Y-m-d',strtotime($c->exp_date))
                    @endif
                    <a href="/users/{{$user->username}}/course/{{$c->course_slug}}">{{ $c->title }}</a>  @if($c->submission_count>0) <sup class="user-submission-count" title="{{ $c->submission_count }} new submissions"> <a href="{{ $siteurl }}/users/{{ $user->username }}/course/{{ $c->course_slug }}" title="View submissions">{{ $c->submission_count }} </a></sup> @endif <br/>
                    @endforeach
                @endif
                </td> 
                <td>
                 @define $c_date = ''
                @if (count($user->courses)>0)
                	@foreach ($user->courses as $c)
                    @if ($c->exp_date != '0000-00-00 00:00:00')
                    @define	$c_date = date('Y-m-d',strtotime($c->exp_date))
                    {{ $c_date }} <br/>
                    @endif
                   
                    @endforeach
                @endif
                </td>  
                <td> @if($c_count == 1)
                    <a href="/users/{{ $user->username }}/course/{{ $user->courses[0]->course_slug }}">View</a>
                    @else
                    <a href="/users/{{$user->username}}">View</a>
                    @endif
                    
                    | <a href="/edituser/{{$user->id}}">Edit</a> | <a class="delete" href="{{ $siteurl }}/deleteuser/{{$user->id}}" onclick="return delete_user()">Delete</a></td>
            </tr> 
            @endforeach
            @endif
 
            </tbody> 
            </table>
        </div>
    </div>
</div>