<div class="userview">

<div class="row courses details">  
	<div class="col-md-12">  	
        <h3>{{ $module->title }} @if($upload_disabled==3) (Passed) @endif </h3>
        <div style="color:#fff;padding-bottom: 20px">
            {{ nl2br(html_entity_decode($module->description)) }}
        </div>
        
    </div>
</div>

<div class="row user-review">
	<div class="col-md-12">     	
    	<div class="module-review"> 
        	<div class="module-review-inner"> 	
                <h3>Resources</h3>
                <div class="row">
                @foreach ($resources as $resource)
                <div class="col-md-4">
                    <div class="module-resource">
                        <div class="image">
                            @if ($resource->attachment_type == "pdf")
                                <i class="fa fa-file-pdf-o"></i>
                            @elseif ($resource->attachment_type == "doc" or $resource->attachment_type == "docx")
                                <i class="fa fa-file-word-o"></i>
                            @elseif($resource->attachment_type == "ppt" or $resource->attachment_type == "pptx")
                                <i class="fa fa-file-powerpoint-o"></i>
                            @elseif($resource->attachment_type == "story")
                                <i class="fa fa-file-audio-o"></i>
                            @elseif($resource->attachment_type == "youtube")
                                <i class="fa fa-youtube-square"></i>
                            @elseif($resource->attachment_type == "vimeo")
                                <i class="fa fa-vimeo-square"></i>
                            @endif
                        </div>
                    
                        <h3>{{ $resource->title }}</h3>
                        <div class="resource-footer">
                            @if($resource->attachment_type == "youtube" or $resource->attachment_type == "vimeo")
                                <a class="pinkbutton center youtube" data-media-type="{{$resource->attachment_type}}" data-video-code="{{$resource->attachment_name}}" href="#harpar-media-player">View <i class="fa fa-angle-right"></i></a>
                            @elseif($resource->attachment_type == "story")
                                <a class="pinkbutton center story" data-media-type="{{$resource->attachment_type}}" data-media-url="{{$siteurl.'/'.$resource->attachment_path.'/'.$resource->attachment_name}}" href="#harpar-media-player">View <i class="fa fa-angle-right"></i></a>
                            @else
                            <a class="pinkbutton center" href="{{ $siteurl }}/downloadResource/{{ base64_encode($resource->id) }}">Download <i class="fa fa-angle-right"></i></a>
                            @endif
                        </div>
                    </div>
                    
                   </div> 
                @endforeach    
                </div>
            </div>
        </div>
    </div>
    <br/>
    @if( $module->is_overview == 1 && $course->type==1)
    <div style="margin-top: 25px" class="col-md-12">
        <div class="module-review self-assessment">
            <div class="module-review-inner">
                <h3>Assessment</h3>
                <div class="row">
                    <div class="col-md-6">
                        <a href="{{$siteurl}}/self-assessment/{{$course->id}}" class="answerButton" style="margin-bottom: 10px; " >Take Assessment</a>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif

    @if( $module->is_overview != 1 && !in_array($course->type, array(1)))
	<div class="col-md-12 module-upload">     	
    	<div class="module-review"> 
        	<div class="module-review-inner">
                <h3>Your Submission for this module</h3>
                <div class="col-md-12">
                <div class="drop"> 
                  
                     <input type="file" multiple="" @if ($upload_disabled==0 OR $upload_disabled==3 OR $upload_disabled==11) disabled="disabled" @endif id="user_submission" class="{{$upload_disabled}}" name="user_submission">
                     
                            
                     
                     <a id="#user_submission_trigger"  onclick="return addModuleSubmission({{$module->id}},{{ $user_current_course->id}}, @if ($upload_disabled==0 OR $upload_disabled==3 OR $upload_disabled==11) false @else true @endif , '') " module_id="{{ $module->id }}" user_id="{{ Auth::user()->id }}" href="#" class="pinkbutton" id="module6add_resource"> Add<i class="fa fa-angle-right"></i></a>

                       @if ($upload_disabled==0)<a href="javascript:void({{ $upload_disabled }})"><i class="fa fa-lock"></i></a> <a tabindex="0" href="javascript:void(0)" id="lock_msg"  role="button" data-toggle="popover" data-trigger="focus" data-content="Your submission is not reviewed yet. Please wait for feedback."><i class="fa fa-info-circle"></i>
</a> @endif
                    @if ($upload_disabled==3)<a href="javascript:void({{ $upload_disabled }})"><i class="fa fa-lock"></i></a> <a tabindex="0" href="javascript:void(0)" id="lock_msg"  role="button" data-toggle="popover" data-trigger="focus" data-content="You have passed this module."><i class="fa fa-info-circle"></i>
                    </a> @endif

                    @if ($upload_disabled==1 or $upload_disabled==2)<a href="javascript:void({{ $upload_disabled }})"><i class="fa fa-lock"></i></a> <a tabindex="0" href="javascript:void(0)" id="lock_msg"  role="button" data-toggle="popover" data-trigger="focus" data-content="Please see the latest feedback on this module"><i class="fa fa-info-circle"></i>
                    </a> @endif

                    @if ($upload_disabled==11)<a href="javascript:void({{ $upload_disabled }})"><i class="fa fa-lock"></i></a> <a tabindex="0" href="javascript:void(0)" id="lock_msg"  role="button" data-toggle="popover" data-trigger="focus" data-content="You have not passed the previous module. Please complete the previous module to start this one."><i class="fa fa-info-circle"></i>
                    </a> @endif

                     </div>

                     <ul id="user-submission-items">
    					@foreach($user_submissions as $sub)
                        <li> <span class="date">{{ date('d/m/Y',strtotime($sub->created_at)) }}</span><span class="filename">{{ $sub->attachment_name }}</span> </li>

                        @endforeach
                     </ul>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="col-md-12 examiner-feedback">
    	<div class="module-review pinkbg">
        	<div class="module-review-inner">
                <h3>Feedback</h3>
                <div class="col-md-12 feedbacks">
                @if (count($user_feedbacks)>0)

                    @foreach($user_feedbacks as $uf)
                      <div class="row">
                      	 <div class="col-md-1">
                         @if ($uf->user_image!==null)
                         <img class="img-circle feedback-user-image" src="{{ $siteurl.'/uploads/'.$uf->user_image }}" />
                         @else
                         <img class="img-circle feedback-user-image" src="{{ $siteurl }}/images/default-user.png" />
                         @endif
                         </div>
                         <div class="col-md-2">{{ date('d/m/Y', strtotime($uf->created_at)) }}</div>
                         <div class="col-md-9">{{ $uf->message}} <br/>
                          @if($uf->attachment_ids != '') 
                             @define $at_ids = explode(',',$uf->attachment_ids)
                             @define $i = 1;
                             @foreach($at_ids as $ai)
                             <a href="{{$siteurl}}/downloadResource/{{base64_encode($ai)}}">Download Attachment {{$i}}</a>
                             @endforeach
                           @endif     
                          </div>
                      </div>
                    @endforeach
                @endif  
                </div>
            </div>
        </div>
    </div>    

</div>

    <div id="inline1" style="width:500px;display: none;">
        <form name="feedbackForm" id="adminFeedbackForm" class="feedbackForm" method="post" action="#">
            <h3>Questions or feedback? </h3>
        <p>
            Complete the form below
        </p>

            <p>
                <input type="hidden" name="feedbackModuleID" id="feedbackModuleID" value="" />
                <input type="hidden" name="feedbackCourseID" id="feedbackCourseID" value="" />
                <input type="hidden" name="feedbackUserID" id="feedbackUserID" value="" />
                <input type="hidden" name="feedbackAttachmentsID" id="feedbackAttachmentsID" value="" />

                <textarea name="feedbackmessage" id="feedbackmessage" cols="40" rows="5" required="required"></textarea>
                <br/>
            <div class="module-upload">
                <h3>Attachments</h3>
                <div class="attachment-container">
                    <div class="attachment-container-inner">
                        <div class="fields-container">
                            <div class="drop">

                                <input type="file" multiple="" id="attachment_submission" name="attachment_submission">



                                <a id="addnew_attachment_trigger"  onclick="return addNewAttachment({{ $user->id }})" user_id="{{ Auth::user()->id }}" disabled-message="" href="#" class="pinkbutton"> Add <i class="fa fa-angle-right"></i></a>


                                <a tabindex="0" href="javascript:void()" id="lock_msg"  role="button" data-toggle="popover" data-trigger="focus" data-content="Supported file type: jpg, jpeg, gif, pdf,doc, docx"><i class="fa fa-info-circle"></i>
                                </a>


                            </div>

                            <ul class="user-attachment-items" id="attachment-items">
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            </p>
            <p id="feedbackProgress"></p>
            <a class="pinkbutton" href="#" id="feedbackFormTrigger">Submit <i class="fa fa-angle-right"></i> </a>
        </form>
    </div>
    
    <div id="harpar-media-player" class="player" style="display: none; width: 100%; height: 100%;"></div>

</div>
