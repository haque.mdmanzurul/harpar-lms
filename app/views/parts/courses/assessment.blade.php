<div class="userview">
<script type="text/javascript">
    var ecommerce_course_id ='{{$course->ecommerce_course_id}}' ;
</script>
    <div class="row courses details" ng-app="quizApp">
        <div class="col-md-12">
            <h3>Self Assessment for course : <i>{{$course->title}}</i><input type="hidden" id="courseid" value="{{$course->ecommerce_course_id}}"><input type="hidden" id="courseidown" value="{{$course->id}}"></h3>
            <div class="quiz-preinfo">

            </div>
            <div id="quiz-holder">
            <quiz/>
            </div>
        </div>
    </div>
</div>
