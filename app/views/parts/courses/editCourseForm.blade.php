<div class="row courses">  
	<div class="col-md-3">  	
        <h3>Edit Course</h3>
    </div>
    <div class="col-md-7">

    </div>
    
    <div class="col-md-2">
    	<a href="{{ $siteurl }}/courses" class="pinkbutton pull-right"><i class="fa fa-angle-left"></i> Back 
</a>
    </div>
</div>
@if (Session::has('flash_notice'))
    <p class="bg-primary">
        {{ Session::get('flash_notice') }}
    </p>
@endif
<div class="row courses">
	<div class="col-md-12">

        <div class="add-course-container">
        <form name="addcourse" id="addcourseform" action="/course/edit/{{ $course->id }}" method="post" enctype="multipart/form-data">
        
        	<input type="hidden" name="ID" placeholder="id" value="{{ $course->id }}">
            <input type="text" name="refno" placeholder="Course Ref No" value="{{ $course->ref_no }}">
            {{--<input type="hidden" name="expiry_date" id="expiry_date" placeholder="{{ date('Y-m-d',strtotime('+2 years')) }}" class="halfwidth" value="{{ date('Y-m-d',strtotime($course->expiry_date)) }}">--}}
            <input type="text" name="title" placeholder="Title" value="{{ $course->title }}">
            <input type="text" name="qualification" placeholder="Qualification" value="{{ $course->qualification }}">
            <input type="text" name="description" placeholder="Description" value="{{ $course->description }}">
            
            <div class="modules">
            <input type="hidden" name="count_module" id="count_module" value="{{  $count_module }}"  />
            <input type="hidden" name="mode" id="mode" value="edit">
                 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  @if($count_module>1)
                  {{--@for ($i = 1; $i <= $count_module-1; $i++)--}}
                   @define $i = 1;
                   @foreach ($modules as $module)


                  <div id="module-panel-{{$i}}" class=" gen-mod panel panel-default       {{($course->type == 3 && $module->is_overview==0 && $i>=3)?'hidden':''}}      {{($course->type == 1 && $module->is_overview==0)?'hidden':''}}  {{ ($i==1)?"open":"" }} {{$course->type}}">
                    <div class="panel-heading" role="tab" id="heading{{$i}}">
                      <h4 class="panel-title">
                        <a module="{{$i}}" class="m_title" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
                          @if ($i==1)
                          Course Overview
                          @else
                          Module {{$i-1}}
                          @endif
                        </a>
                      </h4>
                    </div>
                    <div id="collapse{{$i}}" class="panel-collapse collapse {{ ($i==1)?"in":"" }}" role="tabpane{{$i}}" aria-labelledby="heading{{$i}}">
                      <div class="panel-body">
                        <div class="module-fields">
                        <label>Title</label>
                        <input type="text" id="module_{{$i}}_title" name="module_{{$i}}_title" value="{{ $module->title }}" placeholder="Module Title {{$i-1}}" />
                        <label>Description</label>
                        <textarea class="mceEditor" id="module_{{$i}}_description" cols="100" rows="5" name="module_{{$i}}_description">{{ $module->description  }}</textarea>                        <br/>
                        <div class="drop">
                         
                         <label>Resources</label>
                         <input type="text" id="module{{$i}}_resource_title" name="module{{$i}}_resource_title" value="" placeholder="Resource Title 1" />

                            <input type="text" value="" name="module{{$i}}_video_key" id="module{{$i}}_video_key" placeholder="Put youtube or vimeo url here(optional)">
                            <input type="file" name="module{{$i}}_upl" id="module{{$i}}_upl" multiple />

                         <a id="module{{$i}}add_resource" class="pinkbutton blackbg addresource" href="#" module_id="module{{$i}}" onclick="return addResources('{{$i}}')">
                             Add <i class="fa fa-angle-right"></i>
                         </a>
                         <span id="resource_status_{{$i}}" class="resource_process_status"></span>


                        </div>
                        <div class="module-resources">
                        	<ul id="module-resources-{{$i}}">
                            	 @define $j=1
                                 @define $resource_ids=''

                                @if(count($resources)>0)
                            	 @foreach($resources as $resource)
                                 
                                 @if ($resource->moduleID!=$modules[$i-1]->id)
                                 @define continue
                                 @endif
                                 
                                 @define $resource_ids .= ($resource_ids=='')?$resource->id:','.$resource->id
                            	 <li id="module_{{$i}}resources_{{$j}}">
                                     <input type="hidden" value="{{$resource->title}}" id="module_{{$i}}_resource_{{$j}}_title" name="module_{{$i}}_resource_{{$j}}_title">
                                     @if ($resource->attachment_type == "pdf")
                                         <i class="fa fa-file-pdf-o"></i>
                                     @elseif ($resource->attachment_type == "doc" or $resource->attachment_type == "docx")
                                         <i class="fa fa-file-word-o"></i>
                                     @elseif($resource->attachment_type == "ppt" or $resource->attachment_type == "pptx")
                                         <i class="fa fa-file-powerpoint-o"></i>
                                     @elseif($resource->attachment_type == "story")
                                         <i class="fa fa-file-audio-o"></i>
                                     @elseif($resource->attachment_type == "youtube")
                                         <i class="fa fa-youtube-square"></i>
                                     @elseif($resource->attachment_type == "vimeo")
                                         <i class="fa fa-vimeo-square"></i>
                                     @endif
                                     <span id="module_{{$i}}resources_{{$j}}_title" class="title">{{$resource->title}}</span>
                                     <span class="actionbar">
                                     <a data-target="module_{{$i}}resources_{{$j}}" class="resource_edit" resource_id="{{$resource->id}}" list_module_no="{{ $i  }}" list_resource_no="{{ $j  }}" module_id="{{ $modules[$i-1]->id  }}" href="#">Edit</a> | <a data-target="module_{{$i}}resources_{{$j}}"  resource_id="{{$resource->id}}" module_id="{{ $modules[$i-1]->id  }}" href="#" class="resource_delete">Delete</a>
                                     </span>
                                     </li>
                                     @define $j++;
                                  @endforeach
                                  @endif
                            </ul>
                        </div>
                        </div>
                        <input type="hidden" id="module{{$i}}_resource_action"  name="module{{$i}}_resource_action" value="add" />
                        <input type="hidden" id="module{{$i}}_resource_id"  name="module{{$i}}_resource_id" value="" />                        
                        <input type="hidden" name="module{{$i}}_resource_count" id="module{{$i}}_resource_count" value="{{ $j-1 }}" />
                        <input type="hidden" id="module_{{$i}}_action" name="module_{{$i}}_action" value="update" />
                        <input type="hidden" id="module_{{$i}}_id" name="module_{{$i}}_id" value="{{ $modules[$i-1]->id  }}" />
                        <input type="hidden" id="module_{{$i}}_resource_ids" name="module_{{$i}}_resource_ids" value="{{$resource_ids}}" placeholder="" />
                       @if($i>1) 
                       <div class="delete-module">
                           <label><input type="checkbox" name="module_delete[]" value="{{ $modules[$i-1]->id  }}" /> Delete</label>

                       </div>
                       @endif
                          @define $i++
                      </div>
                      
                    </div>
                  </div>

				   @endforeach
                   @endif

                    <!-- Certification Module -->

                         <div id="module-panel-{{$i}}" class="panel hidden panel-default {{ (( $course->type == 1))?'hidden':'enabled' }}  ">
                                 <div class="panel-heading" role="tab" id="heading{{$i}}">
                                     <h4 class="panel-title">
                                         <a module="{{$i}}" class="m_title" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
                                             Certification
                                         </a>
                                     </h4>
                                 </div>
                                 <div id="collapse{{$i}}" class="panel-collapse collapse" role="tabpane{{$i}}" aria-labelledby="heading{{$i}}">
                                     <div class="panel-body">
                                         <div class="module-fields">
                                             <label>Title</label>
                                             <input type="text" id="module_{{$i}}_title" name="module_{{$i}}_title" value="{{$certification_module_title}}" placeholder="Certification" />
                                             <label>Description</label>
                                             <textarea  class="mceEditor" id="module_{{$i}}_description" cols="100" rows="5" name="module_{{$i}}_description">{{ $certification_module_description  }}</textarea>                        <br/>
                                             <div class="drop">

                                                 <label>Resources</label>
                                                 <input type="text" id="module{{$i}}_resource_title" name="module{{$i}}_resource_title" value="" placeholder="Resource Title 1" />

                                                 <input type="text" value="" name="module{{$i}}_video_key" id="module{{$i}}_video_key" placeholder="Put youtube or vimeo url here(optional)">
                                                 <input type="file" name="module{{$i}}_upl" id="module{{$i}}_upl" multiple />

                                                 <a id="module{{$i}}add_resource" class="pinkbutton blackbg addresource" href="#" module_id="module{{$i}}" onclick="return addResources('{{$i}}')">
                                                     Add <i class="fa fa-angle-right"></i>
                                                 </a>
                                                 <span id="resource_status_{{$i}}" class="resource_process_status"></span>


                                             </div>
                                             <div class="module-resources">
                                                 <ul id="module-resources-{{$i}}">
                                                     @define $j=1
                                                     @define $resource_ids=''

                                                     @if(count($resources)>0)

                                                     @foreach($resources as $resource)

                                                         @if ($resource->moduleID!=$certification_module->id)
                                                             @define continue
                                                         @endif

                                                             @if(isset($resource->title))
                                                         @define $resource_ids .= ($resource_ids=='')?$resource->id:','.$resource->id
@endif

                                                         <li id="module_{{$i}}resources_{{$j}}">
                                                             @if(isset($resource->title))
                                                             <input type="hidden" value="{{$resource->title}}" id="module_{{$i}}_resource_{{$j}}_title" name="module_{{$i}}_resource_{{$j}}_title">
                                                             @else
                                                                 <input type="hidden" value="" id="module_{{$i}}_resource_{{$j}}_title" name="module_{{$i}}_resource_{{$j}}_title">

                                                             @endif


                                                             @if ($resource->attachment_type == "pdf")
                                                                 <i class="fa fa-file-pdf-o"></i>
                                                             @elseif ($resource->attachment_type == "doc" or $resource->attachment_type == "docx")
                                                                 <i class="fa fa-file-word-o"></i>
                                                             @elseif($resource->attachment_type == "ppt" or $resource->attachment_type == "pptx")
                                                                 <i class="fa fa-file-powerpoint-o"></i>
                                                             @elseif($resource->attachment_type == "story")
                                                                 <i class="fa fa-file-audio-o"></i>
                                                             @elseif($resource->attachment_type == "youtube")
                                                                 <i class="fa fa-youtube-square"></i>
                                                             @elseif($resource->attachment_type == "vimeo")
                                                                 <i class="fa fa-vimeo-square"></i>
                                                             @endif
                                                             <span id="module_{{$i}}resources_{{$j}}_title" class="title">{{$resource->title}}</span>
                                     <span class="actionbar">
                                     <a data-target="module_{{$i}}resources_{{$j}}" class="resource_edit" resource_id="{{$resource->id}}" list_module_no="{{ $i  }}" list_resource_no="{{ $j  }}" module_id="{{ $certification_module->id  }}" href="#">Edit</a> | <a data-target="module_{{$i}}resources_{{$j}}"  resource_id="{{$resource->id}}" module_id="{{ $certification_module->id  }}" href="#" class="resource_delete">Delete</a>
                                     </span>
                                                         </li>
                                                         @define $j++;
                                                     @endforeach
                                                         @endif
                                                 </ul>
                                             </div>
                                         </div>
                                         <input type="hidden" id="module{{$i}}_resource_action"  name="module{{$i}}_resource_action" value="add" />
                                         <input type="hidden" id="module{{$i}}_resource_id"  name="module{{$i}}_resource_id" value="" />
                                         <input type="hidden" name="module{{$i}}_resource_count" id="module{{$i}}_resource_count" value="{{ $j-1 }}" />
                                         <input type="hidden" id="module_{{$i}}_action" name="module_{{$i}}_action" value="update" />
                                         <input type="hidden" id="module_{{$i}}_id" name="module_{{$i}}_id" value="{{ $certification_module_id  }}" />
                                         <input type="hidden" id="module_{{$i}}_resource_ids" name="module_{{$i}}_resource_ids" value="{{$resource_ids}}" placeholder="" />

                                     </div>

                                 </div>
                             </div>

                 </div>
                <a href="#" id="addModule" type="button" class="pinkbutton addmodule"> Add New Module </a>         	    <a href="#" id="addCourseSubmit" type="button" class="pinkbutton addmodule pull-right"> Update  Course </a>       	
            </div>
            
             
        </form>
        </div>
    </div>
</div>