<div class="row courses">  
	<div class="col-md-7">  	
        <h3>Add Course</span></h3>
    </div>
    <div class="col-md-3">
    	
    </div>
    
    <div class="col-md-2">
    	<a href="{{ $siteurl }}/courses" class="pinkbutton pull-right"><i class="fa fa-angle-left"></i> Back 
</a>
    </div>
</div>
<div class="row courses">
	<div class="col-md-12">

        <div class="add-course-container">
        <form name="addcourse" id="addcourseform" action="/addcourse" method="post" enctype="multipart/form-data">
        	<input type="text" name="refno" placeholder="Course Ref No" class="halfwidth">
            <input type="hidden" name="expiry_date" id="expiry_date" placeholder="Course Expiry Date( YYYY-MM-DD )" class="halfwidth">
            <input type="text" name="title" placeholder="Title">
            <input type="text" name="qualification" placeholder="Qualification">
            <input type="text" name="description" placeholder="Description">
            
            <div class="modules">
            <input type="hidden" name="count_module" id="count_module" value="5"  />
            <input type="hidden" name="mode" id="mode" value="add">
                 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  @for ($i = 1; $i < 6; $i++)
                  <div id="module-panel-{{$i}}" class="panel panel-default {{ ($i==1)?"open":"" }}">
                    <div class="panel-heading" role="tab" id="heading{{$i}}">
                      <h4 class="panel-title">
                        <a module="{{$i}}" class="m_title" data-toggle="collapse" data-parent="#accordion" href="#collapse{{$i}}" aria-expanded="true" aria-controls="collapse{{$i}}">
                          
                          @if ($i==1)
                          Course Overview
                          @else
                          Module {{$i-1}}
                          @endif
                        </a>
                      </h4>
                    </div>
                    <div id="collapse{{$i}}" class="panel-collapse collapse {{ ($i==1)?"in":"" }}" role="tabpane{{$i}}" aria-labelledby="heading{{$i}}">
                      <div class="panel-body">
                      
                        <div class="module-fields">
                        <label>Title</label>
                        <input type="text" id="module_{{$i}}_title" name="module_{{$i}}_title" value="" placeholder="@if ($i==1) Overview title @else Module Title {{$i-1}} @endif" />
                        <label>Description</label>
                        <textarea id="module_{{$i}}_description" cols="100" rows="5" name="module_{{$i}}_description"></textarea>                        <br/>
                        <div class="drop">
                         <input type="hidden" name="module{{$i}}_resource_count" id="module{{$i}}_resource_count" value="0" />
                         <label>Resources</label>
                         <input type="text" id="module{{$i}}_resource_title" name="module{{$i}}_resource_title" value="" placeholder="Resource Title 1" />
                            <input type="text" value="" name="module{{$i}}_video_key" id="module{{$i}}_video_key" placeholder="Put youtube or vimeo url here(optional)">
						 <input type="file" name="module{{$i}}_upl" id="module{{$i}}_upl" multiple />
                         <input type="hidden" id="module{{$i}}_resource_action"  name="module{{$i}}_resource_action" value="add" />
                         <input type="hidden" id="module{{$i}}_resource_id"  name="module{{$i}}_resource_id" value="" />
                         <a class="pinkbutton blackbg addresource" href="#" module_id="module{{$i}}" onclick="return addResources('{{$i}}')"> Add <i class="fa fa-angle-right"></i></a>
                            <span id="resource_status_{{$i}}" class="resource_process_status"></span>

                        </div>
                        <div class="module-resources">
                        	<ul id="module-resources-{{$i}}">
                            	 
                            </ul>
                        </div>
                        </div>
                        <input type="hidden" id="module_{{$i}}_resource_ids" name="module_{{$i}}_resource_ids" value="" placeholder="Module Title {{$i}}" />
                        <input type="hidden" id="module_{{$i}}_action" name="module_{{$i}}_action" value="add" />
                        @if($i>1)
                        <div class="delete-module"><a href="#" onclick=" return delete_module({{$i}})" class="deletemoduletrgigger" data-target="module-panel-{{$i}}">Delete this module</a></div>
                        @endif
                      </div>
                    </div>
                  </div>           
                   
				   @endfor                  
                </div>  
                <a href="#" id="addModule" type="button" class="pinkbutton addmodule"> Add New Module </a>         	    <a href="#" id="addCourseSubmit" type="button" class="pinkbutton addmodule pull-right"> Save Course </a>       	
            </div>
            
             
        </form>
        </div>
    </div>
</div>