<div class="row courses">  
	<div class="col-md-7 col-sm-7">
        <h3>Courses <span>(Displaying {{ $course_current_start }} - {{ $courses_current_total }} of {{ $courses_total }} Courses)</span></h3>
    </div>
    <div class="col-md-3 col-sm-3">
    	{{$courses->appends(Input::except('page'))->links()}}
    </div>
    
    <div class="col-md-2 col-sm-2">
    	@if(Auth::user()->user_type == 0) <a href="{{ $siteurl }}/addcourse" class="pinkbutton pull-right">Add <i class="fa fa-angle-right"></i>
</a>
@endif
    </div>
</div>
@if(Session::get('flash_notice'))
<div class="col-md-12">
    <p class="bg-primary">{{ Session::get('flash_notice') }}</p>
</div>
@endif

<div class="row filterbar">
    {{ Form::open(array('url' => '/courses/','method'=>'get')) }}
    <div class="col-md-3 col-sm-3"></div>
    <div class="col-md-2 col-sm-2">
        {{Form::text('search',Input::get('search'),array('placeholder'=>'Search By Ref No, Course Title or Qualification'));}}
         </div>
    <div class="col-md-3 col-sm-3">
        <div class="styled-select">
            Sort By: <select name="sortby">
            <option @if(isset($sortby) && $sortby=='ref_no') selected="selected" @endif value="ref_no">Ref No</option>
            <option @if(isset($sortby) && $sortby=='title') selected="selected" @endif  value="title">Title</option>
            <option @if(isset($sortby) && $sortby=='qualification') selected="selected" @endif  value="qualification">Qualification</option>
        </select>
        </div>
    </div>
    <div class="col-md-2 col-sm-2">
        <div class="styled-select-2">Sort: <select name="order">
            <option @if(isset($order) && $order=='ASC') selected="selected" @endif  value="ASC">ASC</option>
            <option @if(isset($order) && $order=='DESC') selected="selected" @endif  value="DESC">DESC</option>
        </select>
        </div>
    </div>

    <div class="col-md-2 col-sm-2">
        <input type="submit" name="submit" value="Filter"/>
    </div>
    {{ Form::close() }}
</div>
<div class="row courses">
	<div class="col-md-12">

        <div class="pending-review-container">
        <table border="0" cellpadding="0" cellspacing="0" id="myTable" class="tablesorter"> 
            <thead> 
            <tr> 
                <th>Ref.</th> 
                <th>Title</th> 
                <th>Qualification</th>
                @if(Auth::user()->user_type == 1)
                    <td>Expire on</td>
                @endif
                @if(Auth::user()->user_type == 0)<th></th>  @endif
            </tr> 
            </thead> 
            <tbody> 
            @if (count($courses) > 0)
            @foreach($courses as $course)
            <tr> 
                <td>{{$course->ref_no}}</td> 
                <td>{{$course->title}}</td> 
                <td>{{$course->qualification}}</td>
                @if(Auth::user()->user_type == 1)
                    <td>{{ date('Y-m-d', strtotime($course->exp_date)) }}</td>
                @endif
                <td>@if(Auth::user()->user_type == 0)<a class="edit" href="{{ $siteurl }}/course/edit/{{$course->id.'-'.$course->type}}">Edit</a> | <a class="delete" href="{{ $siteurl }}/course/delete/{{$course->id}}" onclick="return delete_course()">Delete</a>
                @else
                <a class="delete" href="{{ $siteurl }}/course/{{$course->course_slug}}/{{ base64_encode($course->moduleID) }}" >View</a>
                @endif
                </td>   
            </tr> 
            @endforeach
                @else
               <tr> <td> You do not have any courses to display.</td> </tr> 
            @endif
            </tbody> 
            </table>
        </div>
    </div>
</div>