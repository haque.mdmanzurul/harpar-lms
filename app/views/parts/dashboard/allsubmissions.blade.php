<div class="row user-greeting">
	<div class="col-md-1 user-image">
    @if (Auth::user()->user_image=="")
    	<img src="{{ $siteurl }}/images/default-user.png" class="img-circle" />
    @else    
    	<img src="{{ $siteurl }}/uploads/{{ Auth::user()->user_image }}" class="img-circle" />
    @endif
    </div>
    <div class="col-md-11 user-details dashbaord">
    	<h1>Welcome back {{ Auth::user()->firstname }}.</h1>
        <p>You last logged in at {{ date('h.i a',strtotime($history_latest->created_at)) }} on {{ date('d/m/Y',strtotime($history_latest->created_at)) }}</p>
    </div>
</div>



<div class="row pending-reviews">
    <div class="col-md-12">

        <h3>Module submitted and pending review <span>( Displaying {{ $user_submissions_current_start }} - {{ $user_submissions_current_total }}   @if($user_submissions_current_total>1) submissions @else submission @endif )</span></h3>
        <div class="pending-review-container">
            <table border="0" cellpadding="0" cellspacing="0" id="myTable" class="tablesorter">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Course</th>
                    <th>Module</th>
                    <th>Date</th>
                    {{--<th>Status</th>--}}
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($user_submissions as $us)

                    <tr>
                        <td>{{ $us->firstname.' '.$us->lastname }}</td>
                        <td>{{ $us->coursetitle }}</td>
                        <td>{{ $us->moduletitle }}</td>
                        <td>{{ date('Y-m-d',strtotime($us->created_at)) }}</td>
                       {{-- <td>{{$us->status}}</td>--}}
                        <td style="text-align: center"><a href="/users/{{ $us->username }}/course/{{ $us->course_slug }}" class="viewaccount">View</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        <div class="pull-right">{{ $user_submissions->links() }}</div>
    </div>
</div>




<div id="inline1" style="width:500px;display: none;">
	<div class="userMessage">
    <strong>Posted by:</strong> <span id="postedby"></span><br/>
    <strong>Posted on:</strong> <span id="postedon"></span><br/>
    <strong>Message:</strong> <span id="fmessage"></span><br/>
    </div>

    <form name="feedbackForm" id="adminFeedbackForm" class="feedbackForm" method="post" action="#">
        <h3>Questions or feedback? </h3>
        <p>
            Complete the form below
        </p>

        <p>
            <input type="hidden" name="feedbackModuleID" id="feedbackModuleID" value="" />
            <input type="hidden" name="feedbackCourseID" id="feedbackCourseID" value="" />
            <input type="hidden" name="feedbackUserID" id="feedbackUserID" value="" />
            <input type="hidden" name="feedbackAttachmentsID" id="feedbackAttachmentsID" value="" />

            <textarea name="feedbackmessage" id="feedbackmessage" cols="40" rows="5" required="required"></textarea>
            <br/>
        <div class="module-upload">
            <h3>Attachments</h3>
            <div class="attachment-container">
                <div class="attachment-container-inner">
                    <div class="fields-container">
                        <div class="drop">

                            <input type="file" multiple="" id="attachment_submission" name="attachment_submission">



                            <a id="addnew_attachment_trigger"  onclick="return addNewAttachment({{ Auth::user()->id }})" user_id="{{ Auth::user()->id }}" disabled-message="" href="#" class="pinkbutton"> Add <i class="fa fa-angle-right"></i></a>


                            <a tabindex="0" href="javascript:void()" id="lock_msg"  role="button" data-toggle="popover" data-trigger="focus" data-content="Browse for certificate, supported type: jpg, jpeg, gif, pdf,doc,docx"><i class="fa fa-info-circle"></i>
                            </a>


                        </div>

                        <ul class="user-attachment-items" id="attachment-items">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </p>
        <p id="feedbackProgress"></p>
        <a class="pinkbutton" href="#" id="feedbackFormTrigger">Submit <i class="fa fa-angle-right"></i> </a>
    </form>
</div>
</div>