<div class="row user-greeting">
	<div class="col-md-1 user-image">
    @if (Auth::user()->user_image=="")
    	<img src="{{ $siteurl }}/images/default-user.png" class="img-circle" />
    @else    
    	<img src="{{ $siteurl }}/uploads/{{ Auth::user()->user_image }}" class="img-circle" />
    @endif
    </div>
    <div class="col-md-11 user-details dashbaord">
    	<h1>Welcome back {{ Auth::user()->firstname }}.</h1>
        <p>You last logged in at {{ date('h.i a',strtotime($history_latest->created_at)) }} on {{ date('d/m/Y',strtotime($history_latest->created_at)) }}</p>
    </div>
</div>

<div class="row site-stat">
	<div class="col-md-6 manage-users">
    	<div class="stat-box">
        	<div class="stat-box-head"><h2>Manage Users</h2></div>
            <div class="stat-box-content">
            	<span class="icon"><i class="fa fa-users"></i></span>
                <p>You have {{ $users_total }} Users</p>
                <a href="/users">View All</a>
            </div>
            <div class="stat-box-footer">
            	<div class="latest-user col-md-9">
                @if ($user_latest[0]->user_image=="")
                    <img src="{{ $siteurl }}/images/default-user.png" class="img-circle pull-left" />
                @else    
                    <img src="{{ $siteurl }}/uploads/{{$user_latest[0]->user_image}}" class="img-cirlce pull-left" />
                @endif                
                	
                    
                    	<h3>Latest User</h3>
                        <span>{{$user_latest[0]->firstname}} {{$user_latest[0]->lastname}}</span>
                 
                </div>
                <div class="action-bar col-md-3">
                	(<a href="/users/{{$user_latest[0]->username}}">View</a> / <a href="/edituser/{{$user_latest[0]->id}}">Edit</a>)
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 manage-courses">
   		<div class="stat-box">
        	<div class="stat-box-head lightgreen"><h2>Manage Courses</h2></div>
            <div class="stat-box-content">
            <span class="icon lightgreen"><i class="fa fa-desktop"></i></span>
                <p>You have {{ $courses_total }} Courses</p>
                <a href="/courses">View All</a>            
            </div>
            <div class="stat-box-footer">
                <div class="latest-course col-md-9">
                	<img src="images/course.png" class="img-responsive pull-left" />
                    
                    	<h3 class="lightgreen">Latest Course</h3>
                        <span>{{$course_latest[0]->title}}</span>              
                    
                </div>
                
                <div class="action-bar col-md-3">
                	(<a href="/course/edit/{{$course_latest[0]->id}}">View / Edit</a>)
                </div>
            </div>
        </div>
    </div>
</div>


<div class="row pending-reviews">
	<div class="col-md-12">
    	
        <h3>Help Requests</h3>
        <div class="pending-review-container">
        <table border="0" cellpadding="0" cellspacing="0" id="myTable" class="tablesorter"> 
            <thead> 
            <tr> 
                <th>Name</th> 
                <th>Course / Module</th> 
                <th>Message</th> 
                
            </tr> 
            </thead> 
            <tbody> 
	@if (count($user_feedbacks)>0)   
            @define $i = 1  
            @foreach ($user_feedbacks as $uf)
            @if($i == 10)    
            @define break   
            @endif            
            <tr> 
                <td>{{ $uf->firstname.' '.$uf->lastname }}</td> 
                <td>{{ $uf->coursetitle }}<br/>Module: {{ $uf->moduletitle }} <br/>
                <a href="{{$siteurl}}/users/{{ $uf->username }}/submissionhistory/{{ $uf->moduleID }}" feedbackid="{{ $uf->id }}" moduleID="{{ $uf->moduleID }}" courseID="{{ $uf->courseID }}" userID="{{ $uf->userID }}" class="viewaccount feedbackentry fancybox"> View Full Thread</a></td> 
                <td>{{ substr($uf->message,0,50) }}... <a href="#inline1" feedbackid="{{ $uf->id }}" moduleID="{{ $uf->moduleID }}" courseID="{{ $uf->courseID }}" userID="{{ $uf->userID }}" class="viewaccount feedbackentry fancybox">more</a>  </td>  
                
            </tr> 
            @define $i++
			@endforeach  
     @else
     <tr><td> No recent request found.</td></tr>  
     @endif
            </tbody> 
            </table>
        </div>
        <div class="pull-right"><a href="{{$siteurl}}/feedbacks" class="addmodule">View All</a> </div>
    </div>
</div>



<div class="row pending-reviews">
	<div class="col-md-12">
    	
        <h3>Module submitted and pending review <span></span></h3>
        <div class="pending-review-container">
        <table border="0" cellpadding="0" cellspacing="0" id="myTable" class="tablesorter"> 
            <thead> 
            <tr> 
                <th>Name</th> 
                <th>Course</th> 
                <th>Module</th> 
                <th>Status</th> 
                <th></th> 
            </tr> 
            </thead> 
            <tbody> 
            @foreach ($user_submissions as $us)
                @if($us->show == false)
                @define continue
                @endif
            <tr> 
                <td>{{ $us->firstname.' '.$us->lastname }}</td> 
                <td>{{ $us->coursetitle }}</td> 
                <td>{{ $us->moduletitle }}</td> 
                <td>submitted</td>  
                <td><a href="/users/{{ $us->username }}/course/{{ $us->course_slug }}" class="viewaccount">View</a></td> 
            </tr> 
			@endforeach 
            </tbody> 
            </table>
        </div>
        <div class="pull-right"><a href="{{$siteurl}}/submissions" class="addmodule">View All</a> </div>
    </div>
</div>

<div id="inline1" style="width:500px;display: none;">
	<div class="userMessage">
    <strong>Posted by:</strong> <span id="postedby"></span><br/>
    <strong>Posted on:</strong> <span id="postedon"></span><br/>
    <strong>Message:</strong> <span id="fmessage"></span><br/>
    </div>

    <form name="feedbackForm" id="adminFeedbackForm" class="feedbackForm" method="post" action="#">
        <h3>Questions or feedback? </h3>
        <p>
            Complete the form below
        </p>

        <p>
            <input type="hidden" name="feedbackModuleID" id="feedbackModuleID" value="" />
            <input type="hidden" name="feedbackCourseID" id="feedbackCourseID" value="" />
            <input type="hidden" name="feedbackUserID" id="feedbackUserID" value="" />
            <input type="hidden" name="feedbackAttachmentsID" id="feedbackAttachmentsID" value="" />

            <textarea name="feedbackmessage" id="feedbackmessage" cols="40" rows="5" required="required"></textarea>
            <br/>
        <div class="module-upload">
            <h3>Attachments</h3>
            <div class="attachment-container">
                <div class="attachment-container-inner">
                    <div class="fields-container">
                        <div class="drop">

                            <input type="file" multiple="" id="attachment_submission" name="attachment_submission">



                            <a id="addnew_attachment_trigger"  onclick="return addNewAttachment({{ Auth::user()->id }})" user_id="{{ Auth::user()->id }}" disabled-message="" href="#" class="pinkbutton"> Add <i class="fa fa-angle-right"></i></a>


                            <a tabindex="0" href="javascript:void()" id="lock_msg"  role="button" data-toggle="popover" data-trigger="focus" data-content="Browse for certificate, supported type: jpg, jpeg, gif, pdf,doc,docx"><i class="fa fa-info-circle"></i>
                            </a>


                        </div>

                        <ul class="user-attachment-items" id="attachment-items">
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        </p>
        <p id="feedbackProgress"></p>
        <a class="pinkbutton" href="#" id="feedbackFormTrigger">Submit <i class="fa fa-angle-right"></i> </a>
    </form>
</div>
</div>